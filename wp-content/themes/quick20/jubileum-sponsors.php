<?php
/*
 Template Name: Jublieum-sponsors
 
 */
?>
<?php get_header(); ?>
<div class="JubileumPage">
	<div class="MainTitle">
		<div class="container ContainerMainTitle">
			<div class="col-xs-12">˙
				<h1 class="Title">Sponsors</h1>
			</div>
		</div>
	</div>
	<div class="ImageHeaderSingle">
		<div class="Image" style="background-image: url(<?php the_field('afbeelding'); ?>);">
        </div>
	</div>
	<div class="container">
		<div class="col-lg-offset-2 col-lg-8 col-md-8">
			<div class="Sponsors">
				<div class="col-xs-12">
					<h2 class="black">Hoofdsponsors</h2>
				</div>
				<div class="col-xs-12">
					<div class="row JubileumSlider">
            			<ul id="jubileumScroller">
            				<?php
            				if( have_rows('hoofdsponsors', 'option') ):
            				while ( have_rows('hoofdsponsors', 'option') ) : the_row(); ?>
            					<li><a href="<?php the_sub_field('link', 'option'); ?>" target="_blank" ><img src="<?php the_sub_field('logo', 'option'); ?>"></a></li>
            				<?php
            				endwhile;
            				else :
            				endif;
            				?>
            			</ul>  
            		</div>
				</div>
				<div class="col-xs-12">
					<h2 class="black">Subsponsors</h2>
				</div>
				<div class="col-xs-12">
					<div class="col-lg-6 col-md-6 col-sm-12 Left">
					<?php
    				if( have_rows('subsponsors') ):
    				    $size = count(get_field('subsponsors'));
    				    $split = ceil($size / 2);
    				    $index = 0;
        				while ( have_rows('subsponsors') ) : the_row();
        				    if($index == $split) : ?>
        				    	</div>
								<div class="col-lg-6 col-md-6 col-sm-12 Right">
        				    <?php endif; ?>
        				<?php
        				    $addedClass = '';
        				    if(strlen(get_sub_field('naam')) > 31) {
        				        $addedClass = 'Smol';
        				    } else if (strlen(get_sub_field('naam')) > 25) {
        				        $addedClass = 'Medium';
        				    }
        				    $element = 'a';
        				    if(!get_sub_field('website')) {
        				        $element = 'div';
        				    }
        				?>
                        <<?php echo $element ?> class="Subsponsor <?php echo $addedClass ?>" target="_blank" href="<?php the_sub_field('website'); ?>">
                        	<?php the_sub_field('naam'); ?>
                        	<?php if(get_sub_field('website')) : ?>
                                <span class="CollapseIcon"></span>
                            <?php endif; ?>
                    	</<?php echo $element ?>>
					<?php
					    $index++;
        				endwhile;
            			else :
        				endif;
        			?>
        			</div>
    			</div>
        	</div>
		</div>
	</div>
</div>
<?php include 'footer.php';?>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.q20.jubileumpage.20200105.js"></script>