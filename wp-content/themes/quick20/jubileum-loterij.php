<?php
/*
 Template Name: Jublieum-loterij
 
 */
?>
<?php get_header(); ?>
<div class="JubileumPage Loterij">
	<div class="MainTitle">
		<div class="container ContainerMainTitle">
			<div class="col-xs-12">˙
				<h1 class="Title">Loterij</h1>
			</div>
		</div>
	</div>
	<div class="ImageHeader">
		<div class="Image" style="background-image: url(<?php the_field('afbeelding_links'); ?>);">
		</div>
		<div class="Image" style="background-image: url(<?php the_field('afbeelding_midden'); ?>);">
		</div>
		<div class="Image" style="background-image: url(<?php the_field('afbeelding_rechts'); ?>);">
		</div>
	</div>
	<div class="container">
		<div class="col-lg-offset-2 col-lg-8 col-md-8">
			<div class="Introduction">
				<div class="col-xs-12">
					<h2 class="black">Jubileumloterĳ</h2>
				</div>
				<?php the_field('inleiding'); ?>
			</div>
			<div class="Programma">
			
			<!-- aanmelden -->
				<div class="Part">
					<div class="col-xs-12">
						<h2 class="black">Uitslag trekkingen</h2>
					</div>
					<div class="col-xs-12 Trekkingen">
						<a href="https://www.quick20.nl/2020/01/21/uitslag-eerste-trekking-jubileumlotterij/">
							Eerste trekking
						</a>
						<br/>
						<a href="https://www.quick20.nl/2020/02/20/uitslag-tweede-trekking-jubileumlotterij/">
							Tweede trekking
						</a>
						<br/>
						<a href="https://www.quick20.nl/2020/03/21/uitslag-derde-trekking-jubileumlotterij/">
							Derde trekking
						</a>
						<br/>
						<a href="https://www.quick20.nl/2020/04/20/uitslag-vierde-trekking-jubileumlotterij/">
							Vierde trekking
						</a>
						<br/>
						<a href="https://www.quick20.nl/2020/05/20/uitslag-vijfde-trekking-jubileumlotterij/">
							Vijfde trekking
						</a>
						<br/>
						<a href="https://www.quick20.nl/2020/10/24/uitslag-zesde-trekking-jubileumlotterij/">
							Zesde trekking
						</a>
					</div>
				</div>
        	</div>
        	<div class="col-xs-12">
				<div class="row JubileumSlider">
        			<ul id="jubileumScroller">
        				<?php
        				if( have_rows('hoofdsponsors', 'option') ):
        				while ( have_rows('hoofdsponsors', 'option') ) : the_row(); ?>
        					<li><a href="<?php the_sub_field('link', 'option'); ?>" target="_blank" ><img src="<?php the_sub_field('logo', 'option'); ?>"></a></li>
        				<?php
        				endwhile;
        				else :
        				endif;
        				?>
        			</ul>  
        		</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="AanmeldingsModal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="AanmeldingsModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4>Inschrijfformulier</h4>
			</div>
			<div class="modal-body">
				<div id="Data"></div>
				<div class="Sending">
					Uw inschrijving wordt verzonden
					<img src="<?php bloginfo('template_directory'); ?>/images/loading.gif" />
				</div>
				<div class="Success">Uw inschrijving is verzonden!</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Sluiten</button>
			</div>
		</div>
	</div>
</div>


<?php include 'footer.php';?>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.q20.jubileumpage.20200105.js"></script>