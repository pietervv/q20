<?php
/*
Template Name: Jublieum-home

*/
?>
<?php get_header(); ?>
<div class="JubileumPage">
	<div class="MainTitle">
	    <div class="container ContainerMainTitle">
		    <div class="col-xs-12">
		    	<h1 class="Title"><img src="<?php bloginfo('template_directory'); ?>/images/header-home.png"></h1>
		    </div>
	    </div>
	</div>
	<div class="ImageHeaderSingle">
		<div class="Image" style="background-image: url(<?php the_field('afbeelding'); ?>);">
        </div>
	</div>
	<div class="container">
		<div class="col-lg-offset-2 col-lg-8 col-md-8">
			<div class="JubileumHome">
    		<?php
				if( have_rows('onderdelen') ):
				    $index = 0;
    				while ( have_rows('onderdelen') ) : the_row();
    				if($index % 2 == 0):
			?>
        		<div class="row">
    				<?php if(get_sub_field('titel_link')): ?>
        				<a href="<?php the_sub_field('titel_link'); ?>" target="_blank">
        			<?php endif; ?>
    					<div class="col-lg-6 col-md-6 col-sm-12">
    						<div class="PartTitle"><?php the_sub_field('titel'); ?></div>
    						<div class="PartText"><?php the_sub_field('tekst'); ?></div>
    					</div>
    					<div class="col-lg-6 col-md-6 col-sm-12">
    						<img src="<?php the_sub_field('afbeelding'); ?>">
    					</div>
    				<?php if(get_sub_field('titel_link')): ?>
            			</a>
            		<?php endif; ?>
    			</div>
			<?php
                    else:
            ?>
    			<div class="row">
    				<?php if(get_sub_field('titel_link')): ?>
        				<a href="<?php the_sub_field('titel_link'); ?>">
        			<?php endif; ?>
    					<div class="col-lg-6 col-md-6 col-sm-12">
    						<img src="<?php the_sub_field('afbeelding'); ?>">
    					</div>
    					<div class="col-lg-6 col-md-6 col-sm-12">
    						<div class="PartTitle"><?php the_sub_field('titel'); ?></div>
    						<div class="PartText"><?php the_sub_field('tekst'); ?></div>
    					</div>
    				<?php if(get_sub_field('titel_link')): ?>
            			</a>
            		<?php endif; ?>
    			</div>
			<?php
                    endif;
                    $index++;
    				endwhile;
			endif;
			?>
        	</div>
        	<div class="row JubileumSlider">
    			<ul id="jubileumScroller">
    				<?php
    				if( have_rows('hoofdsponsors', 'option') ):
    				while ( have_rows('hoofdsponsors', 'option') ) : the_row(); ?>
    					<li><a href="<?php the_sub_field('link', 'option'); ?>" target="_blank" ><img src="<?php the_sub_field('logo', 'option'); ?>"></a></li>
    				<?php
    				endwhile;
    				else :
    				endif;
    				?>
    
    			</ul>  
    		</div>
		</div>
	</div>
</div>	
<?php include 'footer.php';?>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.q20.jubileumpage.20200105.js"></script>