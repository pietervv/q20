<?php
/*
 Template Name: Jublieum-geschiedenis
 
 */
?>
<?php get_header(); ?>
<div class="JubileumPage">
	<div class="MainTitle">
		<div class="container ContainerMainTitle">
			<div class="col-xs-12">˙
				<h1 class="Title">Geschiedenis</h1>
			</div>
		</div>
	</div>
	<div class="ImageHeader">
		<div class="Image" style="background-image: url(<?php the_field('afbeelding_links'); ?>);">
		</div>
		<div class="Image" style="background-image: url(<?php the_field('afbeelding_midden'); ?>);">
		</div>
		<div class="Image" style="background-image: url(<?php the_field('afbeelding_rechts'); ?>);">
		</div>
	</div>
	<div class="container">
		<div class="col-lg-offset-2 col-lg-8 col-md-8">
			<div class="Introduction">
				<?php the_field('inleiding'); ?>
			</div>
			<div class="Programma">
        		<?php
    				if( have_rows('programma') ):
        				while ( have_rows('programma') ) : the_row();
        			?>
				<div class="ProgrammaRow Current">
					<div class="InitialContent">
						<div class="Date">
							<span><?php the_sub_field('afbeelddatum'); ?></span>
						</div>
						<div class="ProgrammaTitle">
							<span><?php the_sub_field('titel'); ?></span>
							<div class="PostLink">
								<?php
                                    $file = get_sub_field('pdf');
                                    if( $file ): ?>
                                        <a href="<?php the_sub_field('pdf'); ?>"><?php the_sub_field('link_vooraf_tekst'); ?></a>
                                <?php endif; ?>
    						</div>
						</div>
					</div>
				</div>
    						<?php
        				endwhile;
        			else :
    				endif;
    			?>
        	</div>
        	<div class="col-xs-12">
            	<div class="row JubileumSlider">
        			<ul id="jubileumScroller">
        				<?php
        				if( have_rows('hoofdsponsors', 'option') ):
        				while ( have_rows('hoofdsponsors', 'option') ) : the_row(); ?>
        					<li><a href="<?php the_sub_field('link', 'option'); ?>" target="_blank" ><img src="<?php the_sub_field('logo', 'option'); ?>"></a></li>
        				<?php
        				endwhile;
        				else :
        				endif;
        				?>
        			</ul>
        		</div>
    		</div>
		</div>
	</div>
</div>
<?php include 'footer.php';?>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.q20.jubileumpage.20200105.js"></script>