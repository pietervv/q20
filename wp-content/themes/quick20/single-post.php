<?php
/*
Template Name Posts: Nieuwsbericht
*/
?>

<?php get_header(); ?>
    
    <div class="MainTitle">
	    <div class="container ContainerMainTitle">
		    <div class="col-xs-12">
		    	<h1 class="Title">Nieuws</h1>
		    </div>
	    </div>
    </div>
 <div>
	<?php if(get_field('hoofdafbeelding')): ?>
  
    <div class="MainPhoto <?php the_field('uitlijnen_afbeelding'); ?>" style="background-image: url(<?php the_field('hoofdafbeelding'); ?>);">
	    <div class="photo-overlay"></div>
    </div>

	<?php else: ?>

		<?php if (in_category('jeugd') ) : ?>
			<div class="MainPhoto <?php the_field('uitlijnen_afbeelding_jeugd', 'option'); ?>" style="background-image: url(<?php the_field('jeugd', 'option'); ?>);">
				<div class="photo-overlay"></div>
    		</div>
		<?php endif; ?>
		<?php if (in_category('sponsoren') ) : ?>
			<div class="MainPhoto <?php the_field('uitlijnen_afbeelding_sponsoren', 'option'); ?>" style="background-image: url(<?php the_field('sponsoren', 'option'); ?>);">
				<div class="photo-overlay"></div>
    		</div>
		<?php endif; ?>
		<?php if (in_category('senioren') ) : ?>
			<div class="MainPhoto <?php the_field('uitlijnen_afbeelding_senioren', 'option'); ?>" style="background-image: url(<?php the_field('senioren', 'option'); ?>);">
				<div class="photo-overlay"></div>
    		</div>
		<?php endif; ?>
		<?php if (in_category('1e-selectie') ) : ?>
			<div class="MainPhoto <?php the_field('uitlijnen_afbeelding_1e-selectie', 'option'); ?>" style="background-image: url(<?php the_field('1e_selectie', 'option'); ?>);">
				<div class="photo-overlay"></div>
    		</div>
		<?php endif; ?>
		<?php if (in_category('vrijwilligers') ) : ?>
			<div class="MainPhoto <?php the_field('uitlijnen_afbeelding_vrijwilligers', 'option'); ?>" style="background-image: url(<?php the_field('vrijwilligers', 'option'); ?>);">
				<div class="photo-overlay"></div>
    		</div>
		<?php endif; ?>
		<?php if (in_category('wedstrijdprogrammas') ) : ?>
			<div class="MainPhoto <?php the_field('uitlijnen_afbeelding_wedstrijdprogrammas', 'option'); ?>" style="background-image: url(<?php the_field('wedstrijdprogrammas', 'option'); ?>);">
				<div class="photo-overlay"></div>
    		</div>
		<?php endif; ?>
		<?php if (in_category('interactief') ) : ?>
			<div class="MainPhoto <?php the_field('uitlijnen_afbeelding_interactief', 'option'); ?>" style="background-image: url(<?php the_field('interactief', 'option'); ?>);">
				<div class="photo-overlay"></div>
    		</div>
		<?php endif; ?>
		<?php if (in_category('algemeen') ) : ?>
			<div class="MainPhoto <?php the_field('uitlijnen_afbeelding_algemeen', 'option'); ?>" style="background-image: url(<?php the_field('algemeen', 'option'); ?>);">
				<div class="photo-overlay"></div>
    		</div>
		<?php endif; ?>

	<?php endif; ?> 
 </div>
	<div class="NewsSingle">
		<div class="container">
			<div class="col-lg-offset-1 col-lg-10 col-md-12 NewsSingleContent">
				<div class="row row-return">
					<div class="col-lg-10 col-md-9 col-sm-8 col-xs-6">
						<h5 class="ReturnLink"> &nbsp; &#60; &nbsp; <a href="/nieuws">Ga terug naar nieuwsoverzicht</a></h5>
					</div>
					<!--<div class="col-xs-2 FacebookShare">
						<div class="fb-like" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
					</div>-->
				</div>
				<div class="col-xs-12">
					<h1 class="SingleNewsTitle"><?php echo get_the_title(); ?></h1>
					<div class="col-md-6 SingleNewsText">
						<span class="SingleNewsDate"><?php the_time('l j F Y') ?></span><strong> - </strong><span class="SingleNewsCategory"><i><?php $category = get_the_category( $custompost ); echo $category[0]->cat_name; ?></i></span>
						<p><br>
							<?php the_field('content_nieuwsbericht'); ?>


						</p>
					</div>
					<div class="col-md-6 SingleNewsPhoto">
						<?php
						if( have_rows('versterkende_afbeeldingen') ):
						while ( have_rows('versterkende_afbeeldingen') ) : the_row(); ?>
						
						
							<img class="NewsPhoto" src="<?php the_sub_field('afbeelding'); ?>">
							
							
						<?php
						endwhile;
						else :
						endif;
   						?>
					</div>
				</div>
				
				
				<div class="col-md-3 share-col">
					<div class="row">
						<div class="share-buttons">
							<div class="col-md-4 share-fb">
								<div class="fb-share-button" data-href="<?php the_permalink(); ?>" data-layout="button"></div>
							</div>
							<div class="col-md-4 share-twitter">
								<a href="https://twitter.com/share" class="twitter-share-button"{count} data-text="<?php echo get_the_title(); ?>"></a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>					</div>
							<div class="col-md-4 share-google">
								<!-- Plaats deze tag bovenaan of vlak voor je laatste inhoudstag. -->
								<script src="https://apis.google.com/js/platform.js" async defer>
								  {lang: 'nl'}
								</script>
								
								<!-- Plaats deze tag waar je de knop 'delen' wilt weergeven. -->
								<div class="g-plus" data-action="share" data-annotation="none"></div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	
	
		<div class="news">
		<div class="container">
			
				<div class="col-lg-offset-1 col-lg-10">
					<h3 class="RecentNewsItems">Recente nieuws items</h3>
				</div>
			
				<div id="owl-example" class="owl-carousel col-lg-offset-1 col-lg-10 col-md-12 col-xs-12 newsItems">
				<?php $count = 0; ?>
				<?php
				$displayposts = new WP_Query();
				$displayposts->query('post');
				while ($displayposts->have_posts()) : $displayposts->the_post(); ?>
				<?php $count++; ?>
				<?php if ($count<9) : ?>
					<a href="<?php the_permalink(); ?>">
						<div class="newsItem">
							<?php if(get_field('hoofdafbeelding')): ?>
								<div class="newsItemImage <?php the_field('uitlijnen_afbeelding'); ?>" style="background-image: url(<?php the_field('hoofdafbeelding'); ?>);"></div>
							<?php else: ?>
							
								<?php if (in_category('jeugd') ) : ?>
									<div class="newsItemImage <?php the_field('uitlijnen_afbeelding_jeugd', 'option'); ?>" style="background-image: url(<?php the_field('jeugd', 'option'); ?>);"></div>
								<?php endif; ?>
								<?php if (in_category('sponsoren') ) : ?>
									<div class="newsItemImage <?php the_field('uitlijnen_afbeelding_sponsoren', 'option'); ?>" style="background-image: url(<?php the_field('sponsoren', 'option'); ?>);"></div>
								<?php endif; ?>
								<?php if (in_category('senioren') ) : ?>
									<div class="newsItemImage <?php the_field('uitlijnen_afbeelding_senioren', 'option'); ?>" style="background-image: url(<?php the_field('senioren', 'option'); ?>);"></div>
								<?php endif; ?>
								<?php if (in_category('1e-selectie') ) : ?>
									<div class="newsItemImage <?php the_field('uitlijnen_afbeelding_1e-selectie', 'option'); ?>" style="background-image: url(<?php the_field('1e_selectie', 'option'); ?>);"></div>
								<?php endif; ?>
								<?php if (in_category('vrijwilligers') ) : ?>
									<div class="newsItemImage <?php the_field('uitlijnen_afbeelding_vrijwilligers', 'option'); ?>" style="background-image: url(<?php the_field('vrijwilligers', 'option'); ?>);"></div>
								<?php endif; ?>
								<?php if (in_category('wedstrijdprogrammas') ) : ?>
									<div class="newsItemImage <?php the_field('uitlijnen_afbeelding_wedstrijdprogrammas', 'option'); ?>" style="background-image: url(<?php the_field('wedstrijdprogrammas', 'option'); ?>);"></div>
								<?php endif; ?>
								<?php if (in_category('interactief') ) : ?>
									<div class="newsItemImage <?php the_field('uitlijnen_afbeelding_interactief', 'option'); ?>" style="background-image: url(<?php the_field('interactief', 'option'); ?>);"></div>
								<?php endif; ?>
								<?php if (in_category('algemeen') ) : ?>
									<div class="newsItemImage <?php the_field('uitlijnen_afbeelding_algemeen', 'option'); ?>" style="background-image: url(<?php the_field('algemeen', 'option'); ?>);"></div>
								<?php endif; ?>
								
							<?php endif; ?> 
								<h3 class="newsTitle"><?php echo get_the_title(); ?></h3>
								<h6 class="newsDate"><?php the_time( get_option( 'date_format' ) ); ?></h6>
						</div>
					</a>				<?php else : ?>
				<?php endif; ?>
				<?php endwhile; ?>
				<?php wp_reset_query(); ?>
				</div>
			
		</div>
	</div>

	
	<?php include 'footer.php';?>
