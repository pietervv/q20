<?php
/*
Template Name: Sponsors
*/
?>

<?php get_header(); ?>
    
    <div class="MainTitle">
	    <div class="container ContainerMainTitle">
		    <div class="col-xs-12">
		    	<h1 class="Title">Sponsoren</h1>
		    </div>
	    </div>
    </div>
    
	<?php if (has_post_thumbnail( $post->ID ) ): ?>
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
<div class="MainPhoto" style="background-image: url('<?php echo $image[0]; ?>');">
	    <div class="photo-overlay"></div>
</div>
    <?php endif; ?>
<div class="SponsorContentTop">
	<div class="container">
		<div class="col-lg-offset-1 col-lg-10 col-xs-12">
			<h1 class="SponsorTitle">Quick'20, Door passie tot veel in staat!</h1>
			<p class="SponsorText">
				<?php the_field('bovenste_tekst'); ?>
			</p>
		</div>
	</div>
</div> 

<div class="SponsorContentMid">
	<div class="container">
		<div class="col-lg-offset-1 col-lg-10 col-xs-12">
			<p>
				<?php the_field('middelste_tekst'); ?>
			</p>
				<?php
				if( have_rows('sponsor_redenen') ):
				while ( have_rows('sponsor_redenen') ) : the_row(); ?>
				
					<p class="SponsorsReasonsText">&nbsp; &nbsp; - <?php the_sub_field('reden'); ?></p>
				<?php
				endwhile;
				else :
				endif;
   				?>
		</div>
	</div>
</div>

<div class="SponsorContentBottom">
	<div class="container">
		<div class="col-lg-offset-1 col-lg-10 col-md-12">
			<div class="row">
				<div class="col-md-6 SponsorBottomText">

				<?php
				if( have_rows('uitstraling_naar') ):
				while ( have_rows('uitstraling_naar') ) : the_row(); ?>
				
					<p class="SponsorsReasonsText">&nbsp; &nbsp; - <?php the_sub_field('regel'); ?></p>
				<?php
				endwhile;
				else :
				endif;
   				?>
					
					<p><br>
						<?php the_field('onderste_tekst'); ?>
					</p>
				</div>
				<div class="col-md-6 SponsorVideo">
					<?php the_field('embed_youtube_link'); ?>
				</div>
			</div>
		</div>
 


			<div class="col-lg-offset-1 col-lg-11 col-xs-12">
				<h2>Sponsoren</h2>
			</div>
			<div class="col-lg-offset-1 col-lg-6 col-md-8 col-xs-12 hoofdsponsors">
				<div class="siers"><a target="_blank" href="<?php the_field('link_hoofdsponsor', 'option'); ?>"><img src="<?php the_field('hoofdsponsor', 'option'); ?>"></a></div>
				<div class="stersponsors">
					<a href="<?php the_field('link_partner_1', 'option'); ?>"><img src="<?php the_field('stersponsor_1', 'option'); ?>"></a>
					<a href="<?php the_field('link_partner_2', 'option'); ?>"><img src="<?php the_field('stersponsor_2', 'option'); ?>"></a>
					<a href="<?php the_field('link_partner_3', 'option'); ?>"><img src="<?php the_field('stersponsor_3', 'option'); ?>"></a>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 SponsorSlider">
				<ul id="scroller">
					<?php
					if( have_rows('logos_sponsoren', 'option') ):
					while ( have_rows('logos_sponsoren', 'option') ) : the_row(); ?>
						<li><a href="<?php the_sub_field('link_sponsor', 'option'); ?>" target="_blank" ><img src="<?php the_sub_field('sponsor_logo', 'option'); ?>"></a></li>
					<?php
					endwhile;
					else :
					endif;
   					?>

				</ul>  
			</div>
		</div>
	</div>
		
<?php include 'footer.php';?>