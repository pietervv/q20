<?php
/*
Template Name Posts: Fabri
Template Name: Fabri

*/
?>
<?php get_header(); ?>
	<?php
		$knvbId = $_GET['id'];
		global $wpdb;
		$team = $wpdb->get_row($wpdb->prepare("SELECT * FROM webservice__team WHERE knvb_id=%d", $knvbId), ARRAY_A);
		$staffStr = "'Ass.-trainer/coach', 'Hoofdcoach', 'Teammanager', 'Trainer', 'Trainer/coach', 'Verzorger', 'Verzorger', 'Assistent-scheidsrechter(club)', 'Standaard functie', 'Keeperstrainer'";
		$players = $wpdb->get_results($wpdb->prepare("SELECT first_name, infix, last_name, function FROM webservice__user u WHERE u.team = %d AND function NOT IN (" . $staffStr . ") GROUP BY u.knvb_code ORDER BY last_name, infix", $knvbId ), ARRAY_A);
		$picture = $wpdb->get_var($wpdb->prepare("SELECT value FROM webservice__team_data WHERE team_id = %d AND `key` = 'teampicture'", $knvbId ));
		$staff = $wpdb->get_results($wpdb->prepare("SELECT first_name, infix, last_name, function FROM webservice__user u WHERE u.team = %d AND function IN (" . $staffStr . ") GROUP BY u.knvb_code ORDER BY last_name, infix, first_name", $knvbId ), ARRAY_A);
		$newsQuery = strtolower($team["short_name"]);
		$displayposts = get_posts(['post_type' => 'post', 'posts_per_page' => 32, 'category_name' => $newsQuery]);
		$teamsArr = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` LIKE '%Fabri%' ORDER BY CHAR_LENGTH(short_name), short_name", ARRAY_A);
		$teams = array();
		foreach ($teamsArr as $t)
			$teams[strval($t["knvb_id"])] = $t;
		$res = $wpdb->get_results("SELECT * FROM webservice__fabri_results ORDER BY CHAR_LENGTH(round) ASC, round ASC", ARRAY_A);
		$results = array();
		foreach($res as $r) {
			$round = $r["round"] . ' - ' . $r["date"];
			$roundArr = $results[$round];
			if ($roundArr == null)
				$roundArr = array();
			$roundArr[] = $r;
			$results[$round] = $roundArr;
		}
		$standings = $wpdb->get_results("SELECT * FROM webservice__fabri_standings ORDER BY place ASC", ARRAY_A);
	?>
    <div class="MainTitle">
	    <div class="container ContainerMainTitle">
		    <div class="col-xs-12">
		    	<h1 class="Title"><?php echo $team["short_name"]; ?></h1>
		    </div>
	    </div>
    </div>
    
	<div class="TeamSingle">
		<div class="container">
			<div class="col-lg-offset-1 col-lg-10 col-md-12">
				<div class="row">
					<div class="col-md-3 col-xs-12 TeamSidebar">
						<ul class="ContentSwitch">
							<li class="SwitchItem active" value="Overview">Teaminfo</li>
							<?php if (sizeof($displayposts) > 0) { ?>
								<li class="SwitchItem" value="News">Nieuws</li>
							<?php } ?>
							<li class="SwitchItem" value="Results">Uitslagen</li>
							<li class="SwitchItem" value="Standings">Stand</li>
						</ul>
						
						<?php if( have_rows('logos_jeugdgroepen', 'option') ):
							while ( have_rows('logos_jeugdgroepen', 'option') ) : the_row(); 
								if ( get_sub_field('groepsnaam', 'option') == $team["group"]) { ?>
							<div class="row TeamSponsor">
								<div class="col-xs-12">
									<h2 class="black">Stersponsor</h2>
								</div>
								
									<a href="<?php the_sub_field('link_sponsor', 'option'); ?>" target="_blank" >
										<img src="<?php the_sub_field('logo_sponsor', 'option'); ?>">
									</a>
							</div>
						<?php }
						endwhile;
						else :
						endif;
	   					?>
					</div>
					<div class="col-md-9 col-xs-12 TeamContent">
						<div class="Content Overview">
							<?php if($picture != null): ?>
								<div id="teampicture" style="display: block;" class="row">
									<img style="width: 100%;" src="/wp-content/uploads/TeamPictures/<?php echo $picture ?>">
								</div>
							<?php endif; ?>
							<div class="TeamPictureDescription"><h6><?php the_field('beschrijving_teamfoto'); ?></h6></div>
							<div class="row OverviewContent">
								<div class="col-md-7 col-xs-12">
									<div class="row TeamRowTitle2">
										<div class="col-xs-12">
											<h2 class="black">Team</h2>
										</div>
									</div>
									<div class="TeamPlayers">
										<?php foreach ($players as $player) { ?>
										<div class="row TeamRowPlayer">
											<div class="col-md-7 col-xs-6 PlayerName"><?php echo $player["first_name"] . " " . $player["infix"] . " " . $player["last_name"] ?></div>
											<div class="col-md-5 col-xs-6 PlayerPosition"><?php echo $player["function"] ?></div>
										</div>
										<?php } ?>
									</div>
									<div class="row TeamRowTitle2 TitleSub">
										<div class="col-xs-12">
											<h2 class="black">Staf</h2>
										</div>
									</div>
									<div class="TeamPlayers">
									<?php foreach ($staff as $st) { ?>
										<div class="row TeamRowStaff">
											<div class="col-md-7 PlayerName"><?php echo $st["first_name"] . " " . $st["infix"] . " " . $st["last_name"]; ?></div>
											<div class="col-md-5 PlayerPosition"><?php echo $st["function"] ?></div>
										</div>
										<div class="row TeamRowStaff">
											<div class="col-md-7"><a href="mailto:<?php echo $st["email"] ?>"></a><?php echo $st["email"] ?></div>
											<div class="col-md-5 PlayerPhone"><?php echo $st["telephone"] ?></div>
										</div>
									<?php } ?>
									</div>
								</div>
								<?php $infoPosts = get_posts(['post_type' => 'post', 'posts_per_page' => 3, 'category_name' => $newsQuery]);
								if (sizeof($infoPosts) >= 1): ?>
								<div class="col-md-5 col-xs-12 TeamLatestNews">
									<div class="row">
										<div class="col-xs-12">
											<h2 class="black">Laatste Nieuws</h2>
										</div>
									</div>
									<?php foreach($infoPosts as $post) : ?>
										<a href="<?php the_permalink(); ?>">
										<div class="row TeamNews">
											<?php if(get_field('hoofdafbeelding')): ?>
												<div class="TeamInfoImageSmall" style="background-image: url(<?php the_field('hoofdafbeelding'); ?>);"></div>
											<?php else: ?>
												<div class="TeamInfoImageSmall" style="background-image: url(<?php the_field('jeugd', 'option'); ?>);"></div>
											<?php endif; ?>
											<div class="TeamNewsTitle"><?php echo get_the_title(); ?></div>
										</div>
										</a>
									<?php endforeach; ?>
								</div>
								<?php endif; ?>
							</div>
						</div>
						<div class="Content News">
							<div class="row TeamRowTitle2">
								<div class="col-xs-12">
									<h2 class="black">Nieuws</h2>
								</div>
								<div class="col-xs-12 NewsMessages">
									<div class="row row-news">
										<?php if (sizeof($displayposts) > 0) {
											$count = 0;
											foreach($displayposts as $post) : ?>
											<?php $count++; ?>
												<?php if ($count < 13) : ?>
													<div class="col-md-6 col-left col-right Nieuwsbericht <?php $category = get_the_category($post->ID); echo $category[0]->slug; ?>">
														<a href="<?php the_permalink(); ?>">
															<div class="col-md-12 NewsMessage ">
																<?php if(get_field('hoofdafbeelding')): ?>
																	<div class="col-xs-12 NewsImageSmall" style="background-image: url(<?php the_field('hoofdafbeelding'); ?>);"></div>
																<?php else: ?>
																	<div class="col-xs-12 NewsImageSmall" style="background-image: url(<?php the_field('jeugd', 'option'); ?>);"></div>
																<?php endif; ?>
																<h3 class="newsTitle"><?php echo get_the_title(); ?></h3>
																<h6 class="newsDate"><?php the_time( get_option( 'date_format' ) ); ?></h6>
															</div>
														</a>
													</div>
												<?php endif; ?>
											<?php endforeach; 
										} ?>
										</div>
									</div>
								</div>
							</div>
							<div class="Content Results">
								<div class="row TeamRowTitle">
									<div class="col-xs-12">
										<div class="row"><h2 class="black">Uitslagen</h2></div>
										<?php foreach ($results as $round => $res_per_round) { ?>
											<h3 class="RowTitle">Speelronde <?php echo $round ?></h3>
											<div id="standings" class="row">
												<table id="resultsTable" class="KnvbTable ResultsTable">
													<tr class="TitleRow">
														<th>veld</th>
														<th>thuisploeg</th>
														<th>uitploeg</th>
														<th>uitslag</th>
														<th>toto</th>
													</tr>
												 	<?php foreach ($res_per_round as $result) { ?>
													 	<tr class='DataRow'>
													 		<td><?php echo $result["field"] ?></td>
															<td><?php echo $teams[strval($result["home_id"])]["short_name"] ?></td>
															<td><?php echo $teams[strval($result["away_id"])]["short_name"] ?></td>
															<td><?php if ($result["toto"] != '0') echo ($result["home_score"] . ' - ' . $result["away_score"]) ?></td>
															<td><?php if ($result["toto"] != '0') echo $result["toto"] ?></td>
													 	</tr>
													<?php } ?>
												</table>
											</div>
										<?php } ?>
									</div>
								</div>
							</div>
							<div class="Content Standings">
								<div class="row TeamRowTitle">
									<div class="col-xs-12">
										<div class="row"><h2 class="black">Stand</h2></div>
										<div id="standings" class="row standings">
											<table id="standingsTable" class="KnvbTable StandingsTable">
												<tr class="TitleRow">
													<th>#</th>
													<th>Team</th>
													<th>G</th>
													<th>W</th>
													<th>GL</th>
													<th>V</th>
													<th>Pnt</th>
													<th>DS</th>
												</tr>
												<?php foreach ($standings as $st) { ?>
													<tr class='DataRow'>
														<td><?php echo $st["place"] ?></td>
														<td><?php echo $teams[strval($st["team_id"])]["short_name"] ?></td>
														<td><?php echo $st["games"] ?></td>
														<td><?php echo $st["win"] ?></td>
														<td><?php echo $st["draw"] ?></td>
														<td><?php echo $st["lose"] ?></td>
														<td><?php echo $st["points"] ?></td>
														<td><?php echo $st["goals_diff"] ?></td>
													</tr>
												<?php } ?>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 

	<?php include 'footer.php';?>
	<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.q20.teams.20190607.js"></script>
	<?php
		// 
	?>