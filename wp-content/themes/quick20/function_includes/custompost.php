<?php
add_action( 'init', 'register_cpt_teams' );
function register_cpt_teams() {

    $labels = array( 
        'name' => _x( 'Teams', 'teams' ),
        'singular_name' => _x( 'Teams', 'teams' ),
        'add_new' => _x( 'Voeg toe', 'teams' ),
        'add_new_item' => _x( 'Nieuw Team', 'teams' ),
        'edit_item' => _x( 'Bewerk Team', 'teams' ),
        'new_item' => _x( 'Nieuw Team', 'teams' ),
        'view_item' => _x( 'Bekijk Teams', 'teams' ),
        'search_items' => _x( 'Doorzoek Teams', 'teams' ),
        'not_found' => _x( 'Geen Teams gevonden', 'teams' ),
        'not_found_in_trash' => _x( 'Geen Teams in de prullenbak', 'teams' ),
        'parent_item_colon' => _x( 'Bovenliggende Teams', 'teams' ),
        'menu_name' => _x( 'Teams', 'teams' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor', 'thumbnail' ),
        
        'public' => false,
        'show_ui' => true,
        'show_in_menu' => false,
        'menu_position' => 5,
        
        'show_in_nav_menus' => false,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );


function my_cpt_post_types( $post_types ) {
    $post_types[] = 'teams';
    return $post_types;
}
add_filter( 'cpt_post_types', 'my_cpt_post_types' );


    register_post_type( 'teams', $args );
    
}






add_action( 'init', 'register_cpt_kalender' );
function register_cpt_kalender() {

    $labels = array( 
        'name' => _x( 'Agenda', 'kalender' ),
        'singular_name' => _x( 'Kalender', 'kalender' ),
        'add_new' => _x( 'Voeg toe', 'kalender' ),
        'add_new_item' => _x( 'Nieuw Agenda-item', 'kalender' ),
        'edit_item' => _x( 'Bewerk Agenda-item', 'kalender' ),
        'new_item' => _x( 'Nieuw Agenda-item', 'kalender' ),
        'view_item' => _x( 'Bekijk Agenda', 'kalender' ),
        'search_items' => _x( 'Doorzoek kalender', 'kalender' ),
        'not_found' => _x( 'Geen agenda-item gevonden', 'kalender' ),
        'not_found_in_trash' => _x( 'Geen agenda-items in de prullenbak', 'kalender' ),
        'parent_item_colon' => _x( 'Bovenliggende agenda', 'kalender' ),
        'menu_name' => _x( 'Agenda', 'kalender' ),
    );

    $args = array( 
        'labels' => $labels,
        'hierarchical' => false,
        
        'supports' => array( 'title', 'editor', 'thumbnail' ),
        
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        
        'show_in_nav_menus' => true,
        'publicly_queryable' => true,
        'exclude_from_search' => false,
        'has_archive' => true,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => true,
        'capability_type' => 'post'
    );


    register_post_type( 'kalender', $args );
    
    
}

add_filter( 'manage_edit-kalender_columns', 'my_edit_kalender_columns' ) ;

function my_edit_kalender_columns( $columns ) {

	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __( 'Titel' ),
		'datum' => __( 'Evenement datum' ),
		'date' => __( 'Datum' )
	);
	

	return $columns;
}


add_action( 'manage_kalender_posts_custom_column', 'my_manage_kalender_columns', 10, 2 );

function my_manage_kalender_columns( $column, $post_id ) {
	global $post;

	switch( $column ) {

		case 'datum' :

			$datum = get_post_meta( $post_id, 'datum', true );

			if ( empty( $datum) )
				echo __( '' );

			else
				printf( $datum );

			break;
		default :
			break;
	}
}


add_filter( 'manage_edit-kalender_sortable_columns', 'my_kalender_sortable_columns' );

function my_kalender_sortable_columns( $columns ) {

	$columns['datum'] = 'datum';

	return $columns;
}


add_action( 'load-edit.php', 'my_edit_movie_load' );

function my_edit_movie_load() {
	add_filter( 'request', 'my_sort_movies' );
}

function my_sort_movies( $vars ) {

	if ( isset( $vars['post_type'] ) && 'kalender' == $vars['post_type'] ) {

		if ( isset( $vars['orderby'] ) && 'datum' == $vars['orderby'] ) {

			$vars = array_merge(
				$vars,
				array(
					'meta_key' => 'datum',
					'orderby' => 'meta_value'
				)
			);
		}
	}

	return $vars;
}
