<?php

add_role( 'manage_fabri', __('Beheer Fabri' ), array(
	'read' => true
));
add_role( 'manage_team', __('Beheer team' ), array(
	'read' => true,
	'team_edit' => true
));
add_role( 'manage_jubileum', __('Beheer jubileum' ), array(
	'read' => true
));

@ini_set( 'upload_max_size' , '2M' );
@ini_set( 'post_max_size', '2M');
@ini_set( 'max_execution_time', '300' );

function get_the_twitter_excerpt(){
	$excerpt = get_field('content_nieuwsbericht');
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$the_str = substr($excerpt, 0, 140);
	return $the_str;
}

if( function_exists('acf_add_options_sub_page') ) {

	acf_add_options_sub_page('Contactgegevens');
	acf_add_options_sub_page('Wedstrijdinformatie');
	acf_add_options_sub_page('Sponsoren');
	acf_add_options_sub_page('Standaard Nieuws afbeeldingen');
	acf_add_options_sub_page('Jubileum');
}
	
	require_once('wp_bootstrap_navwalker.php');
	
	register_nav_menus( array(
    'primary-menu' => __( 'Primary Menu', 'Quick 20' ),
) );
	
add_theme_support( 'post-thumbnails' );

require_once('function_includes/custompost.php')

?>