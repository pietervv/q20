<?php
/*
Template Name: Nieuwspagina
*/
?>
<?php get_header(); ?>
<?php if (have_posts()) : ?>
    <div class="slider my-slider">
	    <ul>
		    
			<?php
				
				
			if(isset($_GET['cat']) && ! empty($_GET['cat'])) {
					$currentCategory = sanitize_text_field($_GET['cat']);
				} else {
					$currentCategory = null;
				}
				
			$displayposts = new WP_Query();
			$displayposts->query('post');
			while ($displayposts->have_posts()) : $displayposts->the_post(); ?>
			
			<?php 
						
						$values = get_field('in_slider');
						if(is_array($values) && in_array("ja", $values)) {
						
					?>
			<?php $count = 0; ?>	
			<?php $count++; ?>
			<?php if ($count<6) : ?>
				<li>
			    <?php if(get_field('hoofdafbeelding')): ?>
				    <div class="slider-container <?php the_field('uitlijnen_afbeelding'); ?>" style="background-image: url(<?php the_field('hoofdafbeelding'); ?>);">
				<?php else: ?>

						<?php if (in_category('algemeen') ) : ?>
							<div class="slider-container <?php the_field('uitlijnen_afbeelding_algemeen', 'option'); ?>" style="background-image: url(<?php the_field('algemeen', 'option'); ?>);">
						<?php endif; ?>
						<?php if (in_category('interactief') ) : ?>
							<div class="slider-container <?php the_field('uitlijnen_afbeelding_interactief', 'option'); ?>" style="background-image: url(<?php the_field('interactief', 'option'); ?>);">
						<?php endif; ?>
						<?php if (in_category('wedstrijdprogrammas') ) : ?>
							<div class="slider-container <?php the_field('uitlijnen_afbeelding_wedstrijdprogrammas', 'option'); ?>" style="background-image: url(<?php the_field('wedstrijdprogrammas', 'option'); ?>);">
						<?php endif; ?>
						<?php if (in_category('vrijwilligers') ) : ?>
							<div class="slider-container <?php the_field('uitlijnen_afbeelding_vrijwilligers', 'option'); ?>" style="background-image: url(<?php the_field('vrijwilligers', 'option'); ?>);">
						<?php endif; ?>
						<?php if (in_category('1e-selectie') ) : ?>
							<div class="slider-container <?php the_field('uitlijnen_afbeelding_1e-selectie', 'option'); ?>" style="background-image: url(<?php the_field('1e_selectie', 'option'); ?>);">
						<?php endif; ?>
						<?php if (in_category('senioren') ) : ?>
							<div class="slider-container <?php the_field('uitlijnen_afbeelding_senioren', 'option'); ?>" style="background-image: url(<?php the_field('senioren', 'option'); ?>);">
						<?php endif; ?>
						<?php if (in_category('sponsoren') ) : ?>
							<div class="slider-container <?php the_field('uitlijnen_afbeelding_sponsoren', 'option'); ?>" style="background-image: url(<?php the_field('sponsoren', 'option'); ?>);">
						<?php endif; ?>
						<?php if (in_category('jeugd') ) : ?>
							<div class="slider-container <?php the_field('uitlijnen_afbeelding_jeugd', 'option'); ?>" style="background-image: url(<?php the_field('jeugd', 'option'); ?>);">
						<?php endif; ?>

				<?php endif; ?> 	    
					    
					    <div class="slider-overlay"></div>
					    <div class="container">
						    <div class="col-lg-offset-1 col-md-8 slider-caption">
							    <h1><?php echo get_the_title(); ?></h1>
							    <div class="slideCaptionLine"></div>
							    <p><?php echo ''.get_the_twitter_excerpt(); ?>...</p>
							    <a href="<?php the_permalink(); ?>"><button class="btn btn-readmore">Lees meer</button></a>
						    </div>
					    </div>
			    	</div>
			    </li>
	    	  <?php else : ?>
						<?php endif; //end-count ?>
				    <?
				    } //end-select
				    ?>
			<?php endwhile; ?>
			<?php endif; ?>
			<?php wp_reset_query(); ?>
	    </ul>
    </div>

    

<div class="NewsPage">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-lg-offset-1 col-lg-10">
				<div class="col-lg-5 col-md-4 col-sm-12">
					<h2>Nieuwste berichten</h2>
				</div>
				<div class="filters">
					
				
		
					
					<div class="collapse navbar-collapse filter-menu" id="bs-example-navbar-collapse-2">
				        <ul class="nav navbar-nav">
					        <li class="dropdown">
					          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
						          <?php if($currentCategory === null) : ?>
						          	Filter laatste Nieuws
						          <?php else :?>
						          	Filter: <?= get_category_by_slug($currentCategory )->name; ?>					     
						          <?php endif; ?>
						          <span class="caret"></span></a>
					          <ul class="dropdown-menu">
						        <li class="filter <?= ($currentCategory == null ? 'active': ''); ?> all"><a href="?cat=">Alles</a></li>
					            <li class="filter <?= ($currentCategory == 'algemeen' ? 'active': ''); ?> algemeen"><a href="?cat=algemeen">Algemeen</a></li>
					            <li class="filter <?= ($currentCategory == '1e-selectie' ? 'active': ''); ?> 1e-selectie"><a href="?cat=1e-selectie">1e Selectie</a></li>
					            <li class="filter <?= ($currentCategory == 'senioren' ? 'active': ''); ?> senioren"><a href="?cat=senioren">Senioren</a></li>
					            <li class="filter <?= ($currentCategory == 'jeugd' ? 'active': ''); ?> jeugd"><a href="?cat=jeugd">Jeugd</a></li>
					            <li class="filter <?= ($currentCategory == 'vrijwilligers' ? 'active': ''); ?> vrijwilligers"><a href="?cat=vrijwilligers">Vrijwilligers</a></li>
					            <li class="filter <?= ($currentCategory == 'sponsoren' ? 'active': ''); ?> sponsoren"><a href="?cat=sponsoren">Sponsoren</a></li>
					            <li class="filter <?= ($currentCategory == 'interactief' ? 'active': ''); ?> interactief"><a href="?cat=interactief">Interactief</a></li>
					            <li class="filter <?= ($currentCategory == 'wedstrijdprogrammas' ? 'active': ''); ?> wedstrijdprogrammas"><a href="?cat=wedstrijdprogrammas">Wedstrijdprogrammas</a></li>
					          </ul>
					        </li>
					        <!--<li class="dropdown">
					          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">December 2015<span class="caret"></span></a>
					          <ul class="dropdown-menu">
					            <li><a href="#">Januari 2016</a></li>
					            <li><a href="#">Februari 2016</a></li>
					            <li><a href="#">Maart 2016</a></li>
					            <li><a href="#">April 2016</a></li>
					            <li><a href="#">Mei 2016</a></li>
					          </ul>
					        </li>-->
				        </ul>
		  			</div>
				</div>
			</div>
		</div>
		<div class="col-lg-offset-1 col-lg-10 col-md-12 NewsMessages">
			<div class="row row-news">

				<?php
				//als de get variable cat bestaat, en als die niet leeg is,
				//dan tonen we enkel de items uit die categorie
				if($currentCategory !== null) {
					$displayposts = get_posts(['post_type' => 'post', 'posts_per_page' => 32, 'category_name' => $currentCategory]);
				} else {
					$displayposts = get_posts(['post_type' => 'post', 'posts_per_page' => 32]);
				}

				$count = 0;
				foreach($displayposts as $post) : ?>
				<?php $count++; ?>
					<?php if ($count < 13) : ?>
								
					<div class="col-md-4 col-left col-right Nieuwsbericht <?php $category = get_the_category($post->ID); echo $category[0]->slug; ?>">	
							<a href="<?php the_permalink(); ?>">
								<div class="col-md-12 NewsMessage ">
									
									<?php if(get_field('hoofdafbeelding')): ?>
									<div class="col-xs-12 NewsImageSmall" style="background-image: url(<?php the_field('hoofdafbeelding'); ?>);"></div>
									<?php else: ?>
									
										<?php if (in_category('jeugd') ) : ?>
											<div class="col-xs-12 NewsImageSmall <?php the_field('uitlijnen_afbeelding_jeugd', 'option'); ?>" style="background-image: url(<?php the_field('jeugd', 'option'); ?>);"></div>
										<?php endif; ?>
										
										<?php if (in_category('sponsoren') ) : ?>
											<div class="col-xs-12 NewsImageSmall <?php the_field('uitlijnen_afbeelding_sponsoren', 'option'); ?>" style="background-image: url(<?php the_field('sponsoren', 'option'); ?>);"></div>
										<?php endif; ?>
										
										<?php if (in_category('senioren') ) : ?>
											<div class="col-xs-12 NewsImageSmall <?php the_field('uitlijnen_afbeelding_senioren', 'option'); ?>" style="background-image: url(<?php the_field('senioren', 'option'); ?>);"></div>
										<?php endif; ?>
										
										<?php if (in_category('1e-selectie') ) : ?>
											<div class="col-xs-12 NewsImageSmall <?php the_field('uitlijnen_afbeelding_1e-selectie', 'option'); ?>" style="background-image: url(<?php the_field('1e_selectie', 'option'); ?>);"></div>
										<?php endif; ?>
										
										<?php if (in_category('vrijwilligers') ) : ?>
											<div class="col-xs-12 NewsImageSmall <?php the_field('uitlijnen_afbeelding_vrijwilligers', 'option'); ?>" style="background-image: url(<?php the_field('vrijwilligers', 'option'); ?>);"></div>
										<?php endif; ?>
										
										<?php if (in_category('wedstrijdprogrammas') ) : ?>
											<div class="col-xs-12 NewsImageSmall <?php the_field('uitlijnen_afbeelding_wedstrijdprogrammas', 'option'); ?>" style="background-image: url(<?php the_field('wedstrijdprogrammas', 'option'); ?>);"></div>
										<?php endif; ?>
										
										<?php if (in_category('interactief') ) : ?>
											<div class="col-xs-12 NewsImageSmall <?php the_field('uitlijnen_afbeelding_interactief', 'option'); ?>" style="background-image: url(<?php the_field('interactief', 'option'); ?>);"></div>
										<?php endif; ?>
										
										<?php if (in_category('algemeen') ) : ?>
											<div class="col-xs-12 NewsImageSmall <?php the_field('uitlijnen_afbeelding_algemeen', 'option'); ?>" style="background-image: url(<?php the_field('algemeen', 'option'); ?>);"></div>
										<?php endif; ?>
	
									<?php endif; ?> 
									
									
									<h3 class="newsTitle"><?php echo get_the_title(); ?></h3>
									<h6 class="newsDate"><?php the_time( get_option( 'date_format' ) ); ?></h6>
								</div>
							</a>
					</div>
					<?php endif; ?>
				<?php endforeach; ?>

			</div>
			
			<?php if(count($displayposts) > 12) : ?>
			
				<div class="col-md-12 OldNews">
					<h2>Ouder Nieuws</h2>
					<div class="col-xs-12 OldNewsCollums">
						<div class="col-md-7 col-sm-6 col-xs-12 OldNewsList">
							<?php
							$count = 0;
							foreach($displayposts as $post) : ?>
							<?php $count++; ?>
							<?php if ($count>12 && $count < 23) : ?>
								<p> &nbsp; <a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></p>			
							<?php endif; ?>
							<?php endforeach; ?>
						</div>
						<div class="col-md-5 col-sm-6 col-xs-12 OldNewsList">
							<?php
							$count = 0;
							foreach($displayposts as $post) : ?>
							<?php $count++; ?>
							<?php if ($count>22) : ?>
								<p> &nbsp; <a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></p>			
							<?php endif; ?>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			
			<?php endif ; ?>
		</div>
	</div>
</div>	

<?php include 'footer.php'; ?>