<?php
/*
Template Name: Organisatie
*/
?>

<?php get_header(); ?>
    
    <div class="MainTitle">
	    <div class="container ContainerMainTitle">
		    <div class="col-xs-12">
		    	<h1 class="Title DoubleTitle"><?php echo get_the_title(); ?></h1>
		    </div>
	    </div>
    </div>
    
	
	<div class="OrganisatiePage">
		<div class="container">
			<div class="col-lg-offset-1 col-lg-10 col-xs-12">
				<div class="col-md-8 col-xs-12 col-left FotoOrganisatie" style="background-image: url(<?php the_field('foto_bestuur'); ?>);"></div>
				<div class="col-md-4 col-xs-12">
					<h5 class="NamesOrganisatie"><?php the_field('beschrijving_foto'); ?></h5>
				</div>
			</div>
			
			
			<div class="col-lg-offset-1 col-md-10 col-xs-12 Bestuur">
				<h2 class="black">Bestuur</h2>
			</div>
			<div class="col-lg-offset-1 col-md-10 col-xs-12 BestuurList">
				
   				
   				
   				<div class="row">
	   				<?php
	   				if( have_rows('bestuursleden') ):
	   				while ( have_rows('bestuursleden') ) : the_row(); ?>
		   				<div class="col-xs-12 col-md-6 BestuurMember">
			   				<div class="col-xs-4 col-sm-3 col-md-4 BestuurPhoto" style="background-image: url(<?php the_sub_field('foto'); ?>);"></div>
			   				<div class="col-xs-6 col-sm-7 col-md-6 BestuurDescription"><br>
				   				<?php the_sub_field('naam'); ?><br>
				   				<?php the_sub_field('functie'); ?><br>
				   				<a href="mailto:<?php the_sub_field('mailadres'); ?>"><?php the_sub_field('mailadres'); ?></a><br>
				   			</div>
		   				</div>
	   				<?php
					endwhile;
					else :
					endif;
	   				?>
   				</div>
   				
   				
   				
   				
   				
   				
   				
			</div>
		</div>
	</div>
	
	<div class="SecretariaatPage">
		<div class="container">
			<div class="col-lg-offset-1 col-lg-10 col-xs-12">
				<div class="row">
					<div class="col-md-6 col-xs-12">
						<div class="row">
							<div class="col-xs-12">
								<h2 class="secr-title">Verenigingssecretariaat</h2>
							</div>
						</div>
						<p>
							<?php the_field('wedstrijdsecretariaat'); ?>
						</p>
					</div>
					<div class="col-md-6">
						<div class="row">
							<div class="col-xs-12">
								<h2 class="secr-title">Wedstrijdsecretariaat</h2>
							</div>
						</div>
						<p>
							<?php the_field('Verenigingssecretariaat'); ?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>

		<?php include 'footer.php';?>