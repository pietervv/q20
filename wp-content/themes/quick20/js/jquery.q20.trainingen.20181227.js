jQuery(document).ready(function($) {
	var getData = function() {
		$.ajax({
			url: urlBase + "/webservice/cdTrainingOverview",
			type: "GET",
			dataType: "json",
			success: function(data) {
				displayPractice(data)
			}
		});
	}
	
	var displayPractice = function(data) {
		$.each(data, function(dayNr, trainings) {
			var dayDiv = $(".DayContainer DIV.Template").clone();
			$(".DayContainer").append(dayDiv);
			dayDiv.find('SPAN.DayHeader').html(getDayOfWeek(dayNr));
			var rows = "";
			$.each(trainings, function() {
				var row = "<tr>";
				row += "<td>" + this.start + " - " + this.end + "</td>";
				row += "<td>" + this.team + "</td>";
				row += "<td>" + this.locatie + "</td>";
				row += "<td>" + this.kleedkamer + "</td>";
				row += "<td class='HideSmall'>" + this.opmerkingen + "</td>";
				row += "</tr>";
				rows += row;
			});
			dayDiv.find("TABLE").append(rows);
			dayDiv.removeClass("Template");
		});
	}
	
	getData();
	
	function getDayOfWeek(dayNr) {
		return isNaN(dayNr) ? null : ['Zondag', 'Maandag', 'Dinsdag', 'Woensdag', 'Donderdag', 'Vrijdag', 'Zaterdag'][dayNr];
	}
});