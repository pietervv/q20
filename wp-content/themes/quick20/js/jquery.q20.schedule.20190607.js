var matchDetails = {};

jQuery(document).ready(function($){

/***
	schedule
***/
	var getData = function(week) {
		$.ajax({
			url: urlBase + "/webservice/cdSchedule",
			type: "POST",
			data: { "week" : week },
			dataType: "json",
			success: function(data) {
				displaySchedule(data, week)
			}
		});
	}
	
	var displayResults = function(data) {
		$.each(data, function(idx, match) {
			var id = match["wedstrijdcode"];
			var result = match["uitslag"];
			var td = $("#result" + id);
			td.html(result);
		});
	}
	
	var displaySchedule = function(data, week) {
		var rows = "";
		var canceledRows = "";
		$.each(data["matchDays"], function(matchDay, matches) {
			$("TABLE.SchedulePageTable TR.fallback").remove();
			$.each(matches, function(index, game) {
				var matchId = game["matchId"];
				matchDetails[matchId] = game;
				var cat = game["displCategory"];
				var linkCat = game["category"] == 'Senioren' ? "senioren" : "jeugd";
				var rowClass = "DataRow " + cat;
				var compType = game['compType'];
				var compTypeShort = game['compTypeShort'];
				//var canceled = game["info"] != null && (game["info"] == "AFG" || game["info"] == "ADO" || game["info"] == "ADB");
				var canceled = game["status"] != null && game["status"].indexOf("Afgelast") >= 0;
				if (canceled) {
					rowClass += " cancelled";
					compType = "Afgelast";
					compTypeShort = "Afg";
				}
				if (game['result'] != null)
					compTypeShort = '<b>' + game['result'] + '</b>'; 
				row = "<tr class='Clear MatchRow " + rowClass + "'";
				//row += "' data-toggle='modal' data-target='#MatchDetailsModal' data-matchid='" + matchId + "'";
				var shortTime = matchDay + "<span class='LineBreak'></span>" + game["time"];
				row += "><td class='zero HideLarge'>" + shortTime + "</td>";
				row += "<td class='one HideSmall'>" + matchDay + "</td>";
				row += "<td class='two HideSmall'>" + game["time"] + "</td>";
				if (game["quickHome"])
					row += "<td class='three Quick'><a href='/teams/" + linkCat + "?id=" + game["teamId"] + "&ref=programma'>" + game["home"] + "</a></td>";
				else
					row += "<td class='three'>" + game["home"] + "</td>";
				if (!game["quickHome"])
					row += "<td class='four Quick'><a href='/teams/" + linkCat + "?id=" + game["teamId"] + "&ref=programma'>" + game["away"] + "</a></td>";
				else
					row += "<td four>" + game["away"] + "</td>";
				row += "<td class='five info' title='" + compType + "'>" + compTypeShort + "</td>";
				var imgBase = $("#imgBase").val();
				var img = "<img class='InfoImg_" + matchId + "' src='" + imgBase + 
					"/images/info.png'/><span class='Result_" + matchId + "'></span>";
				row += "<td class='six'><u class='MatchModal' data-toggle='modal' data-target='#MatchDetailsModal' data-id='" + matchId + "'>" + img + "</u></td>";
				row += "</tr>";
				rows += row;
				if (canceled)
					canceledRows += row;
			});
		});
		if (canceledRows != "") {
			$("TABLE.CanceledTable").append(canceledRows);
			$("TABLE.CanceledTable TD.info").remove();
			$("DIV.CanceledContainer").show();
			$("TABLE.CanceledTable").show();
		} else {
			$("TABLE.CanceledTable .HeaderRow").hide();
			$("TABLE.CanceledTable").append("<tr class='Clear'><td colspan='4'>Er zijn geen afgelastingen deze week.</td></tr>");
		}
		$("TABLE.SchedulePageTable").append(rows);
		$("TABLE.SchedulePageTable").show();
		$(".LoadingIcon").hide();
	};

	$("DIV.ProgrammaPage").each(function() {
		getData(0);	
	});
	
	$(".FilterSelection").click(function() {
		$(this).toggleClass("active");
		var clickedLI = $(this).parent();
		clickedLI.addClass("active");
		clickedLI.siblings("LI").removeClass("active");
		var text = $(this).text();
		$(this).parents("LI.dropdown").find(".FilterTitle").text(text);
		displayRows();
    });
    
    $("SELECT.WeekSelect").on("change", function() {
    	resetPage();
    	$(".LoadingIcon").show();
    	var selectedWeek = $(this).val();
    	getData(selectedWeek);
    });
    
    /***
		Match modal
	***/
	var displayModal = function(data) {
		var container = $('.modal-body');
		var game = data["details"];
		var basic = game["wedstrijdinformatie"];
		var parentMatch = matchDetails[basic["wedstijdnummerintern"]];
		
		var compData = parentMatch["compType"];
		compData += basic['klasse'] != null ? " " + basic['klasse'] : "";
		compData += (basic['poule'] != null && basic['poule'] != '-') ? " " + basic['poule'] : ""; 
		container.find('SPAN.CompType').html(compData);
		
		// Date + time
		container.find('SPAN.Date').html(formatFancyDate(basic["wedstrijddatum"]));
		container.find('SPAN.Time').html(basic["aanvangstijdopgemaakt"]);
		
		// Team names
		container.find('.HomeTeam .ClubTeamName').html(basic["thuisteam"]);
		container.find('.AwayTeam .ClubTeamName').html(basic["uitteam"]);
		
		// Logos
		var logoThuis = "http://bin617.website-voetbal.nl/sites/voetbal.nl/files/knvblogos_width35/" + game["thuisteam"]["code"] + ".png";
		var logoUit = "http://bin617.website-voetbal.nl/sites/voetbal.nl/files/knvblogos_width35/" + game["uitteam"]["code"] + ".png";
		container.find('.HomeTeam .ClubLogo').css('background-image', 'url(' + logoThuis + ')');
		container.find('.AwayTeam .ClubLogo').css('background-image', 'url(' + logoUit + ')');
		
		// Field
		if (basic["veldnaam"] != null && basic["veldnaam"] != '') {
			container.find('.MatchDetails .Field').show();
			container.find('.MatchDetails .Field .DetailData').html(basic["veldnaam"]);
		} else {
			container.find('.MatchDetails .Field').hide();
		}
		
		// Dressing rooms
		var kl = game["kleedkamers"]["thuis"];
		if (game["kleedkamers"]["thuis"] != null && game["kleedkamers"]["thuis"] != '') {
			container.find('.MatchDetails .HomeRoom').show();
			container.find('.MatchDetails .AwayRoom').show();
			container.find('.MatchDetails .HomeRoom .DetailData').html(game["kleedkamers"]["thuis"]);
			container.find('.MatchDetails .AwayRoom .DetailData').html(game["kleedkamers"]["uit"]);
		} else {
			container.find('.MatchDetails .HomeRoom').hide();
			container.find('.MatchDetails .AwayRoom').hide();
		}
		
		// Referee
		if (game["matchofficials"]["scheidsrechters"] != null && game["matchofficials"]["scheidsrechters"] != '') {
			container.find('.MatchDetails .Referee').show();
			var refStr = game["matchofficials"]["scheidsrechters"].replace(/\),/g, ')<br/>');
			refStr = refStr.replace(/Scheidsrechter/g, 'Sch.');
			refStr = refStr.replace(/scheidsrechter/g, 'sch.');
			refStr = refStr.replace(/Assistent/g, 'Ass.');
			refStr = refStr.replace(/assistent/g, 'ass.');
			container.find('.MatchDetails .Referee .DetailData').html(refStr);
		} else if (game["officials"]["verenigingsscheidsrechter"] != null && game["officials"]["verenigingsscheidsrechter"] != '') {
			container.find('.MatchDetails .Referee').show();
			container.find('.MatchDetails .Referee .DetailData').html(game["officials"]["verenigingsscheidsrechter"]);
		} else {
			container.find('.MatchDetails .Referee').hide();
		}
		
		// Location
		var locData = game["accommodatie"];
		var locStr = locData["naam"];
		locStr += "<br/>" + locData["straat"];
		locStr += "<br/>" + locData["plaats"];
		locStr += "<br/>" + locData["telefoon"];
		container.find('.MatchDetails .Location .DetailData').html(locStr);
		
		// Ranking
		if(game["ranking"] != null && game["ranking"].length > 3) {
			var ranking = game["ranking"];
			var nrOfTeams = ranking.length;
			var homeIndex = -1;
			var awayIndex = -1;
			for (var i = 0; i < nrOfTeams; i++) {
				var team = ranking[i];
				if (team["clubrelatiecode"] == game["thuisteam"]["code"])
					homeIndex = i;
				else if (team["clubrelatiecode"] == game["uitteam"]["code"])
					awayIndex = i;
			}
			var teamsToDisplay = [];
			teamsToDisplay[0] = ranking[0];
			teamsToDisplay[1] = ranking[Math.min(homeIndex, awayIndex)];
			teamsToDisplay[2] = ranking[Math.max(homeIndex, awayIndex)];
			teamsToDisplay[3] = ranking[nrOfTeams -1];
			if (homeIndex == 0 || awayIndex == 0) {
				teamsToDisplay[1] = ranking[1];
				if (homeIndex == 1 || awayIndex == 1)
					teamsToDisplay[2] = ranking[2];
			}
			if (homeIndex == nrOfTeams-1 || awayIndex == nrOfTeams-1) {
				teamsToDisplay[2] = ranking[nrOfTeams-2];
				if (homeIndex == nrOfTeams-2 || awayIndex == nrOfTeams-2)
					teamsToDisplay[1] = ranking[nrOfTeams-3];
			}
			$("DIV.Standings DIV.standing-row").each(function(index) {
				var team = teamsToDisplay[index];
				$(this).find("DIV.stand-team").html(team["positie"] + ". " + team["teamnaam"]);
				$(this).find("DIV.stand-games").html(team["gespeeldewedstrijden"]);
				$(this).find("DIV.stand-points").html(team["punten"]);
				if (team["clubrelatiecode"] == "BBKT65L")
					$(this).addClass("quick");
			});
			container.find('.Standings').show();
		} else {
			container.find('.Standings').hide();
		}
		$('.modal-dialog').show();
	};
	
	$('body').on('click', '.MatchModal', function() {
		$('.modal-dialog').hide();
		var matchId = $(this).data('id');
		$.ajax({
			url: urlBase + "/webservice/cdMatchDetails",
			type: "POST",
			data: { "matchId" : matchId },
			dataType: "json",
			success: displayModal
		});
	});
});

function getValues(selector, listOfAllValues) {
	var selected = $("UL." + selector + " LI.active").attr("value");
	var valueMap = [];
	if (selected == "All")
		return listOfAllValues;
	valueMap.push(selected);
	return valueMap;
}

function displayRows(tableClass) {
	$("TABLE.SchedulePageTable TR").not(".HeaderRow").hide();
	var groups = getValues("group", ["senior", "youth"]);
	var times = getValues("time", ["Weekend", "Week"]);
	var places = getValues("place", ["Home", "Away"]);
	for (var g = 0; g < groups.length; g++) {
		var group = groups[g];
		for (var t = 0; t < times.length; t++) {
			var time = times[t];
			for (var p = 0; p < places.length; p++) {
				var place = places[p];
				var selector = group + time + place;
				$("TABLE.SchedulePageTable TR." + selector).show();
			}
		}
	} 
}

var resetPage = function() {
	$(".Clear").remove();
};