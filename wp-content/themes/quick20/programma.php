<?php
/*
Template Name: Programma
*/
?>
<?php get_header(); ?>
<input type="hidden" id="imgBase" value="http://www.quick20.nl/wp-content/themes/quick20"></input>
<div class="MainTitle">
    <div class="container ContainerMainTitle">
	    <div class="col-xs-12">
	    	<h1 class="Title"><?php echo get_the_title(); ?></h1>
	    </div>
    </div>
</div>
<div class="ProgrammaPage">
	<div class="container">
		<div class="col-lg-offset-1 col-lg-10 col-xs-12">
			<div class="row title">
				<div class="CanceledContainer col-xs-12">
					<h2>Afgelastingen</h2>
					<table class="CanceledTable">
						<tr class="HeaderRow">
							<th class="zero HideLarge">Datum</th>
							<th class="one HideSmall">Datum</th>
							<th class="two HideSmall">Tijd</th>
							<th class="three">Thuisploeg</th>
							<th class="four">Uitploeg</th>
							<th class="five HideSmall">Soort</th>
							<th class="six"></th>
						</tr>
					</table>
				</div>
			</div>
			<div class="row title">
				<div class="col-xs-12">
					<h2>Wedstrijdinformatie</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12">
						<?php
						if( have_rows('kleedkamerindeling', 'option') ):
						while ( have_rows('kleedkamerindeling', 'option') ) : the_row(); ?>
							<p><a href="<?php the_sub_field('pdf', 'option'); ?>" target="_blank"><?php the_sub_field('naam', 'option'); ?></a></p>
						<?php
						endwhile;
						else :
						endif;
   					?>
   					<p><a href="/quick20/ledeninfo/info-voor-leiderstrainers/">Wedstrijdinformatie jeugd</a></p> 
				</div>
			</div>
			<div class="row title">
				<div class="col-xs-12">
					<h2>Programma 
						<select class="WeekSelect">
							<?php
								$currWeek = date("W");
								$date = new DateTime;
								$startYear = $currWeek <= 25 ? date("Y") - 1 : date("Y");
							    $date->setISODate($startYear, 53);
							    $lastWeekOfYear = ($date->format("W") === "53" ? 53 : 52);
								for ($week=26; $week<=$lastWeekOfYear; $week++) {
									$selected = $week == $currWeek;
									$year = $startYear;
									$optionStr = "<option value='" . $week . "' curr='" . $selected . "'";
									if ($selected)
										$optionStr = $optionStr . " selected";
									$optionStr = $optionStr . ">week " . $week . " - " . $year . "</option>";
									echo $optionStr;
								}
								for ($week=1; $week<=25; $week++) {
									$selected = $week == $currWeek;
									$year = $startYear + 1;
									$optionStr = "<option value='" . $week . "' curr='" . $selected . "'";
									if ($selected)
										$optionStr = $optionStr . " selected";
									$optionStr = $optionStr . ">week " . $week . " - " . $year . "</option>";
									echo $optionStr;
								}
							?>
						</select>
					</h2>
				</div>
			</div>
			
	  		<div class="row">
				<div class="col-xs-12">
					<h3>Maak hieronder je keuze uit de volgende filters:</h3>
					<div class="col-md-4">
						<div class="filters">
							<div class="collapse navbar-collapse filter-menu" id="bs-example-navbar-collapse-2">
								<ul class="nav navbar-nav">
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
											<span class="FilterTitle">Jeugd & senioren</span>
										</a>
										<ul class="dropdown-menu group">
											<li class="filter active" value="All"><a class="FilterSelection">Jeugd & senioren</a></li>
											<li class="filter" value="youth"><a class="FilterSelection">Jeugd</a></li>
											<li class="filter" value="senior"><a class="FilterSelection">Senioren</a></li>
							          </ul>
							        </li>
						        </ul>
				  			</div>
			  			</div>
					</div>
					<div class="col-md-4">
						<div class="filters">
							<div class="collapse navbar-collapse filter-menu" id="bs-example-navbar-collapse-2">
								<ul class="nav navbar-nav">
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
											<span class="FilterTitle">Weekend & week</span>
										</a>
										<ul class="dropdown-menu time">
											<li class="filter active" value="All"><a class="FilterSelection">Weekend & week</a></li>
											<li class="filter" value="Weekend"><a class="FilterSelection">Weekend</a></li>
											<li class="filter" value="Week"><a class="FilterSelection">Week</a></li>
							          </ul>
							        </li>
						        </ul>
				  			</div>
			  			</div>
					</div>
					<div class="col-md-4">
						<div class="filters">
							<div class="collapse navbar-collapse filter-menu" id="bs-example-navbar-collapse-2">
								<ul class="nav navbar-nav">
									<li class="dropdown">
										<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
											<span class="FilterTitle">Thuis & uit</span>
										</a>
										<ul class="dropdown-menu place">
											<li class="filter active" value="All"><a class="FilterSelection">Thuis & uit</a></li>
											<li class="filter" value="Home"><a class="FilterSelection">Thuis</a></li>
											<li class="filter" value="Away"><a class="FilterSelection">Uit</a></li>
							          </ul>
							        </li>
						        </ul>
				  			</div>
			  			</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="SchedulePage">
					<div class="col-xs-12">
						<table class="SchedulePageTable">
							<tr class="HeaderRow">
								<th class="zero HideLarge">Datum</th>
								<th class="one HideSmall">Datum</th>
								<th class="two HideSmall">Tijd</th>
								<th class="three">Thuisploeg</th>
								<th class="four">Uitploeg</th>
								<th class="five"></th>
								<th class="six"></th>
							</tr>
							<tr class="fallback"><td colspan="5">Geen wedstrijden gevonden</td></tr>
						</table>
						<div class="LoadingIcon"></div>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>

<div class="modal fade" id="MatchDetailsModal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="MatchDetailsModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4>Wedstrijddetails</h4>
			</div>
			<div class="modal-body">
				<div class="CompType">
					<span class="CompType"></span>
				</div>
				<div class="DateTime">
					<p><span class="Date"></span> - <span class="Time"></span> uur<br/></p>
				</div>
				<div class="Teams">
					<div class="col-xs-6 HomeTeam">
						<div class="row">
							<div class="col-xs-12 ClubLogo"></div>
							<div class="col-xs-12 ClubTeamName"></div>
						</div>
					</div>
					<div class="col-xs-6 AwayTeam">
						<div class="row">
							<div class="col-xs-12 ClubLogo"></div>
							<div class="col-xs-12 ClubTeamName"></div>
						</div>
					</div>
				</div>
				<div class="MatchDetails">
					<table>
						<tr class="Field">
							<td><span class="DetailLabel">Veld</span></td>
							<td><span class="DetailData"></span></td>
						</tr>
						<tr class="HomeRoom">
							<td><span class="DetailLabel">Kleedk. thuis</span></td>
							<td><span class="DetailData"></span></td>
						</tr>
						<tr class="AwayRoom">
							<td><span class="DetailLabel">Kleedk. uit</span></td>
							<td><span class="DetailData"></span></td>
						</tr>
						<tr class="Referee">
							<td><span class="DetailLabel">Scheidsrecht(s)</span></td>
							<td><span class="DetailData"></span></td>
						</tr>
						<tr class="Empty">
						</tr>
						<tr class="Location">
							<td><span class="DetailLabel">Accomodatie</span></td>
							<td><span class="DetailData"></span></td>
						</tr>
					</table>
				</div>
				<div class="col-md-12 Standings">
					<h5 class="DetailLabel">Huidige stand</h5>
					<div class="standing-row">
						<div class="col-xs-8 stand-team"></div>
						<div class="col-xs-2 stand-games"></div>
						<div class="col-xs-2 stand-points"></div>
					</div>
					<div class="standing-row">
						<div class="col-xs-8 stand-team"></div>
						<div class="col-xs-2 stand-games"></div>
						<div class="col-xs-2 stand-points"></div>

					</div>
					<div class="standing-row">
						<div class="col-xs-8 stand-team"></div>
						<div class="col-xs-2 stand-games"></div>
						<div class="col-xs-2 stand-points"></div>
					</div>
					<div class="standing-row">
						<div class="col-xs-8 stand-team"></div>
						<div class="col-xs-2 stand-games"></div>
						<div class="col-xs-2 stand-points"></div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Sluiten</button>
			</div>
		</div>
	</div>
</div>


<?php include 'footer.php'; ?>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery.q20.schedule.20190607.js"></script>