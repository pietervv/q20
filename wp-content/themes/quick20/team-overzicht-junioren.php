<?php
/*
Template Name: Team-overzicht - Junioren
*/
?>

<?php get_header(); ?>
    
    <div class="MainTitle">
	    <div class="container ContainerMainTitle">
		    <div class="col-xs-12">
		    	<h1 class="Title">Jeugdteams</h1>
		    </div>
	    </div>
    </div>
    
	
	<div class="TeamOverview">
		<div class="container">
			<div class="col-lg-offset-1 col-lg-10 col-md-12">
				<a name="boven"></a>
				<div class="col-md-2 col-md-20 col-xs-100 col-sm-6 TeamList">
					<div class="TeamListTitle">Meisjes</div>
					<?php
					$teams = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` LIKE '%Meiden%' ORDER BY CHAR_LENGTH(short_name), short_name", ARRAY_A);
					foreach ($teams as $team) {
						?><a href="../teams/jeugd?id=<?php echo $team['knvb_id']; ?>"><button class="col-md-12 btn btn-team btn-teamlist"><?php echo $team['short_name']; ?></button></a>
						<?php
					}
   					?>
				</div>
				<div class="col-md-2 col-md-20 col-xs-100 col-sm-6 TeamList">
					<div class="TeamListTitle">Onder 19</div>
   					<?php
					$teams = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` = 'Onder 19' ORDER BY CHAR_LENGTH(short_name), short_name", ARRAY_A);
					foreach ($teams as $team) {
						?><a href="../teams/jeugd?id=<?php echo $team['knvb_id']; ?>"><button class="col-md-12 btn btn-team btn-teamlist"><?php echo $team['short_name']; ?></button></a>
						<?php
					}
   					?>
				</div>
				<div class="col-md-2 col-md-20 col-xs-100 col-sm-6 TeamList">
					<div class="TeamListTitle">Onder 17</div>
					<?php
					$teams = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` = 'Onder 17' ORDER BY CHAR_LENGTH(short_name), short_name", ARRAY_A);
					foreach ($teams as $team) {
						?><a href="../teams/jeugd?id=<?php echo $team['knvb_id']; ?>"><button class="col-md-12 btn btn-team btn-teamlist"><?php echo $team['short_name']; ?></button></a>
						<?php
					}
   					?>
				</div>
				<div class="col-md-2 col-md-20 col-xs-100 col-sm-6 TeamList">
					<div class="TeamListTitle">Onder 15</div>
					<?php
					$teams = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` = 'Onder 15' ORDER BY CHAR_LENGTH(short_name), short_name", ARRAY_A);
					foreach ($teams as $team) {
						?><a href="../teams/jeugd?id=<?php echo $team['knvb_id']; ?>"><button class="col-md-12 btn btn-team btn-teamlist"><?php echo $team['short_name']; ?></button></a>
						<?php
					}
   					?>
				</div>
				<div class="col-md-2 col-md-20 col-sm-100 col-xs-12 TeamList">
					<div class="MoreInfo">Meer Info</div>
					<a href="<?php the_field('link_download_trainingschema'); ?>"><div class="btn-side btn-green">Download Trainingsschema</div></a>
					<a href="<?php the_field('link_lid_worden'); ?>"><div class="btn-side btn-red">Ik wil lid worden</div></a>
					<a href="<?php the_field('link_vrijwilliger_worden'); ?>"><div class="btn-side btn-white">Ik wil vrijwilliger worden</div></a>
					<a href="<?php the_field('link_sponsor_worden'); ?>"><div class="btn-side btn-black">Ik wil sponsor worden</div></a>
				</div>
			</div>
			
			<div class="col-lg-offset-1 col-lg-10 col-md-12">
				<a name="onder"></a>
				<div class="col-md-2 col-md-20 col-xs-100 col-sm-6 TeamList">
					<div class="TeamListTitle">Onder 13</div>
					<?php
					$teams = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` = 'Onder 13' ORDER BY CHAR_LENGTH(short_name), short_name", ARRAY_A);
					foreach ($teams as $team) {
						?><a href="../teams/jeugd?id=<?php echo $team['knvb_id']; ?>"><button class="col-md-12 btn btn-team btn-teamlist"><?php echo $team['short_name']; ?></button></a>
						<?php
					}
   					?>
				</div>
				<div class="col-md-2 col-md-20 col-xs-100 col-sm-6 TeamList">
					<div class="TeamListTitle">Onder 12</div>
					<?php
					$teams = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` = 'Onder 12' ORDER BY CHAR_LENGTH(short_name), short_name", ARRAY_A);
					foreach ($teams as $team) {
						?><a href="../teams/jeugd?id=<?php echo $team['knvb_id']; ?>"><button class="col-md-12 btn btn-team btn-teamlist"><?php echo $team['short_name']; ?></button></a>
						<?php
					}
   					?>
				</div>
				<div class="col-md-2 col-md-20 col-xs-100 col-sm-6 TeamList">
					<div class="TeamListTitle">Onder 11</div>
					<?php
					$teams = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` = 'Onder 11' ORDER BY CHAR_LENGTH(short_name), short_name", ARRAY_A);
					foreach ($teams as $team) {
						?><a href="../teams/jeugd?id=<?php echo $team['knvb_id']; ?>"><button class="col-md-12 btn btn-team btn-teamlist"><?php echo $team['short_name']; ?></button></a>
						<?php
					}
   					?>
				</div>
				<div class="col-md-2 col-md-20 col-xs-100 col-sm-6 TeamList">
					<div class="TeamListTitle">Onder 10</div>
					<?php
					$teams = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` = 'Onder 10' ORDER BY CHAR_LENGTH(short_name), short_name", ARRAY_A);
					foreach ($teams as $team) {
						?><a href="../teams/jeugd?id=<?php echo $team['knvb_id']; ?>"><button class="col-md-12 btn btn-team btn-teamlist"><?php echo $team['short_name']; ?></button></a>
						<?php
					}
   					?>
				</div>
			</div>
			<div class="col-lg-offset-1 col-lg-10 col-md-12">
				<a name="onder"></a>
				<div class="col-md-2 col-md-20 col-xs-100 col-sm-6 TeamList">
					<div class="TeamListTitle">Onder 9</div>
					<?php
					$teams = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` = 'Onder 9' ORDER BY CHAR_LENGTH(short_name), short_name", ARRAY_A);
					foreach ($teams as $team) {
						?><a href="../teams/jeugd?id=<?php echo $team['knvb_id']; ?>"><button class="col-md-12 btn btn-team btn-teamlist"><?php echo $team['short_name']; ?></button></a>
						<?php
					}
   					?>
				</div>
				<div class="col-md-2 col-md-20 col-xs-100 col-sm-6 TeamList">
					<div class="TeamListTitle">Fabri-League</div>
					<?php
					$teams = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` LIKE '%Fabri%' ORDER BY short_name", ARRAY_A);
					foreach ($teams as $team) {
						?><a href="../fabri?id=<?php echo $team['knvb_id']; ?>"><button class="col-md-12 btn btn-team btn-teamlist"><?php echo $team['short_name']; ?></button></a>
						<?php
					}
   					?>
				</div>
			</div>
		</div>
	</div>	


	<?php include 'footer.php';?>