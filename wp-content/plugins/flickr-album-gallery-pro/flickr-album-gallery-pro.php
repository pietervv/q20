<?php
/**
 * Plugin Name: Flickr Album Gallery Pro
 * Version: 5.2
 * Description: Flickr Album Gallery Pro fetch all your flickr account photo albums and you can publish individual or all flickr albums on your WrodPress site.
 * Author: Weblizar
 * Author URI: http://weblizar.com/plugins/flickr-gallery/
 * Plugin URI: http://weblizar.com/plugins/flickr-gallery/
 */
 
/**
 * Constant Variable
 */
define("FAGP_TEXT_DOMAIN", "weblizar_fag");
define("FAGP_PLUGIN_URL", plugin_dir_url(__FILE__));

/**
 * Flickr Album Gallery Pro Plugin Class
 */
 class FlickrAlbumGalleryPro {

	public function __construct() {
		if (is_admin()) {
			add_action('plugins_loaded', array(&$this, 'FAG_Translate'), 1);
			// 2
			add_action('init', array(&$this, 'FlickrAlbumGalleryPro_CPT'), 1);
			// 3
			add_action('add_meta_boxes', array(&$this, 'Add_all_fag_meta_boxes'));
            add_action('admin_init', array(&$this, 'Add_all_fag_meta_boxes'), 1);
			// 4
			add_action('save_post', array(&$this, 'Save_fag_meta_box_save'), 9, 1);
		}
	}
	
	/**
	 * Translate Plugin
	 */
	public function FAG_Translate() {
		load_plugin_textdomain('weblizar_fag', FALSE, dirname( plugin_basename(__FILE__)).'/lang/' );
	}	
	
	// 2 - Register Flickr Album Custom Post Type
	public function FlickrAlbumGalleryPro_CPT() {
		$labels = array(
			'name' => _x( 'Flickr Album Gallery Pro', 'fa_gallery' ),
			'singular_name' => _x( 'Flickr Album Gallery Pro', 'fa_gallery' ),
			'add_new' => _x( 'Add New Gallery', 'fa_gallery' ),
			'add_new_item' => _x( 'Add New Gallery', 'fa_gallery' ),
			'edit_item' => _x( 'Edit Photo Gallery', 'fa_gallery' ),
			'new_item' => _x( 'New Gallery', 'fa_gallery' ),
			'view_item' => _x( 'View Gallery', 'fa_gallery' ),
			'search_items' => _x( 'Search Galleries', 'fa_gallery' ),
			'not_found' => _x( 'No galleries found', 'fa_gallery' ),
			'not_found_in_trash' => _x( 'No galleries found in Trash', 'fa_gallery' ),
			'parent_item_colon' => _x( 'Parent Gallery:', 'fa_gallery' ),
			'all_items' => __( 'All Galleries', 'fa_gallery' ),
			'menu_name' => _x( 'Flickr Album Gallery Pro', 'fa_gallery' ),
		);

		$args = array(
			'labels' => $labels,
			'hierarchical' => false,
			'supports' => array( 'title', ),
			'public' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 10,
			'menu_icon' => 'dashicons-format-gallery',
			'show_in_nav_menus' => false,
			'publicly_queryable' => false,
			'exclude_from_search' => true,
			'has_archive' => true,
			'query_var' => true,
			'can_export' => true,
			'rewrite' => false,
			'capability_type' => 'post'
		);

        register_post_type( 'fa_gallery', $args );
		
		
		$DefaultFlickrSettingsProArray =  array(
		"flickr_api_key"   		=> "e54499be5aedef32dccbf89df9eaf921",
		"flickr_album_id"   		=> "72157645975425037",
		"flickr_Show_Title"   			=> "yes",
		"flickr_Light_Box"   	=> "lightbox1",
		"flickr_Gallery_Layout"   		=> "col-md-4",
		"flickr_Thumbnail_Size"   	=> "n",
		"flickr_Hover_Color"   	=> "#1E8CBE",
		"flickr_Hover_Color_Opacity"   		=> "0.5",
		"flickr_Hover_Animation"   		=> "fade",
		"flickr_Thumbnail_Border"			=> "yes",
		"flickr_Image_Limit"   		=> "30",
		"flickr_Thumb_Limit"   		=> "30",
		"flickr_Lazy_Loading"   			=> "45",
		"flickr_Custom_CSS"  		=> "",
	);
		add_option("Flickr_Pro_Default_Settings", $DefaultFlickrSettingsProArray);
		add_filter( 'manage_edit-fa_gallery_columns', array(&$this, 'fa_gallery_columns' )) ;
        add_action( 'manage_fa_gallery_posts_custom_column', array(&$this, 'fa_gallery_manage_columns' ), 10, 2 );
	}
	
	function fa_gallery_columns( $columns ){
        $columns = array(
            'cb' => '<input type="checkbox" />',
            'title' => __( 'Flickr Album Title' ),
            'shortcode' => __( 'Copy Flickr Album Shortcode' ),
            'date' => __( 'Date' )
        );
        return $columns;
    }
	
    function fa_gallery_manage_columns( $column, $post_id ){
        global $post;
        switch( $column ) {
          case 'shortcode' :
            echo '<input type="text" value="[FAGP id='.$post_id.']" readonly="readonly" />';
            break;
          default :
            break;
        }
    }
	
	// 3 - Meta Box Creator
	public function Add_all_fag_meta_boxes() {
		add_meta_box( __('Add Images', FAGP_TEXT_DOMAIN), __('Add Images', FAGP_TEXT_DOMAIN), array(&$this, 'fag_meta_box_form_function'), 'fa_gallery', 'normal', 'low' );
		add_meta_box ( __('Flickr Album Gallery Pro Shortcode', FAGP_TEXT_DOMAIN), __('Copy Album Gallery Shortcode', FAGP_TEXT_DOMAIN), array(&$this, 'fag_shortcode_meta_box_form_function'), 'fa_gallery', 'side', 'low');
		add_meta_box ( __('Flickr Album Gallery Pro Widget', FAGP_TEXT_DOMAIN), __('Flickr Album Gallery Pro Widget', FAGP_TEXT_DOMAIN), array(&$this, 'fag_shortcode_meta_box_widget_function'), 'fa_gallery', 'side', 'low');
    }
    
    
	/**
	 * Widget Box
	 */
    function fag_shortcode_meta_box_widget_function(){
		?>
		<div>
			<p><?php _e("To activate widget into any widget area", FAGP_TEXT_DOMAIN);?></p>
			<p><a href="<?php get_site_url();?>./widgets.php" ><?php _e("Click Here", FAGP_TEXT_DOMAIN);?></a>. </p>
			<p><?php _e("Find",FAGP_TEXT_DOMAIN)?> <b><?php _e("Flickr Album Gallery Pro Widget", FAGP_TEXT_DOMAIN);?></b> <?php _e("and place it to your widget area. Select any Shortcode for the dropdown and save changes.", FAGP_TEXT_DOMAIN);?></p>
		</div>
		<?php
	}    
	
	/**
	 * Shortcode Box
	 */
	public function fag_shortcode_meta_box_form_function() { ?>
		<p><?php _e("Use below shortcode in any Page/Post to publish your Flickr Album Gallery Pro", FAGP_TEXT_DOMAIN);?></p>
		<input readonly="readonly" type="text" value="<?php echo "[FAGP id=".get_the_ID()."]"; ?>"> <?php
	}
	
	/**
	 * Gallery Settings Meta Box Interface
	 */
	public function fag_meta_box_form_function($post) {
		// Add the color picker css file
		wp_enqueue_style( 'wp-color-picker' );
		// Custom jQuery with WordPress Color Picker dependency
		wp_enqueue_script( 'fagp-color-picker-script', FAGP_PLUGIN_URL.'js/fagp-color-picker.js', array( 'wp-color-picker' ), false, true );
		wp_enqueue_style('font-awesome-min-css', FAGP_PLUGIN_URL.'css/font-awesome-latest/css/font-awesome.min.css');
		require_once("flicker-album-gallery-pro-settings.php");
	}
	
	/**
	 * FAGP Meta Settings Save
	 */
	public function Save_fag_meta_box_save($PostID) {
		if(isset($_POST['flickr-api-key']) && isset($_POST['flickr-album-id'])) {
			$FAGP_API_KEY = trim($_POST['flickr-api-key']);
			$FAGP_Album_ID = trim($_POST['flickr-album-id']);
			$FAGP_Show_Title = $_POST['FAGP_Show_Title'];
			$FAGP_Light_Box = $_POST['FAGP_Light_Box'];
			$fa_gallery_Layout = $_POST['FAGP_Gallery_Layout'];
			$FAGP_Thumbnail_Size = $_POST['FAGP_Thumbnail_Size'];
			$FAGP_Hover_Color = $_POST['FAGP_Hover_Color'];
			$FAGP_Hover_Color_Opacity = $_POST['FAGP_Hover_Color_Opacity'];
			$FAGP_Hover_Animation = $_POST['FAGP_Hover_Animation'];
			$FAGP_Thumbnail_Border = $_POST['FAGP_Thumbnail_Border'];
			$FAGP_Image_Limit = $_POST['FAGP_Image_Limit'];
			$FAGP_Thumb_Limit = $_POST['FAGP_Thumb_Limit'];
			$FAGP_Lazy_Loading = $_POST['FAGP_Lazy_Loading'];
			$FAGP_Custom_CSS = $_POST['FAGP_Custom_CSS'];
			
			$FAGArray[] = array(
				'fag_api_key' => $FAGP_API_KEY,
				'fag_album_id' => $FAGP_Album_ID,
				'FAGP_Show_Title' => $FAGP_Show_Title,
				'FAGP_Light_Box' => $FAGP_Light_Box,
				'FAGP_Gallery_Layout' => $fa_gallery_Layout,
				'FAGP_Thumbnail_Size' => $FAGP_Thumbnail_Size,
				'FAGP_Hover_Color' => $FAGP_Hover_Color,
				'FAGP_Show_Title' => $FAGP_Show_Title,
				'FAGP_Hover_Color_Opacity' => $FAGP_Hover_Color_Opacity,
				'FAGP_Hover_Animation' => $FAGP_Hover_Animation,
				'FAGP_Thumbnail_Border' => $FAGP_Thumbnail_Border,
				'FAGP_Image_Limit' => $FAGP_Image_Limit,
				'FAGP_Thumb_Limit' => $FAGP_Thumb_Limit,
				'FAGP_Lazy_Loading' => $FAGP_Lazy_Loading,
				'FAGP_Custom_CSS' => $FAGP_Custom_CSS,
			);
			
			update_post_meta($PostID, 'fag_settings', serialize($FAGArray));
		}
	}
}// end of class

global $FlickrAlbumGalleryPro;
$FlickrAlbumGalleryPro = new FlickrAlbumGalleryPro();
require_once("flickr-album-gallery-widget.php"); 

/**
 * Flickr Album Gallery Pro Short Code [FAGP]
 */
require_once("flickr-album-gallery-pro-short-code.php");

/**
 * Hex Color code to RGB Color Code converter function
 */
if(!function_exists('FAGPhex2rgb')) {
    function FAGPhex2rgb($hex) {
       $hex = str_replace("#", "", $hex);

       if(strlen($hex) == 3) {
          $r = hexdec(substr($hex,0,1).substr($hex,0,1));
          $g = hexdec(substr($hex,1,1).substr($hex,1,1));
          $b = hexdec(substr($hex,2,1).substr($hex,2,1));
       } else {
          $r = hexdec(substr($hex,0,2));
          $g = hexdec(substr($hex,2,2));
          $b = hexdec(substr($hex,4,2));
       }
       $rgb = array($r, $g, $b);
       return $rgb; // returns an array with the rgb values
    }
}

/**
 * Documentation Page
 */
add_action('admin_menu' , 'FAGP_DOC_Menu_Function');
function FAGP_DOC_Menu_Function() {
	add_submenu_page('edit.php?post_type=fa_gallery', __('Default Settings', FAGP_TEXT_DOMAIN), __('Default Settings', FAGP_TEXT_DOMAIN), 'administrator', 'flickr-default-settings', 'FAG_Default_Setting_Function_weblizar');
	add_submenu_page('edit.php?post_type=fa_gallery', __('Need Help?', FAGP_TEXT_DOMAIN), __('Need Help?', FAGP_TEXT_DOMAIN), 'administrator', 'flickr-docs', 'FAG_DOC_Page_Function_weblizar');
	}
function FAG_Default_Setting_Function_weblizar(){
		// Add the color picker css file
		wp_enqueue_style( 'wp-color-picker' );
		// Custom jQuery with WordPress Color Picker dependency
		wp_enqueue_script( 'fagp-color-picker-script', FAGP_PLUGIN_URL.'js/fagp-color-picker.js', array( 'wp-color-picker' ), false, true );
		wp_enqueue_style('font-awesome-min-css', FAGP_PLUGIN_URL.'css/font-awesome-latest/css/font-awesome.min.css');
	require_once("flicker-album-gallery-pro-default-settings.php");
}


function FAG_DOC_Page_Function_weblizar(){ 
	wp_enqueue_script('bootstrap-min-js', FAGP_PLUGIN_URL.'js/bootstrap.min.js');
	wp_enqueue_script('weblizar-tab-js', FAGP_PLUGIN_URL .'js/option-js.js',array('jquery', 'media-upload', 'jquery-ui-sortable'));
	wp_enqueue_style('weblizar-option-style-css', FAGP_PLUGIN_URL .'css/weblizar-option-style.css');
	wp_enqueue_style('op-bootstrap-css', FAGP_PLUGIN_URL. 'css/bootstrap.css');
	wp_enqueue_style('weblizar-bootstrap-responsive-google', FAGP_PLUGIN_URL .'css/bootstrap-responsive.css');
	wp_enqueue_style('font-awesome-min-css', FAGP_PLUGIN_URL.'css/font-awesome-latest/css/font-awesome.min.css');
	wp_enqueue_style('Respo-pricing-table-css', FAGP_PLUGIN_URL .'css/pricing-table-responsive.css');
	wp_enqueue_style('pricing-table-css', FAGP_PLUGIN_URL .'css/pricing-table.css');
	require_once("flicker-album-gallery-pro-help.php");
}	
	
/**
 * Flicker Album Gallery Shortcode Button in Page/Post
 */
add_action('media_buttons_context', 'add_fagp_custom_button');
add_action('admin_footer', 'add_fagp_inline_popup_content');

function add_fagp_custom_button($context) {
  $img = plugins_url( '/img/flickr.png' , __FILE__ );
  $container_id = 'FAGP';
  $title = 'Select Flickr Album Gallery to insert into post';
  $context .= '<a class="button button-primary thickbox" title="Select portfolio gallery to insert into post" href="#TB_inline?width=400&inlineId='.$container_id.'">
	<span class="wp-media-buttons-icon" style="background: url('.$img.'); background-repeat: no-repeat; background-position: left bottom;"></span>
	Flicker Album Gallery Shortcode
	</a>';
  return $context;
}

function add_fagp_inline_popup_content() { ?>
	<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery('#flickr-gallery-insert').on('click', function() {
			var id = jQuery('#fagp-gallery-select option:selected').val();
			window.send_to_editor('<p>[FAGP id=' + id + ']</p>');
			tb_remove();
		})
	});
	</script>
	<div id="FAGP" style="display:none;">
	<h3><?php _e('Select Flickr Album Gallery to insert into post', FAGP_TEXT_DOMAIN); ?></h3>
	<?php 
		$all_posts = wp_count_posts( 'fa_gallery')->publish;
		$args = array('post_type' => 'fa_gallery', 'posts_per_page' =>$all_posts);
		global $fagp_galleries;
		$fagp_galleries = new WP_Query( $args );		
		if( $fagp_galleries->have_posts() ) { ?>
			<select id="fagp-gallery-select">
				<?php
				while ( $fagp_galleries->have_posts() ) : $fagp_galleries->the_post(); ?>
				<option value="<?php echo get_the_ID(); ?>"><?php the_title(); ?></option>
				<?php
				endwhile; 
				?>
			</select>
			<button class='button primary' id='flickr-gallery-insert'><?php _e('Insert Album Shortcode', FAGP_TEXT_DOMAIN); ?></button>
		<?php
		} else {
			_e('No Album Gallery Found', FAGP_TEXT_DOMAIN);
		} 
	?>
	</div>
	<?php
}
?>