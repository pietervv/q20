<?php 
	wp_enqueue_script('jquery');
	wp_enqueue_script( 'fagp-bootstrap-min-js', plugins_url('js/bootstrap.min.js', __FILE__ ), array('jquery'), false, true );
	wp_enqueue_script('fagp-hover-pack-js',FAGP_PLUGIN_URL.'js/hover-pack.js', array('jquery'));
	wp_enqueue_script( 'fagp-imagesloaded-pkgd-min-js', plugins_url('js/imagesloaded.pkgd.min.js', __FILE__ ), array('jquery'), false, true );
	wp_enqueue_script( 'fagp-jquery-blueimp-gallery-min-js', plugins_url('js/jquery.blueimp-gallery.min.js', __FILE__ ), array('jquery'), false, true );
	wp_enqueue_script( 'fagp-bootstrap-image-gallery-min-js', plugins_url('js/bootstrap-image-gallery.min.js', __FILE__ ), array('jquery'), false, true );

	if($FAGP_Light_Box == "lightbox1") {
		// 1- nivo js css
		wp_enqueue_style('fagp-nivo-lightbox-min-css', FAGP_PLUGIN_URL.'lightbox/nivo/nivo-lightbox.min.css');
		wp_enqueue_script('fagp-nivo-lightbox-min-js', FAGP_PLUGIN_URL.'lightbox/nivo/nivo-lightbox.min.js', array('jquery'));
	}
	
	if($FAGP_Light_Box == "lightbox2") {
		// 2 - photo box js css
		wp_enqueue_style('fagp-photobox-css', FAGP_PLUGIN_URL.'lightbox/photobox/photobox.css');
		wp_enqueue_script('fagp-photobox-js', FAGP_PLUGIN_URL.'lightbox/photobox/jquery.photobox.js', array('jquery'));
	}
	
	if($FAGP_Light_Box == "lightbox3") {
		//3 - pretty photo box js css
		wp_enqueue_style('fagp-pretty-css', FAGP_PLUGIN_URL.'lightbox/prettyphoto/prettyPhoto.css');
		wp_enqueue_script('fagp-pretty-js', FAGP_PLUGIN_URL.'lightbox/prettyphoto/jquery.prettyPhoto.js', array('jquery'));
	}
	
	if($FAGP_Light_Box == "lightbox4") {
		//window box js css
		wp_enqueue_style('fagp-windowbox-css', FAGP_PLUGIN_URL.'lightbox/windowbox/lightbox.css');
		wp_enqueue_script('fagp-windowbox-js', FAGP_PLUGIN_URL.'lightbox/windowbox/lightbox-2.6.min.js');
	}
	
	if($FAGP_Light_Box == "lightbox5") {
		//smooth box js css
		wp_enqueue_style('fagp-smoothbox-css', FAGP_PLUGIN_URL.'lightbox/smoothbox/smoothbox.css');
	}

	if($FAGP_Light_Box == "lightbox6") {
		//swipe box js css
		wp_enqueue_style('fagp-swipe-css', FAGP_PLUGIN_URL.'lightbox/swipebox/swipebox.css');
		wp_enqueue_script('fagp-swipe-js', FAGP_PLUGIN_URL.'lightbox/swipebox/jquery.swipebox.js');
	}
	
	if($FAGP_Light_Box == "lightbox7") {
		//ion box js css
		wp_enqueue_style('fagp-ionbox-css', FAGP_PLUGIN_URL.'lightbox/ionbox/ion.zoom.css');
	}

	if($FAGP_Light_Box == "lightbox8") {
		//fancy box js css
		wp_enqueue_style('fagp-fancybox-css', FAGP_PLUGIN_URL.'lightbox/fancybox/jquery.fancybox.css');
		wp_enqueue_script('fagp-fancybox-js', FAGP_PLUGIN_URL.'lightbox/fancybox/jquery.fancybox.js');		
	}	

	//CSS
	wp_enqueue_style('fagp-bootstrap-min-css', FAGP_PLUGIN_URL.'css/bootstrap.css');
	wp_enqueue_style('fagp-blueimp-gallery-min-css', FAGP_PLUGIN_URL.'css/blueimp-gallery.min.css');
	wp_enqueue_style('fagp-site-css', FAGP_PLUGIN_URL.'css/site.css');
	wp_enqueue_style('fagp-img-gallery-css', FAGP_PLUGIN_URL.'css/fagp-gallery.css');
	wp_enqueue_style('fagp-hover-css', FAGP_PLUGIN_URL.'css/hover-pack.css');			
?>