<div class="wrap" id="weblizar_wrap" >		
	<div id="content_wrap">			
		<div class="weblizar-header" >
			<h2><span><?php _e('Need Help About Flickr Album Gallery Pro?', FAGP_TEXT_DOMAIN); ?></span></h2>			
			<div class="weblizar-submenu-links" id="weblizar-submenu-links">
				<ul>
					<li class=""> <div class="dashicons dashicons-format-chat" > </div> <a href="http://wordpress.org/plugins/flickr-album-gallery/" target="_blank" title="Support Forum"><?php _e('Support Forum', FAGP_TEXT_DOMAIN); ?></a></li>
					<li class=""> <div class="dashicons dashicons-welcome-write-blog"></div> <a href="<?php echo FAGP_PLUGIN_URL.'readme.txt'?>" target="_blank" title="Theme Changelog"><?php _e('View Changelog', FAGP_TEXT_DOMAIN); ?></a></li>      
				</ul>
			</div>			
		</div>		
		<div id="content">
			<div id="options_tabs" class="ui-tabs ">
				<ul class="options_tabs ui-tabs-nav" role="tablist" id="nav">					
					<li class="active"><a href="#" id="general"><div class="dashicons dashicons-admin-home"></div><?php _e('Flickr Album Gallery Pro', FAGP_TEXT_DOMAIN);?></a></li>					
					</ul>					
				<?php require_once('help-body.php'); ?>
			</div>		
		</div>
		<div class="weblizar-header" style="margin-top:0px; border-radius: 0px 0px 6px 6px;">			
			<div class="weblizar-submenu-links" style="margin-top:15px;">				
			</div><!-- weblizar-submenu-links -->
		</div>
		<div class="clear"></div>
	</div>
</div>