<?php
/**
 * Load Saved Flickr Album Gallery settings
 */
$FAGP_Settings = unserialize(get_post_meta( $post->ID, 'fag_settings', true));
if(isset($FAGP_Settings[0]['fag_api_key']) && isset($FAGP_Settings[0]['fag_album_id'])) {
	$FAGP_API_KEY = $FAGP_Settings[0]['fag_api_key'];
	$FAGP_Album_ID = $FAGP_Settings[0]['fag_album_id'];
	$FAGP_Show_Title = $FAGP_Settings[0]['FAGP_Show_Title'];
	$FAGP_Light_Box = $FAGP_Settings[0]['FAGP_Light_Box'];
	$FAGP_Gallery_Layout = $FAGP_Settings[0]['FAGP_Gallery_Layout'];
	$FAGP_Thumbnail_Size = $FAGP_Settings[0]['FAGP_Thumbnail_Size'];
	$FAGP_Hover_Color = $FAGP_Settings[0]['FAGP_Hover_Color'];
	$FAGP_Hover_Color_Opacity = $FAGP_Settings[0]['FAGP_Hover_Color_Opacity'];
	$FAGP_Hover_Animation = $FAGP_Settings[0]['FAGP_Hover_Animation'];	
	$FAGP_Thumbnail_Border = $FAGP_Settings[0]['FAGP_Thumbnail_Border'];
	$FAGP_Image_Limit = $FAGP_Settings[0]['FAGP_Image_Limit'];
	$FAGP_Thumb_Limit = $FAGP_Settings[0]['FAGP_Thumb_Limit'];
	$FAGP_Lazy_Loading = $FAGP_Settings[0]['FAGP_Lazy_Loading'];
	$FAGP_Custom_CSS = $FAGP_Settings[0]['FAGP_Custom_CSS'];
} else {
	/**
	 * Load Saved Default Flickr Album Gallery settings
	 */
	$FAGP_Settings = get_option('fagp_default_settings');
	//print_r($FAGP_Settings);
	if(isset($FAGP_Settings['FAGP_API_KEY']) && isset($FAGP_Settings['FAGP_Album_ID'])) {
		$FAGP_API_KEY = $FAGP_Settings['FAGP_API_KEY'];
		$FAGP_Album_ID = $FAGP_Settings['FAGP_Album_ID'];
		$FAGP_Show_Title = $FAGP_Settings['FAGP_Show_Title'];
		$FAGP_Light_Box = $FAGP_Settings['FAGP_Light_Box'];
		$FAGP_Gallery_Layout = $FAGP_Settings['FAGP_Gallery_Layout'];
		$FAGP_Thumbnail_Size = $FAGP_Settings['FAGP_Thumbnail_Size'];
		$FAGP_Hover_Color = $FAGP_Settings['FAGP_Hover_Color'];
		$FAGP_Hover_Color_Opacity = $FAGP_Settings['FAGP_Hover_Color_Opacity'];
		$FAGP_Hover_Animation = $FAGP_Settings['FAGP_Hover_Animation'];	
		$FAGP_Thumbnail_Border = $FAGP_Settings['FAGP_Thumbnail_Border'];
		$FAGP_Image_Limit = $FAGP_Settings['FAGP_Image_Limit'];
		$FAGP_Thumb_Limit = $FAGP_Settings['FAGP_Thumb_Limit'];
		$FAGP_Lazy_Loading = $FAGP_Settings['FAGP_Lazy_Loading'];
		$FAGP_Custom_CSS = $FAGP_Settings['FAGP_Custom_CSS'];
	} else {
		$FAGP_API_KEY = "e54499be5aedef32dccbf89df9eaf921";
		$FAGP_Album_ID = "72157645975425037";
		$FAGP_Show_Title = "yes";
		$FAGP_Light_Box = "yes";
		$FAGP_Gallery_Layout = "col-md-4";
		$FAGP_Thumbnail_Size = "n";
		$FAGP_Hover_Color = "#1E8CBE";
		$FAGP_Hover_Color_Opacity = "0.5";
		$FAGP_Hover_Animation = "fade";
		$FAGP_Thumbnail_Border = "yes";
		$FAGP_Image_Limit = 30;
		$FAGP_Thumb_Limit = 30;
		$FAGP_Lazy_Loading = 45;
		$FAGP_Custom_CSS = '';
	}
}

?>
<table class="form-table">
	<tbody>
		<tr>
			<th scope="row"><label><?php _e("Flickr API Key", FAGP_TEXT_DOMAIN ); ?></label></th>
			<td>
				<input type="text" style="width:30%;" name="flickr-api-key" id="FAGP_API_KEY" value="<?php echo $FAGP_API_KEY; ?>"> <a title="Get your flickr account API Key"href="http://weblizar.com/get-flickr-api-key/" target="_blank"><?php _e("Get Your Flickr API Key", FAGP_TEXT_DOMAIN ); ?></a>
				<p class="description"><?php _e("Set your Flickr account API Key", FAGP_TEXT_DOMAIN ); ?>.</p>
			</td>
		</tr>
		
		<tr>
			<th scope="row"><label><?php _e("Enter Flickr Album ID", FAGP_TEXT_DOMAIN ); ?></label></th>
			<td>
				<input type="text" style="width:30%;" name="flickr-album-id" id="FAGP_Album_ID" value="<?php echo $FAGP_Album_ID; ?>"> <a title="Get your flickr photo Album ID" href="http://weblizar.com/get-flickr-album-id/" target="_blank"><?php _e("Get Your Flickr Album ID", FAGP_TEXT_DOMAIN ); ?></a>
				<p class="description"><?php _e("Set your Flickr Album ID", FAGP_TEXT_DOMAIN ); ?>.</p>
			</td>
		</tr>
		
		<tr>
			<th scope="row"><label><?php _e("Show Gallery Title", FAGP_TEXT_DOMAIN ); ?></label></th>
			<td>
				<input type="radio" name="FAGP_Show_Title" id="FAGP_Show_Title" value="yes" <?php if($FAGP_Show_Title == 'yes' ) echo "checked"; ?>>  <i class="fa fa-check fa-2x"></i>
				<input type="radio" name="FAGP_Show_Title" id="FAGP_Show_Title" value="no" <?php if($FAGP_Show_Title == 'no' ) echo "checked"; ?>>  <i class="fa fa-times fa-2x"></i>
				<p class="description"><?php _e("Choose Yes / No option to hide / show gallery title", FAGP_TEXT_DOMAIN ); ?>.</p>
			</td>
		</tr>
		
		<tr>
			<th scope="row"><label><?php _e("Light Box Styles", FAGP_TEXT_DOMAIN ); ?></label></th>
			<td>
				<select name="FAGP_Light_Box" id="FAGP_Light_Box">
					<optgroup label="Select Caption Position">
						<option value="lightbox1" <?php if($FAGP_Light_Box == 'lightbox1') echo "selected=selected"; ?>><?php _e("Nivo", FAGP_TEXT_DOMAIN ); ?></option>
						<option value="lightbox2" <?php if($FAGP_Light_Box == 'lightbox2') echo "selected=selected"; ?>><?php _e("Photo Box", FAGP_TEXT_DOMAIN ); ?></option>
						<option value="lightbox3" <?php if($FAGP_Light_Box == 'lightbox3') echo "selected=selected"; ?>><?php _e("Pretty Photo", FAGP_TEXT_DOMAIN ); ?></option>
						<option value="lightbox4" <?php if($FAGP_Light_Box == 'lightbox4') echo "selected=selected"; ?>><?php _e("Window Box", FAGP_TEXT_DOMAIN ); ?></option>
						<option value="lightbox5" <?php if($FAGP_Light_Box == 'lightbox5') echo "selected=selected"; ?>><?php _e("Smooth Box", FAGP_TEXT_DOMAIN ); ?></option>
						<option value="lightbox6" <?php if($FAGP_Light_Box == 'lightbox6') echo "selected=selected"; ?>><?php _e("Swipe Box", FAGP_TEXT_DOMAIN ); ?></option>
						<option value="lightbox7" <?php if($FAGP_Light_Box == 'lightbox7') echo "selected=selected"; ?>><?php _e("Ion Box", FAGP_TEXT_DOMAIN ); ?></option>
						<option value="lightbox8" <?php if($FAGP_Light_Box == 'lightbox8') echo "selected=selected"; ?>><?php _e("Fancy Box", FAGP_TEXT_DOMAIN ); ?></option>
					</optgroup>
				</select>
				<p class="description"><?php _e("Choose a image preview light box style", FAGP_TEXT_DOMAIN ); ?>.</p>
			</td>
		</tr>
		
		<tr>
			<th scope="row"><label><?php _e("Album Gallery Layout", FAGP_TEXT_DOMAIN ); ?></label></th>
			<td>
				<select name="FAGP_Gallery_Layout" id="FAGP_Gallery_Layout">
					<optgroup label="Select Gallery Layout">
						<!--<option value="col-md-12" <?php //if($FAGP_Gallery_Layout == 'col-md-12') echo "selected=selected"; ?>><?php _e("One Column", FAGP_TEXT_DOMAIN ); ?></option>-->
						<!--<option value="col-md-6" <?php //if($FAGP_Gallery_Layout == 'col-md-6') echo "selected=selected"; ?>><?php _e("Two Column", FAGP_TEXT_DOMAIN ); ?></option>-->
						<option value="col-md-4" <?php if($FAGP_Gallery_Layout == 'col-md-4') echo "selected=selected"; ?>><?php _e("Three Column", FAGP_TEXT_DOMAIN ); ?></option>
						<option value="col-md-3" <?php if($FAGP_Gallery_Layout == 'col-md-3') echo "selected=selected"; ?>><?php _e("Four Column", FAGP_TEXT_DOMAIN ); ?></option>
						<option value="col-md-5" <?php if($FAGP_Gallery_Layout == 'col-md-5') echo "selected=selected"; ?>><?php _e("Five Column", FAGP_TEXT_DOMAIN ); ?></option>
						<option value="col-md-2" <?php if($FAGP_Gallery_Layout == 'col-md-2') echo "selected=selected"; ?>><?php _e("Six Column", FAGP_TEXT_DOMAIN ); ?></option>
						<option value="col-md-8" <?php if($FAGP_Gallery_Layout == 'col-md-8') echo "selected=selected"; ?>><?php _e("Eight Column", FAGP_TEXT_DOMAIN ); ?></option>
						<option value="col-md-10" <?php if($FAGP_Gallery_Layout == 'col-md-10') echo "selected=selected"; ?>><?php _e("Ten Column", FAGP_TEXT_DOMAIN ); ?></option>
					</optgroup>
				</select>
				<p class="description"><?php _e("Choose a column layout for Flickr Album gallery", FAGP_TEXT_DOMAIN ); ?>.</p>
			</td>
		</tr>
		
		<tr>
			<th scope="row"><label><?php _e("Thumbnail Size", FAGP_TEXT_DOMAIN ); ?></label></th>
			<td>
				<select name="FAGP_Thumbnail_Size" id="FAGP_Thumbnail_Size">
					<optgroup label="Select Any Thumbnail Size">
						<option value="q"  <?php if($FAGP_Thumbnail_Size == 'q')  echo "selected=selected"; ?>><?php _e("Small  Thumbnail", FAGP_TEXT_DOMAIN ); ?></option>
						<option value="n"  <?php if($FAGP_Thumbnail_Size == 'n')  echo "selected=selected"; ?>><?php _e("Medium Thumbnail", FAGP_TEXT_DOMAIN ); ?></option>
						<option value="m"  <?php if($FAGP_Thumbnail_Size == 'm')  echo "selected=selected"; ?>><?php _e("Large Thumbnail", FAGP_TEXT_DOMAIN ); ?></option>
					</optgroup>
				</select>
				<p class="description"><?php _e("You can select a thumbnail size for your flickr album gallery images", FAGP_TEXT_DOMAIN ); ?>.</p>
			</td>
		</tr>
		
		<tr>
			<th scope="row"><label><?php _e("Hover Color", FAGP_TEXT_DOMAIN ); ?></label></th>
			<td>
				<input id="FAGP_Hover_Color" name="FAGP_Hover_Color" type="text" value="<?php echo $FAGP_Hover_Color; ?>" class="fagp-color-picker" data-default-color="#ffffff" />
				<p class="description"><?php _e("Choose a hover color effect on album image", FAGP_TEXT_DOMAIN ); ?>.</p>
			</td>
		</tr>

		<tr>
			<th scope="row"><label><?php _e("Hover Color Opacity", FAGP_TEXT_DOMAIN ); ?></label></th>
			<td>
				<select name="FAGP_Hover_Color_Opacity" id="FAGP_Hover_Color_Opacity">
					<optgroup label="Select Color Opacity">
						<option value="1" <?php if($FAGP_Hover_Color_Opacity == '1') echo "selected=selected"; ?>>1</option>
						<option value="0.9" <?php if($FAGP_Hover_Color_Opacity == '0.9') echo "selected=selected"; ?>>0.9</option>
						<option value="0.8" <?php if($FAGP_Hover_Color_Opacity == '0.8') echo "selected=selected"; ?>>0.8</option>
						<option value="0.7" <?php if($FAGP_Hover_Color_Opacity == '0.7') echo "selected=selected"; ?>>0.7</option>
						<option value="0.6" <?php if($FAGP_Hover_Color_Opacity == '0.6') echo "selected=selected"; ?>>0.6</option>
						<option value="0.5" <?php if($FAGP_Hover_Color_Opacity == '0.5') echo "selected=selected"; ?>>0.5</option>
						<option value="0.4" <?php if($FAGP_Hover_Color_Opacity == '0.4') echo "selected=selected"; ?>>0.4</option>
						<option value="0.3" <?php if($FAGP_Hover_Color_Opacity == '0.3') echo "selected=selected"; ?>>0.3</option>
						<option value="0.2" <?php if($FAGP_Hover_Color_Opacity == '0.2') echo "selected=selected"; ?>>0.2</option>
						<option value="0.1" <?php if($FAGP_Hover_Color_Opacity == '0.1') echo "selected=selected"; ?>>0.1</option>
						<option value="0" <?php if($FAGP_Hover_Color_Opacity == '0') echo "selected=selected"; ?>>0 - No Animation</option>
					</optgroup>
				</select>
				<p class="description"><?php _e("Choose hover color opacity on album images", FAGP_TEXT_DOMAIN ); ?>.</p>
			</td>
		</tr>
		
		<tr>
			<th scope="row"><label><?php _e("Image Hover Animation", FAGP_TEXT_DOMAIN ); ?></label></th>
			<td>
				<select name="FAGP_Hover_Animation" id="FAGP_Hover_Animation">
					<optgroup label="Select Animation">
						<option value="fade" <?php if($FAGP_Hover_Animation == 'fade') echo "selected=selected"; ?>>Fade</option>
						<option value="stroke" <?php if($FAGP_Hover_Animation == 'stroke') echo "selected=selected"; ?>>Stroke</option>
						<option value="flow" <?php if($FAGP_Hover_Animation == 'flow') echo "selected=selected"; ?>>Flow</option>
						<option value="box" <?php if($FAGP_Hover_Animation == 'box') echo "selected=selected"; ?>>Box</option>
						<option value="stripe" <?php if($FAGP_Hover_Animation == 'stripe') echo "selected=selected"; ?>>Stripe</option>
						<option value="apart-horisontal" <?php if($FAGP_Hover_Animation == 'apart-horisontal') echo "selected=selected"; ?>>Apart Horizontal</option>
						<option value="apart-vertical" <?php if($FAGP_Hover_Animation == 'apart-vertical') echo "selected=selected"; ?>>Apart Vertical</option>
						<option value="diagonal" <?php if($FAGP_Hover_Animation == 'diagonal') echo "selected=selected"; ?>>Diagonal</option>
					</optgroup>
				</select>
				<p class="description"><?php _e("Choose an animation effect apply on album images mouse hover.", FAGP_TEXT_DOMAIN ); ?></p>
			</td>
		</tr>
		
		<tr>
			<th scope="row"><label><?php _e("Show Thumbnail Border", FAGP_TEXT_DOMAIN ); ?></label></th>
			<td>
				<input type="radio" name="FAGP_Thumbnail_Border" id="FAGP_Thumbnail_Border" value="yes" <?php if($FAGP_Thumbnail_Border == 'yes' ) echo "checked"; ?>>  <i class="fa fa-check fa-2x"></i>
				<input type="radio" name="FAGP_Thumbnail_Border" id="FAGP_Thumbnail_Border" value="no" <?php if($FAGP_Thumbnail_Border == 'no' ) echo "checked"; ?>>  <i class="fa fa-times fa-2x"></i>
				<p class="description"><?php _e("Choose Yes / No option to hide / show gallery thumbnail border", FAGP_TEXT_DOMAIN ); ?>.</p>
			</td>
		</tr>
		
		<tr>
			<th scope="row"><label><?php _e("Fetch Image Limit", FAGP_TEXT_DOMAIN ); ?></label></th>
			<td>
				<input type="text" name="FAGP_Image_Limit" id="FAGP_Image_Limit" value="<?php echo $FAGP_Image_Limit; ?>">
				<p class="description"><?php _e("Fetch limited number of images from Flickr album, if album has too many images. Set a numeric value like: 10, 20, 25, 50, 100", FAGP_TEXT_DOMAIN ); ?></p>
			</td>
		</tr>
		
		<tr>
			<th scope="row"><label><?php _e("Show Thumbnail Limit", FAGP_TEXT_DOMAIN ); ?></label></th>
			<td>
				<input type="text" name="FAGP_Thumb_Limit" id="FAGP_Thumb_Limit" value="<?php echo $FAGP_Thumb_Limit; ?>">
				<p class="description"><?php _e("Show no. of thumbnails from fetched images, if there are too many fetched images, you can limit theme here. Set a numeric value like: 10, 20, 25, 50, 100", FAGP_TEXT_DOMAIN ); ?></p>
			</td>
		</tr>
		
		<tr>
			<th scope="row"><label><?php _e("Lazy Loading", FAGP_TEXT_DOMAIN ); ?></label></th>
			<td>
				<input type="text" name="FAGP_Lazy_Loading" id="FAGP_Lazy_Loading" value="<?php echo $FAGP_Lazy_Loading; ?>">
				<p class="description"><?php _e("Load your Flickr album images slowly slowly. Set a numeric value between 0-1000", FAGP_TEXT_DOMAIN ); ?></p>
			</td>
		</tr>

		<tr>
			<th scope="row"><label><?php _e('Custom CSS', FAGP_TEXT_DOMAIN); ?></label></th>
			<td>
				<textarea name="FAGP_Custom_CSS" id="FAGP_Custom_CSS" rows="5" cols="97"><?php echo $FAGP_Custom_CSS; ?></textarea>
				<p class="description">
					<?php _e('Enter any custom css you want to apply.', FAGP_TEXT_DOMAIN); ?>.<br>
					<?php _e('Note', FAGP_TEXT_DOMAIN); ?><strong>:</strong> <?php _e('Please Do Not Use', FAGP_TEXT_DOMAIN); ?> <strong>&lt;style&gt;...&lt;/style&gt;</strong> <?php _e('Tag With Custom CSS', FAGP_TEXT_DOMAIN); ?>.
				</p>
			</td>
		</tr>
	</tbody>
</table>