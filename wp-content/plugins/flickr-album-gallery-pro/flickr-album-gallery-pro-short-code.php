<?php
add_shortcode( 'FAGP', 'FlickerAlbumGalleryShortCodePro' );
function FlickerAlbumGalleryShortCodePro( $Id ) {
	ob_start();
	if(isset($Id['id'])) {
		/**
		 * Load All Flickr Album Gallery Pro Custom Post Type
		 */
		$FAG_CPT_Name = "fa_gallery";
		$AllGalleries = array(  'p' => $Id['id'], 'post_type' => $FAG_CPT_Name, 'orderby' => 'ASC', 'post_staus' => 'publish');
		$loop = new WP_Query( $AllGalleries );
			
		while ( $loop->have_posts() ) : $loop->the_post();
			/**
			 * Get All Photos from Gallery Details Post Meta
			 */
			$ID = get_the_ID();
			$fagp_Id = $ID;
			$FAG_Albums = unserialize(get_post_meta( $ID, 'fag_settings', true));
			foreach($FAG_Albums as $FAG_Album) {
				/**
				 * Load Saved Settings
				 */
				if(isset($FAG_Album['fag_api_key']) && isset($FAG_Album['fag_album_id'])  && isset($FAG_Album['FAGP_Show_Title'])  && isset($FAG_Album['FAGP_Light_Box'])   && isset($FAG_Album['FAGP_Gallery_Layout'])  && isset($FAG_Album['FAGP_Thumbnail_Size'])  && isset($FAG_Album['FAGP_Hover_Color'])    && isset($FAG_Album['FAGP_Hover_Color_Opacity'])    && isset($FAG_Album['FAGP_Hover_Animation'])    && isset($FAG_Album['FAGP_Thumbnail_Border'])  && isset($FAG_Album['FAGP_Image_Limit'])   && isset($FAG_Album['FAGP_Thumb_Limit'])  && isset($FAG_Album['FAGP_Lazy_Loading']) ) {
					$FAGP_API_KEY = trim($FAG_Album['fag_api_key']);
					$FAGP_Album_ID = trim($FAG_Album['fag_album_id']);
					$FAGP_Show_Title = $FAG_Album['FAGP_Show_Title'];
					$FAGP_Light_Box = $FAG_Album['FAGP_Light_Box'];
					$FAGP_Gallery_Layout = $FAG_Album['FAGP_Gallery_Layout'];
					$FAGP_Thumbnail_Size = $FAG_Album['FAGP_Thumbnail_Size'];
					$FAGP_Hover_Color = $FAG_Album['FAGP_Hover_Color'];
					$fagp_Hover_Color_Opacity = $FAG_Album['FAGP_Hover_Color_Opacity'];
					$FAGP_Hover_Animation = $FAG_Album['FAGP_Hover_Animation'];
					$FAGP_Thumbnail_Border = $FAG_Album['FAGP_Thumbnail_Border'];
					$FAGP_Image_Limit = $FAG_Album['FAGP_Image_Limit'];
					$FAGP_Thumb_Limit = $FAG_Album['FAGP_Thumb_Limit'];
					$FAGP_Lazy_Loading = $FAG_Album['FAGP_Lazy_Loading'];
					$FAGP_Custom_CSS = $FAG_Album['FAGP_Custom_CSS'];
				} else {
					/**
					 * Load Saved Default Flickr Album Gallery settings
					 */
					$FAGP_Settings = get_option('fagp_default_settings');
					//print_r($FAGP_Settings);
					if(isset($FAGP_Settings['FAGP_API_KEY']) && isset($FAGP_Settings['FAGP_Album_ID'])) {
						$FAGP_API_KEY = $FAGP_Settings['FAGP_API_KEY'];
						$FAGP_Album_ID = $FAGP_Settings['FAGP_Album_ID'];
						$FAGP_Show_Title = $FAGP_Settings['FAGP_Show_Title'];
						$FAGP_Light_Box = $FAGP_Settings['FAGP_Light_Box'];
						$FAGP_Gallery_Layout = $FAGP_Settings['FAGP_Gallery_Layout'];
						$FAGP_Thumbnail_Size = $FAGP_Settings['FAGP_Thumbnail_Size'];
						$FAGP_Hover_Color = $FAGP_Settings['FAGP_Hover_Color'];
						$FAGP_Hover_Color_Opacity = $FAGP_Settings['FAGP_Hover_Color_Opacity'];
						$FAGP_Hover_Animation = $FAGP_Settings['FAGP_Hover_Animation'];	
						$FAGP_Thumbnail_Border = $FAGP_Settings['FAGP_Thumbnail_Border'];
						$FAGP_Image_Limit = $FAGP_Settings['FAGP_Image_Limit'];
						$FAGP_Thumb_Limit = $FAGP_Settings['FAGP_Thumb_Limit'];
						$FAGP_Lazy_Loading = $FAGP_Settings['FAGP_Lazy_Loading'];
						$FAGP_Custom_CSS = $FAGP_Settings['FAGP_Custom_CSS'];
					} else {
						$FAGP_API_KEY = "e54499be5aedef32dccbf89df9eaf921";
						$FAGP_Album_ID = "72157645975425037";
						$FAGP_Show_Title = "yes";
						$FAGP_Light_Box = "yes";
						$FAGP_Gallery_Layout = "col-md-4";
						$FAGP_Thumbnail_Size = "n";
						$FAGP_Hover_Color = "#1E8CBE";
						$FAGP_Hover_Color_Opacity = "0.5";
						$FAGP_Hover_Animation = "fade";
						$FAGP_Thumbnail_Border = "yes";
						$FAGP_Image_Limit = 30;
						$FAGP_Thumb_Limit = 30;
						$FAGP_Lazy_Loading = 45;
						$FAGP_Custom_CSS = '';
					}
				}
				
			/**
				 * Default Settings
				 */
				if(!isset($FAGP_API_KEY)) {
					$FAGP_API_KEY = "e54499be5aedef32dccbf89df9eaf921";
				}
				 
				if(!isset($FAGP_Album_ID)) {
					$FAGP_Album_ID = "72157645975425037";
				}
				 
				if(!isset($FAGP_Show_Title)) {
					$FAGP_Show_Title = "yes";
				}

				if(!isset($FAGP_Light_Box)) {
					$FAGP_Light_Box = "yes";
				}

				if(!isset($FAGP_Gallery_Layout)) {
					$FAGP_Gallery_Layout = "col-md-6";
				}
				
				if(!isset($FAGP_Thumbnail_Size)) {
					$FAGP_Thumbnail_Size = "n";
				}
				 
				if(!isset($FAGP_Hover_Color)) {
					$FAGP_Hover_Color = "#1E8CBE";
				}
				 
				if(!isset($fagp_Hover_Color_Opacity)) {
					$fagp_Hover_Color_Opacity = "0.5";
				}
				
				if(!isset($FAGP_Hover_Animation)) {
					$FAGP_Hover_Animation = "fade";
				}
				 
				if(!isset($FAGP_Thumbnail_Border)) {
					$FAGP_Thumbnail_Border = "yes";
				}

				if(!isset($FAGP_Image_Limit)) {
					$FAGP_Image_Limit = 30;
				}
				if(!isset($FAGP_Thumb_Limit)) {
					$FAGP_Thumb_Limit = 30;
				}

				if(!isset($FAGP_Lazy_Loading)) {
					$FAGP_Lazy_Loading = 45;
				}
				if(!isset($FAGP_Custom_CSS)) {
					$FAGP_Custom_CSS = '';
				}
				
				//load lightbox scripts
				include("flickr-album-gallery-pro-short-code-script.php");
				
				/**
				 * Set Light Box Properties
				 */
				if($FAGP_Light_Box == "lightbox1") { // nivo
					$Properties1 = "'class': 'thumb img-thumbnail gall-img-responsive'";			//img tag
					$Properties2 = "'data-lightbox-gallery': 'enigma_lightbox', 'class': 'nivoz'";	//a tag
				}
				
				if($FAGP_Light_Box == "lightbox2") { // photo box
					$Properties1 = "'class': 'thumb img-thumbnail gall-img-responsive'"; //img tag
					$Properties2 = "'class': ''"; 										//a tag
				}
				
				if($FAGP_Light_Box == "lightbox3") { // pretty photo box
					$Properties1 = "'class': 'thumb img-thumbnail gall-img-responsive'"; 									//img tag
					$Properties2 = "'class': 'portfolio-zoom icon-resize-full', 'data-rel': 'prettyPhoto[portfolio]'";		//a tag
				}
				
				if($FAGP_Light_Box == "lightbox4") { // window box
					$Properties1 = "'class': 'thumb img-thumbnail gall-img-responsive'";		//img tag
					$Properties2 = "'data-lightbox': 'image', 'class': 'hover_thumb'"; 			//a tag
				}
				
				if($FAGP_Light_Box == "lightbox5") { // smooth box
					$Properties1 = "'class': 'thumb img-thumbnail gall-img-responsive'";	//img tag
					$Properties2 = "'class': 'sb'"; 										//a tag
				}
				
				if($FAGP_Light_Box == "lightbox6") { // swipe box
					$Properties1 = "'class': 'thumb img-thumbnail gall-img-responsive'";	//img tag
					$Properties2 = "'class': 'swipebox'";									//a tag
				}
				
				if($FAGP_Light_Box == "lightbox7") { // ion box
					 $Properties1 = "'class': 'thumb img-thumbnail gall-img-responsive'";	//img tag
					 $Properties2 = "'class': 'gallery_1'";									//a tag
				}
				
				if($FAGP_Light_Box == "lightbox8") { // fancy box
					$Properties1 = "'class': 'thumb img-thumbnail  gall-img-responsive'";		//img tag
					$Properties2 = "'class': 'fancybox', 'data-fancybox-group': 'gallery'";		//a tag				
				}

				
				$RGB = FAGPhex2rgb($FAGP_Hover_Color);
				$HoverColorRGB = implode(", ", $RGB);
				?>
				<style>
				.LoadingImg img {
					max-width: 45px;
					max-height: 45px;
					box-shadow:  none;
				}
				
				#fagp_<?php echo $fagp_Id; ?> .img-thumbnail {
					padding: <?php if($FAGP_Thumbnail_Border == "yes") echo "4px"; if($FAGP_Thumbnail_Border == "no") echo "0px"; ?>;
				}

				#fagp_<?php echo $fagp_Id; ?> .b-link-fade .b-wrapper,#fagp_<?php echo $fagp_Id; ?> .b-link-fade .b-top-line{
					background: rgba(<?php echo $HoverColorRGB; ?>, <?php echo $fagp_Hover_Color_Opacity; ?>);
				}
				#fagp_<?php echo $fagp_Id; ?> .b-link-flow .b-wrapper,#fagp_<?php echo $fagp_Id; ?> .b-link-flow .b-top-line{
					background: rgba(<?php echo $HoverColorRGB; ?>, <?php echo $fagp_Hover_Color_Opacity; ?>);
				}
				#fagp_<?php echo $fagp_Id; ?> .b-link-stroke .b-top-line{
					background: rgba(<?php echo $HoverColorRGB; ?>, <?php echo $fagp_Hover_Color_Opacity; ?>);
				}
				#fagp_<?php echo $fagp_Id; ?> .b-link-stroke .b-bottom-line{
					background: rgba(<?php echo $HoverColorRGB; ?>, <?php echo $fagp_Hover_Color_Opacity; ?>);
				}

				#fagp_<?php echo $fagp_Id; ?> .b-link-box .b-top-line{
					border: 16px solid rgba(<?php echo $HoverColorRGB; ?>, <?php echo $fagp_Hover_Color_Opacity; ?>);
				}
				#fagp_<?php echo $fagp_Id; ?> .b-link-box .b-bottom-line{
					background: rgba(<?php echo $HoverColorRGB; ?>, <?php echo $fagp_Hover_Color_Opacity; ?>);
				}
				#fagp_<?php echo $fagp_Id; ?> .b-link-stripe .b-line{
					background: rgba(<?php echo $HoverColorRGB; ?>, <?php echo $fagp_Hover_Color_Opacity; ?>);
				}
				#fagp_<?php echo $fagp_Id; ?> .b-link-apart-horisontal .b-top-line,#fagp_<?php echo $fagp_Id; ?> .b-link-apart-horisontal .b-top-line-up{
					background: rgba(<?php echo $HoverColorRGB; ?>, <?php echo $fagp_Hover_Color_Opacity; ?>);
				}
				#fagp_<?php echo $fagp_Id; ?> .b-link-apart-horisontal .b-bottom-line,#fagp_<?php echo $fagp_Id; ?> .b-link-apart-horisontal .b-bottom-line-up{
					background: rgba(<?php echo $HoverColorRGB; ?>, <?php echo $fagp_Hover_Color_Opacity; ?>);
				}
				#fagp_<?php echo $fagp_Id; ?> .b-link-apart-vertical .b-top-line,#fagp_<?php echo $fagp_Id; ?> .b-link-apart-vertical .b-top-line-up{
					background: rgba(<?php echo $HoverColorRGB; ?>, <?php echo $fagp_Hover_Color_Opacity; ?>);
				}
				#fagp_<?php echo $fagp_Id; ?> .b-link-apart-vertical .b-bottom-line,#fagp_<?php echo $fagp_Id; ?> .b-link-apart-vertical .b-bottom-line-up{
					background: rgba(<?php echo $HoverColorRGB; ?>, <?php echo $fagp_Hover_Color_Opacity; ?>);
				}
				#fagp_<?php echo $fagp_Id; ?> .b-link-diagonal .b-line{
					background: rgba(<?php echo $HoverColorRGB; ?>, <?php echo $fagp_Hover_Color_Opacity; ?>);
				}
				
				#fagp_<?php echo $fagp_Id; ?> .hidepics {
					display: none !important;
				}

				@media (min-width: 992px){
					#fagp_<?php echo $fagp_Id; ?> .col-md-6 {
					width: 49.97% !important;
					}
					#fagp_<?php echo $fagp_Id; ?> .col-md-4 {
					width: 33.30% !important;
					}
					#fagp_<?php echo $fagp_Id; ?> .col-md-3 {
					width: 24.90% !important;
					}
					#fagp_<?php echo $fagp_Id; ?> .col-md-2 {
					width: 16.60% !important;
					}
					#fagp_<?php echo $fagp_Id; ?> .col-md-5 {
					width: 20% !important;
					}
					#fagp_<?php echo $fagp_Id; ?> .col-md-8 {
					width: 12% !important;
					}
					#fagp_<?php echo $fagp_Id; ?> .col-md-10 {
					width: 10% !important;
					}
				}
				<?php echo $FAGP_Custom_CSS; ?>
				</style>
				<script type="text/javascript">
				jQuery(function() {
					// Engage gallery.
					jQuery('.gallery<?php echo $ID; ?>').flickr<?php echo $ID; ?>({
						apiKey: '<?php echo  $FAGP_API_KEY; ?>',
						photosetId: '<?php echo $FAGP_Album_ID; ?>',
						loadingSpeed: <?php echo $FAGP_Lazy_Loading; ?>,
						photosLimit: <?php echo $FAGP_Image_Limit; ?>
					});
				});
				
				/*
				* jQuery Flickr Photoset
				* https://github.com/hadalin/jquery-flickr-photoset
				*
				* Copyright 2014, Primož Hadalin
				*
				* Licensed under the MIT license:
				* http://www.opensource.org/licenses/MIT
				*/

				;(function (jQuery, window, document, undefined) {
					var fcount = 1;
				//	alert(fcount);
					'use strict';

					var pluginName = "flickr<?php echo $ID; ?>",
						defaults = {
							apiKey: "",
							photosetId: "",
							errorText: "Error generating gallery."
						},
						apiUrl = 'https://api.flickr.com/services/rest/',
						photos = [];

					// The actual plugin constructor
					function Plugin(element, options) {
						this.element = jQuery(element);
						this.settings = jQuery.extend({}, defaults, options);
						this._defaults = defaults;
						this._name = pluginName;

						this._hideSpinner = function() {
							this.element.find('.spinner-wrapper').hide().find('*').hide();
						};

						this._printError = function() {
							this.element.find('.gallery-container').append(jQuery("<div></div>", { "class": "col-lg-12 col-lg-offset-1" })
								.append(jQuery("<div></div>", { "class": "error-wrapper" })
									.append(jQuery("<span></span>", { "class": "label label-danger error" })
										.html(this.settings.errorText))));
						};

						this._flickrAnimate = function() {
							this.element.find('.gallery-container img').each(jQuery.proxy(function(index, el) {
								var image = el;
								setTimeout(function() {
									jQuery(image).parent().fadeIn();
								}, this.settings.loadingSpeed * index);
							}, this));
						};

						this._printGallery = function(photos) {
							var element = this.element.find('.gallery-container');
							
							jQuery.each(photos, function(key, photo) {
								var img = jQuery('<img>', { <?php echo $Properties1; ?>, src: photo.thumbnail, });
								element.append(jQuery('<div></div>', { 'class': '<?php echo $FAGP_Gallery_Layout; ?> col-sm-6 wl-gallery ' + photo.hideme })
								.append(jQuery('<div></div>', { 'class': 'b-link-<?php echo $FAGP_Hover_Animation; ?> b-animate-go' })
									.append(jQuery('<a></a>', { <?php echo $Properties2; ?>, title: photo.title, href: photo.href  }).hide()
									.append(img)
									.append(jQuery('<div></div>', { 'class': 'b-wrapper' })))));
							});

							element.imagesLoaded()
								.done(jQuery.proxy(this._flickrAnimate, this))
								.always(jQuery.proxy(this._hideSpinner, this));
						};

						this._flickrPhotoset = function(photoset) {
							var _this = this;
							var hidemeval = "";
							photos[photoset.id] = [];
							jQuery.each(photoset.photo, function(key, photo) {
								
								// hide thumbnails after a limit
								
								if(key > <?php echo $FAGP_Thumb_Limit-1; ?>) {
									hidemeval = "hidepics";
								}
								
								// Limit number of photos.
								if(key >= _this.settings.photosLimit) {
									return false;
								}

								photos[photoset.id][key] = {
									thumbnail: 'https://farm' + photo.farm + '.static.flickr.com/' + photo.server + '/' + photo.id + '_' + photo.secret + '_<?php echo $FAGP_Thumbnail_Size; ?>.jpg',
									href: 'https://farm' + photo.farm + '.static.flickr.com/' + photo.server + '/' + photo.id + '_' + photo.secret + '_b.jpg',
									title: photo.title,
									hideme: hidemeval									
								};
							});

							this._printGallery(photos[photoset.id]);
						};

						this._onFlickrResponse = function(response) {
							if(response.stat === "ok") {
								 this._flickrPhotoset(response.photoset);
							}
							else {
								this._hideSpinner();
								this._printError();
							}
						};

						this._flickrRequest = function(method, data) {
							var url = apiUrl + "?format=json&jsoncallback=?&method=" + method + "&api_key=" + this.settings.apiKey;

							jQuery.each(data, function(key, value) {
								url += "&" + key + "=" + value;
							});

							jQuery.ajax({
								dataType: "json",
								url: url,
								context: this,
								success: this._onFlickrResponse
							});
						};

						this._flickrInit = function () {
							this._flickrRequest('flickr.photosets.getPhotos', {
								photoset_id: this.settings.photosetId
							});
						};

						// Init
						this.init();
					}

					Plugin.prototype = {
						init: function () {
							this._flickrInit();
						}
					};

					// Wrapper
					jQuery.fn[pluginName] = function (options) {
						this.each(function () {
							if (!jQuery.data(this, "plugin_" + pluginName)) {
								jQuery.data(this, "plugin_" + pluginName, new Plugin(this, options));
							}
						});

						// Chain
						return this;
					};
				})(jQuery, window, document);
				</script>
				<!-- Gallery Thumbnails -->
				<div style="margin-bottom: 25px; padding-top: 20px; overflow: auto; display: block;">
					<?php if($FAGP_Show_Title == "yes") { ?>
					<h3 style="font-size: 18px; border-bottom: 2px solid #F1F1F1; text-align:center;"><?php echo ucwords(get_the_title($ID)); ?></h3>
					<?php } ?>
					<div class="gallery<?php echo $ID; ?>" id="fagp_<?php echo $ID; ?>">
						<div class="col-xs-12 spinner-wrapper">
							<div class="LoadingImg"><img src="<?php echo FAGP_PLUGIN_URL."img/loading.gif"; ?>" /></div>
						</div>
						<?php if($FAGP_Light_Box == "lightbox8") { ?>
						<div style="">
						<?php } ?>
							<div  class="gallery-container <?php if($FAGP_Light_Box == "lightbox2"){ echo "photobox-lightbox"; } ?> <?php if($FAGP_Light_Box != "lightbox8") { echo "gallery1" ;} ?>"></div>
						<?php if($FAGP_Light_Box == "lightbox8") { ?>
						</div>
						<?php } ?>
					</div>
				</div>
				<?php
			}// end of foreach
		endwhile;
		
		if(!$FAGP_Light_Box) { ?>
		<!-- Blueimp gallery -->
		<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
			<div class="slides"></div>
			<h3 class="title"></h3>
			<a class="prev">‹</a>
			<a class="next">›</a>
			<a class="close">×</a>
			<a class="play-pause"></a>
			<ol class="indicator"></ol>
			<div class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" aria-hidden="true">&times;</button>
							<h4 class="modal-title"></h4>
						</div>
						<div class="modal-body next"></div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left prev">
								<i class="glyphicon glyphicon-chevron-left"></i>
								<?php _e("Previous", FAGP_TEXT_DOMAIN ); ?>
							</button>
							<button type="button" class="btn btn-primary next">
								<?php _e("Next", FAGP_TEXT_DOMAIN ); ?>
								<i class="glyphicon glyphicon-chevron-right"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
		jQuery(function() {
			// Set blueimp gallery options
			jQuery.extend(blueimp.Gallery.prototype.options, {
				useBootstrapModal: false,
				hidePageScrollbars: false
			});
		});
		</script>
		<?php } // end of lightbox check ?>
		
		<!-- Nivo Box -->
		<?php  if($FAGP_Light_Box == "lightbox1") { ?>
		<script>
		jQuery(window).load(function() {
				// NIVO-LIGHTBOX
				jQuery('a.nivoz').nivoLightbox({
				effect: 'slideDown',  
			});
		});
		</script>
		<?php } ?>
		
		<!-- Photo box-->
		<?php if($FAGP_Light_Box == "lightbox2") { ?>
		<script>
		jQuery( document ).ready(function() {
			jQuery('.photobox-lightbox').photobox('a');
			// or with a fancier selector and some settings, and a callback:
			jQuery('.photobox-lightbox').photobox('a:first', { thumbs:false, time:0 }, imageLoaded);
			function imageLoaded() {
				console.log('image has been loaded...');
			}
		});
		</script>
		<?php } ?>
		
		<?php  if($FAGP_Light_Box == "lightbox3") { ?>
		<script>
		jQuery(window).load(function() {
			jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
				animation_speed: 'fast', /* fast/slow/normal */
				slideshow: 2000, /* false OR interval time in ms */
				autoplay_slideshow: false, /* true/false */
				opacity: 0.80  /* Value between 0 and 1 */
			});
		});
		</script>
		<?php } ?>

		<!-- Smooth Box-->
		<?php  if($FAGP_Light_Box == "lightbox5") {?>
		<script type="text/javascript">
		jQuery(window).load(function() {

			jQuery('.sb').click(function(event){
				// which was clicked?
				var clicked = jQuery(this).index('.sb');
					
				// create smoothbox
			   jQuery('body').append('<div class="smoothbox sb-load"><div class="smoothbox-table"><div class="smoothbox-centering"><div class="smoothbox-sizing"><div class="sb-nav"><a href="#" class="sb-prev sb-prev-on" alt="Previous">&larr;</a><a href="#" class="sb-cancel" alt="Close">&times;</a><a href="#" class="sb-next sb-next-on" alt="Next">&rarr;</a></div><ul class="sb-items"></ul></div></div></div></div>');
				  
				jQuery.fn.reverse = [].reverse;
				// get each picture, put them in the box
				jQuery('.sb').reverse().each(function() {
				   var href = jQuery(this).attr('href');
					if (jQuery(this).attr('title')) {
						var caption = jQuery(this).attr('title');
						jQuery('.sb-items').append('<div class="sb-item"><div class="sb-caption">'+ caption +'</div><img src="'+ href + '"/></div>');
					}   
					else {
						  jQuery('.sb-items').append('<div class="sb-item"><img src="'+ href + '"/></div>');
				   }
				});
				
				jQuery('.sb-item').slice(0,-(clicked)).appendTo('.sb-items');
				jQuery('.sb-item').not(':last').hide();
				jQuery('.sb-item img:last').load(function() { 
					jQuery('.smoothbox-sizing').fadeIn('slow', function() {
						jQuery('.sb-nav').fadeIn();
						jQuery('.sb-load').removeClass('sb-load');
					});
				});
				event.preventDefault();
			});
			
			jQuery(document).on('click', ".sb-cancel", function(event){
				jQuery('.smoothbox').fadeOut('slow', function() {
					jQuery('.smoothbox').remove();
				});
				event.preventDefault();
			});

			jQuery(document).on('click', ".sb-next-on", function(event){
				jQuery(this).removeClass('sb-next-on');
				jQuery('.sb-item:last').addClass('sb-item-ani');
				// after animation, move order & remove class
				
					jQuery(".sb-item:last").bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(){
						jQuery('.sb-item').eq(-2).addClass('no-trans').fadeIn('fast');
						jQuery(this).removeClass('sb-item-ani').prependTo('.sb-items').hide();
						jQuery('.sb-item:last').removeClass('no-trans');
						jQuery('.sb-next').addClass('sb-next-on');
						jQuery('.sb-item').unbind();
					}); 
				event.preventDefault();
			});

			jQuery(document).on('click', ".sb-prev-on", function(event){  
				jQuery(this).removeClass('sb-prev-on');
			   
					jQuery('.sb-item:last').hide(); 
					jQuery(".sb-item:first").addClass('sb-item-ani2 no-trans').appendTo('.sb-items');
					jQuery('.sb-item:last').show().removeClass('no-trans').delay(1).queue(function(next){
						jQuery('.sb-item:last').removeClass('sb-item-ani2');
						next();
					});
					jQuery(this).addClass('sb-prev-on');    
				event.preventDefault();
			});
		});
		</script>
		<?php } ?>
		
		<!-- Swipe Box-->
		<?php  if($FAGP_Light_Box == "lightbox6") {?>
		<script type="text/javascript">
			jQuery(document).ready(function() {
				jQuery( '.swipebox' ).swipebox();
			});
		</script>
		<?php } ?>
		
		<!-- Ion box-->
		<?php  if($FAGP_Light_Box == "lightbox7") { ?>
		<script src="<?php echo FAGP_PLUGIN_URL.'lightbox/ionbox/ion.zoom.min.js'; ?>" ></script> 
		<script>
		jQuery(window).load(function() {
			jQuery(".gallery_1").ionZoom();
			});
		</script>
		<?php } ?>
		
		<!-- Fancy box-->
		<?php  if($FAGP_Light_Box == "lightbox8") { ?>
		<script type="text/javascript">
		jQuery(document).ready(function() {
			jQuery('.fancybox').fancybox();
		});
		</script>
		<?php } ?>
		
		<!-- Hover Animation Effect JS-->
		<script>
		jQuery(window).load(function() {
			/* Slide */
			jQuery('#slide a').each(function(index, element) {
				jQuery(this).hoverdir();
			});

			/* Stroke */
			jQuery('.b-link-stroke').prepend('<div class="b-top-line"></div>');
			jQuery('.b-link-stroke').prepend('<div class="b-bottom-line"></div>');

			/* Twist */
			jQuery('.b-link-twist').prepend('<div class="b-top-line"><b></b></div>');
			jQuery('.b-link-twist').prepend('<div class="b-bottom-line"><b></b></div>');
			jQuery('.b-link-twist img').each(function(index, element) {
				jQuery(this).css('visibility','hidden');
				jQuery(this).parent().find('.b-top-line, .b-bottom-line').css('background-image','url('+jQuery(this).attr('src')+')');
			});

			/* Flip */
			jQuery('.b-link-flip').prepend('<div class="b-top-line"><b></b></div>');
			jQuery('.b-link-flip').prepend('<div class="b-bottom-line"><b></b></div>');
			jQuery('.b-link-flip img').each(function(index, element) {
				jQuery(this).css('visibility','hidden');
				jQuery(this).parent().find('.b-top-line, .b-bottom-line').css('background-image','url('+jQuery(this).attr('src')+')');

			});

			/* Fade */
			jQuery('.b-link-fade').each(function(index, element) {
				jQuery(this).append('<div class="b-top-line"></div>')
			});

			/* Flow */
			jQuery('.b-link-flow').each(function(index, element) {
				jQuery(this).append('<div class="b-top-line"></div>')
			});

			/* Box */
			jQuery('.b-link-box').prepend('<div class="b-top-line"></div>');
			jQuery('.b-link-box').prepend('<div class="b-bottom-line"></div>');

			/* Stripe */
			jQuery('.b-link-stripe').each(function(index, element) {
				jQuery(this).prepend('<div class="b-line b-line1"></div><div class="b-line b-line2"></div><div class="b-line b-line3"></div><div class="b-line b-line4"></div><div class="b-line b-line5"></div>');
			});

			/* Apart */
			jQuery('.b-link-apart-vertical, .b-link-apart-horisontal').each(function(index, element) {
				jQuery(this).prepend('<div class="b-top-line"></div><div class="b-bottom-line"></div><div class="b-top-line-up"></div><div class="b-bottom-line-up"></div>');
			});

			/* diagonal */
			jQuery('.b-link-diagonal').each(function(index, element) {
				jQuery(this).prepend('<div class="b-line-group"><div class="b-line b-line1"></div><div class="b-line b-line2"></div><div class="b-line b-line3"></div><div class="b-line b-line4"></div><div class="b-line b-line5"></div></div>');
			});

			setTimeout("calculate_margin();", 100);
		});
		</script>
		<div style="font-size: small; margin-top:10px; float: left; display:none;">
			Flickr Album Gallery Pro Powered By: <a href="http://www.weblizar.com/" target="_blank">Weblizar</a>
		</div>
		<?php
	} else {
		echo "<div align='center' class='alert alert-danger'>".__("Sorry! Invalid Flicker Album Shortcode Embedded", FAGP_TEXT_DOMAIN )."</div>";
	}
	wp_reset_query();
	return ob_get_clean();
}//end of shortcode function
?>