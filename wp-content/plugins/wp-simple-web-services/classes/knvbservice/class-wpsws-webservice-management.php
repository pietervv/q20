<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WPSWS_Webservice_management {
	private static $instance = null;
	private static $fabriPrefix = "FL - ";

	public static function get() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function __construct() {
		$this->hooks();
	}

	private function hooks() {
		add_action( 'wpsws_webservice_imageupload', array( $this, 'imageupload' ) );
		add_action( 'wpsws_webservice_deleteimage', array( $this, 'deleteimage' ) );
		add_action( 'wpsws_webservice_oldold_csvupload', array( $this, 'oldold_csvupload' ) );
		add_action( 'wpsws_webservice_updateMatchDetails', array( $this, 'updateMatchDetails' ) );
		add_action( 'wpsws_webservice_publish', array( $this, 'publish' ) );
		add_action( 'wpsws_webservice_old_archiveSeason', array( $this, 'old_archiveSeason' ) );
		add_action( 'wpsws_webservice_fabriupload', array( $this, 'fabriupload' ) );
		add_action( 'wpsws_webservice_loadClubinfo', array( $this, 'loadClubinfo' ) );
		add_action( 'wpsws_webservice_updateClubinfo', array( $this, 'updateClubinfo' ) );
		add_action( 'wpsws_webservice_sendmail', array( $this, 'sendmail' ) );
		add_action( 'wpsws_webservice_check', array( $this, 'check' ) );
		add_action( 'wpsws_webservice_deleteComment', array( $this, 'deleteComment' ) );
	}
	
	public function sendmail() {
		global $wpdb;
		$name = $_POST["sender"];
		$email = $_POST["email"];
		$comment = $_POST["comment"];
		$wpdb->replace('jubileum', array( 
			'name' => $name,
			'email' => $email,
			'comment' => $comment
		));
	}
	
	public function deleteComment() {
		global $wpdb;
		$id = $_POST["id"];
		$wpdb->query($wpdb->prepare("DELETE FROM jubileum WHERE id=%d", $id));
	}
	
	public function updateClubinfo() {
		global $wpdb;
		$total = 0;
		$error = false;
		if ( isset( $_POST['id'] ) ) {
			foreach ( $_POST['id'] as $index => $id ) {
				$url = $_POST['url'][$index];
				if($url != null)
					$out = $out . $id . " " . $url . "    ";
				$ret = $wpdb->update( 
					'webservice__clubinfo', 
					array('url' => $url), 
					array('id' => $id), 
					array('%s'), 
					array('%d') 
				);
				if ($ret != false)
					$total += $ret;
			}
		}
		if ($total > 0)
			WSMacro::loadClubInfoFromDB();
		WPSWS_Output::get()->output($total);
	}
	
	public function loadClubinfo() {
		global $wpdb;
		$teams = $wpdb->get_results("SELECT * FROM webservice__team", ARRAY_A);
		$clubs = array();
		$newCount = 0;
		foreach ($teams as $team) {
			$knvbId = $team["knvb_id"];
			$competitionData = WSMacro::retreiveKnvbData("/competities/" . $knvbId, null);
			$competitions = $competitionData["List"];
			foreach ($competitions as $competition) {
				$compType = $competition['CompType'];
				$pouleId = $competition["PouleId"];
				$path = "/competities/" . $knvbId . "/" . $competition["District"] . "/" . $competition["CompId"] . "/" . $competition["ClassId"] . "/" . $pouleId;
				$compRanking = WSMacro::retreiveKnvbData($path  . "/ranking", null);
				if ($compRanking["errorcode"] == 1000) {
					foreach($compRanking["List"] as $team) {
						$clubCode = $team["ClubNummer"];
						$existing = $wpdb->get_row($wpdb->prepare("SELECT * FROM webservice__clubinfo WHERE clubcode=%s", $clubCode), ARRAY_A);
						if ($existing == null) {
							$newCount++;
							$wpdb->replace('webservice__clubinfo', array ( 
								'clubcode' =>  $clubCode,
								'name' => $team["naam"]
								)
							);
						}
					}
				}
			}
		}
		WPSWS_Output::get()->output($newCount);
	}

	public function log($text) {
		$wpdb->insert( 'webservice__test', array('value' => $text) );
	}
	
	public function check() {
		global $wpdb;
		$teamId = $_POST['teamId'];
		$sub = $_POST['sub'];
		$retVal = $wpdb->replace('stats', array( 
			'team_id' => $teamId,
			'sub' => $sub
			)
		);
		WPSWS_Output::get()->output( array('retVal:' => $retVal) );
	}
	
	public function fabriupload() {
		$nameConv = array();
		$nameConv['FC Porto'] = 'FC Porto';
		$nameConv['Chelsea'] = 'Chelsea';
		$nameConv['Barcelona'] = 'Barcelona';
		$nameConv['Bayern Munchen'] = 'Bayern M.';
		$nameConv['Real Madrid'] = 'Real Madrid';
		$nameConv['Juventus'] = 'Juventus';
		$nameConv['Liverpool'] = 'Liverpool';
		$nameConv['Manchester City'] = 'Manch. City';
		$nameConv['Inter Milaan'] = 'Inter Milaan';
		$nameConv['Borussia Dortmund'] = 'Borussia Dortmund';
		global $wpdb;
		global $_POST;
		$out = 'nothing happened';
		if ($_FILES) {
			$allowedExts = array("xlsx", "xls");
			$temp = explode(".", $_FILES["file"]["name"]);
			$extension = end($temp);
			if ( in_array($extension, $allowedExts)) {
				$out = 'Bestand is ontvangen; ';
  				if ($_FILES["file"]["error"] > 0) {
					$out = 'error';
  				} else {
  					// Read teams from database
  					$teamsFromDb = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` LIKE '%Fabri%'", ARRAY_A);
  					$teamsMap = array();
  					foreach($teamsFromDb as $team)
  						$teamsMap[$team["short_name"]] = $team;
  					// Load Excel
  					$reader;
  					if ($extension == 'xls')
  						$reader = PHPExcel_IOFactory::createReader('Excel5');
  					else
  						$reader = PHPExcel_IOFactory::createReader('Excel2007');
  					$excel = $reader->load($_FILES["file"]["tmp_name"]);
  					// Empty tables;
  					$wpdb->query('TRUNCATE TABLE webservice__fabri_results');
  					$wpdb->query('TRUNCATE TABLE webservice__fabri_standings');
  					// Read results
  					$excel->setActiveSheetIndexByName('Fabriuitslagen');
  					$sheet = $excel->getActiveSheet();
  					$round = 0;
  					for($rownum = 2; $rownum < 1000; $rownum++) {
  						$rowCheck = $sheet->getCell('D' . $rownum)->getValue();
  						if ($rowCheck === null || $rowCheck === '') {
  							// We're done
  							break;
  						}
  						$home = $sheet->getCell('D' . $rownum)->getOldCalculatedValue();
  						if ($home == '0' || $home == null || $home == '') {
  							// No team, so ignore this row
  							continue;
  						}
  						else {
  							$round = $this->cellValue($sheet, 'A', $rownum);
  							$date = $this->cellValue($sheet, 'B', $rownum);
  							$field = $this->cellValue($sheet, 'C', $rownum);
  							$home = $this->cellValue($sheet, 'D', $rownum);
  							$homeTeam = $teamsMap[$nameConv[$home]];
  							$away = $this->cellValue($sheet, 'E', $rownum);
  							$awayTeam = $teamsMap[$nameConv[$away]];
  							$homeScore = $this->cellValue($sheet, 'F', $rownum);
  							$awayScore = $this->cellValue($sheet, 'G', $rownum);
  							$toto = $this->cellValue($sheet, 'H', $rownum);
  							$wpdb->replace('webservice__fabri_results', array( 
									'round' => $round,
									'date' => $date,
									'field' => $field,
									'home_id' => $homeTeam["knvb_id"],
									'away_id' => $awayTeam["knvb_id"],
									'home_score' => $homeScore,
									'away_score' => $awayScore,
									'toto' => $toto
									)
							);
  						}
  					}
  					// Read standings
  					$excel->setActiveSheetIndexByName('fabristand');
  					$sheet = $excel->getActiveSheet();
  					$round = 0;
  					$started = 'false';
  					for($rownum = 2; $rownum < 100; $rownum++) {
  						$rowCheck = $sheet->getCell('A' . $rownum)->getOldCalculatedValue();
  						if ($rowCheck == null || $rowCheck == '') {
  							// We're done
  							break;
  						}
						$place = $sheet->getCell('A' . $rownum)->getOldCalculatedValue();
						$teamName = $sheet->getCell('B' . $rownum)->getOldCalculatedValue();
						$team = $teamsMap[$nameConv[$teamName]];
						$games = $sheet->getCell('C' . $rownum)->getOldCalculatedValue();
						$win = $sheet->getCell('D' . $rownum)->getOldCalculatedValue();
						$draw = $sheet->getCell('E' . $rownum)->getOldCalculatedValue();
						$lose = $sheet->getCell('F' . $rownum)->getOldCalculatedValue();
						$points = $sheet->getCell('G' . $rownum)->getOldCalculatedValue();
						$goalsScored = $sheet->getCell('H' . $rownum)->getOldCalculatedValue();
						$goalsAgainst = $sheet->getCell('I' . $rownum)->getOldCalculatedValue();
						$goalsDiff = $sheet->getCell('J' . $rownum)->getOldCalculatedValue();
						$wpdb->replace('webservice__fabri_standings', array( 
							'place' => $place,
							'team_id' => $team["knvb_id"],
							'games' => $games,
							'win' => $win,
							'draw' => $draw,
							'lose' => $lose,
							'points' => $points,
							'goals_scored' => $goalsScored,
							'goals_against' => $goalsAgainst,
							'goals_diff' => $goalsDiff
							)
						);
  					}
  				}
  			}
		} else {
			$out = 'no file';
		}
		WPSWS_Output::get()->output( array('Succesvol:' => $out) );
	}
	
	function cellValue($sheet, $columnnum, $rownum) {
		$val = $sheet->getCell($columnnum . $rownum)->getOldCalculatedValue();
		return $val == null ? $sheet->getCell($columnnum . $rownum)->getValue() : $val;
	}

	public function newseason_analysis() {
		global $wpdb;
		global $_POST;
		$out = 'De upload is niet gelukt.';
		$unknown = array();
		$result;
		if ($_FILES) {
			$allowedExts = array("csv");
			$temp = explode(".", $_FILES["file"]["name"]);
			$extension = end($temp);
			if ( in_array($extension, $allowedExts)) {
  				if ($_FILES["file"]["error"] > 0) {
					$wpdb->insert( 'webservice__test', array('value' => 'error') );
  				} else {
					$csv = new File_CSV_DataSource;
					$csvSettings = array('delimiter'=>',', 'eol'=>"", 'length'=>0, 'escape'=>'"');
					$csv->settings($csvSettings);
					$csv->load($_FILES["file"]["tmp_name"]);
					$players = $csv->connect(array('Team', 'Teamcode', 'Relatienr', 'Roepnaam', 'Tussenvoegsel', 'Achternaam', 'Op wedstrijdform?', 'E-mail', 'Mobiel', 'Leeftijdscat team', 'Teamfunctie', 'Team rol'));
					
					$knvbTeams = array();
					$res = WSMacro::retreiveKnvbData("/teams", null);
					if ($res["errorcode"] == 1000) {
						foreach ($res["List"] as $team) {
							$teamCode = $team["teamid"];
							$teams[$teamCode] = array();
							$knvbTeams[$teamCode] = $team;
						}
					}
					$teams = array();
					foreach($players as $player) {
						$teamId = $player["Teamcode"];
						$teamArr = $teams[$teamId];
						if ($teamArr == null)
							$teamArr = array();
						$sportlink = $teamArr["sportlink"];
						if ($sportlink == null) {
							$sportlink = array();
							$sportlink["teamId"] = $teamId;
							$sportlink["teamName"] = $player["Team"];;
							$teamArr["sportlink"] = $sportlink;	
						}
						$knvb = $teamArr["knvb"];
						if ($knvb == null) {
							$knvbTeam = $knvbTeams[$teamId];
							if ($knvbTeam == null)
								$teamArr["knvb"] = false;
							else 
								$teamArr["knvb"] = $knvbTeam;
						}
						$teams[$teamId] = $teamArr;
					}
				}
			}
		} else {
			$wpdb->insert( 'webservice__test', array('value' => 'nofile') );
		}
		WPSWS_Output::get()->output( array('result' => $result, 'Succesvol:' => $out, 'Niet bekend bij API:' => $unknown) );
	}
	
	public function oldold_csvupload() {
		global $wpdb;
		global $_POST;
		$noYouth = $_POST["nopl_y"] == 'on';
		$noSeniors = $_POST["nopl_s"] == 'on';
		$out = 'De upload is niet gelukt.';
		$unknown = array();
		if ($_FILES) {
			$allowedExts = array("csv");
			$temp = explode(".", $_FILES["file"]["name"]);
			$extension = end($temp);
			if ( in_array($extension, $allowedExts)) {
  				if ($_FILES["file"]["error"] > 0) {
					$wpdb->insert( 'webservice__test', array('value' => 'error') );
  				} else {
  					// Retrieve teams from knvb
					$res = WSMacro::retreiveKnvbData("/teams", null);
					$teams = array();
					if ($res["errorcode"] == 1000) {
						foreach ($res["List"] as $team) {
							$teamCode = $team["teamid"];
							$teams[$teamCode] = array();
							$teams[$teamCode]['details'] = $team;
						}
					} else {
						$wpdb->insert( 'webservice__test', array('value' => 'teams ophalen mislukt') );
						WPSWS_Output::get()->output(array('result' => 'Importeren mislukt: kon geen teams ophalen via knvb dataservice'));
						return;
					}
					$wpdb->insert( 'webservice__test', array('value' => 'Aantal teams van knvb: ' . count($teams)) );
					// Retrieve players from csv and add them to teams
					$csv = new File_CSV_DataSource;
					$csvSettings = array('delimiter'=>',', 'eol'=>"", 'length'=>0, 'escape'=>'"');
					$csv->settings($csvSettings);

					$csv->load($_FILES["file"]["tmp_name"]);
					$players = $csv->connect(array('Team', 'Teamcode', 'Relatienr', 'Roepnaam', 'Tussenvoegsel', 'Achternaam', 'Op wedstrijdform?', 'E-mail', 'Mobiel', 'Leeftijdscat team', 'Teamfunctie', 'Team rol'));
					foreach($players as $player) {
						$teamCode = $player["Teamcode"];
						$teamDetails = $teams[$teamCode];
						// Noodoplossing: aan het begin van het seizoen zijn teams nog niet
						// beschikbaar via de API, maar moeten de spelers al wel getoond
						// worden op de website. We nemen hier alleen de jeugd mee, omdat het
						// voor de jeugd belangrijk is om te weten. Bij de senioren werkt dit
						// niet, omdat via Sportlink geen onderscheid gemaakt kan worden
						// tussen zaterdag- en zondagteams.
						if ($teamDetails == null) {
							//$unknown[$teamCode] = 'onbekend team: ' . $player["Team"];
							$teamName = $player["Team"];
							if (!($this->startsWith($teamName, 'A') || $this->startsWith($teamName, 'B') || $this->startsWith($teamName, 'C') || $this->startsWith($teamName, 'D') || $this->startsWith($teamName, 'E') || $this->startsWith($teamName, 'F') || $this->startsWith($teamName, 'M')))
								continue;
							$teams[$teamCode] = array();
							$teams[$teamCode]['details'] = array();
							$teams[$teamCode]['details']["speeldag"] = 'zaterdag';
							$teams[$teamCode]['details']["teamname"] = "Quick '20 " . $player["Team"];
							$teamDetails = $teams[$teamCode];
						}
						if ($teams[$teamCode]['details']["categorie"] == null) {
							$teamCat = $player["Leeftijdscat team"];
							if ($teamCat != null && teamCat != '') {
								$cat = 'Junioren';
								if (strpos($teamCat, 'D-') !== false || strpos($teamCat, 'E-') !== false || strpos($teamCat, 'F-') !== false)
									$cat = 'Pupillen';
								$teams[$teamCode]['details']["categorie"] = $cat;
								$wpdb->insert( 'webservice__test', array('value' => 'updated:' . $teamCode) );
							}
						}
						$playerArray = $teamDetails['players'];
						if ($playerArray == null)
							$playerArray = array();
						$playerArray[$player["Relatienr"]] = $player;
						$teams[$teamCode]['players'] = $playerArray;
					}
					foreach ($teams as $key => $value) {
						if ($value['players'] == null)
							unset($teams[$key]);
					}
					$newTeams = array();
					$newPlayers = array();
					$updatedTeams = 0;
					$updatedPlayers = 0;
					// Delete all existing data
					// Teams: not yet, we will use image name
					$wpdb->query( "TRUNCATE TABLE webservice__team");
					$wpdb->query( "TRUNCATE TABLE webservice__user");
					foreach ($teams as $key => $value) {
						// Get the competition name from KNVB API:
						$c = WSMacro::retreiveKnvbData("/competities/" . $key, array('comptype' => 'R'));
						$competitionName = null;
						if ($c != null && $c["errorcode"] == 1000 && $c["List"] != null && $c["List"][0] != null)
							$competitionName = $c["List"][0]["ClassName"] . ' ' . $c["List"][0]["PouleName"];
						$teamDetails = $value['details'];
						$teamCat = $teamDetails['categorie'];
						$speelDag = $teamDetails['speeldag'];
						$teamPlayers = $value['players'];
						$player = array_values($teamPlayers)[0];
						$teamGroup = $teamCat;
						foreach ($teamPlayers as $plr => $dat)
							if ($dat["Leeftijdscat team"] != '')
								$teamGroup = $dat["Leeftijdscat team"];
						if ($teamGroup == 'Senioren' || $teamGroup == 'Senioren Dames')
							$teamGroup = $speelDag;
						$teamShortname = $player["Team"];
						// Fix teamnames voor Fabri League:
						if ($this->startsWith($teamShortname, self::$fabriPrefix)) {
							$teamShortname = substr($teamShortname, strlen(self::$fabriPrefix));
							$teamGroup = 'Fabri';
						}
						$teamName = $teamDetails['teamname'];
					  	if ($teamCat != 'Pupillen' && $teamCat != 'Junioren')
					  		$teamName = $teamName . ' (' . $speelDag . ')';
					  	$teamFromDb = $wpdb->get_row($wpdb->prepare("SELECT * FROM webservice__team WHERE knvb_id=%d", $key), ARRAY_A);
					  	if ($teamFromDb == null) {
							if ($key == '209296')
								$unknown['insert'] = $teamName . '-' . $teamShortname . '-' . $key . '-' . $teamCat . '-' . $teamGroup;
							$retValTeam = $wpdb->replace('webservice__team', array( 
								'name' =>  $teamName,
								'short_name' => $teamShortname,
								'knvb_id' => $key,
								'category' => $teamCat,
								'group' => $teamGroup,
								'competition_title' => $competitionName,
								'created' => current_time('mysql', 1)
								)
							);
							if ($retValTeam == 1)
								array_push($newTeams, $key);
							if ($retValTeam == 2)
								$updatedTeams++;
						}
						$includePlayers = (($teamCat == 'Junioren' || $teamCat == 'Pupillen') && !$noYouth) || ($teamCat == 'Senioren' && !$noSeniors);
						if ($includePlayers) {
							foreach ($teamPlayers as $playerNr => $playerData) {
								if ($playerData["Team rol"] != 'Teamspeler' || $playerData["Op wedstrijdform?"] == 'TRUE') {
										$retValPl = $wpdb->replace('webservice__user', array( 
											'team' => $key,
											'knvb_code' => $playerData["Relatienr"],
											'first_name' => $playerData["Roepnaam"],
											'infix' => $playerData["Tussenvoegsel"],
											'last_name' => $playerData["Achternaam"],
											'email' => $playerData["E-mail"],
											'function' => $playerData["Teamfunctie"],
											'telephone' => $playerData["Mobiel"],
											'created' => current_time('mysql', 1)
											)
										);
										if ($retValPl == 1)
											array_push($newPlayers, $playerData["Roepnaam"] . " " . $playerData["Tussenvoegsel"] . " " . $playerData["Achternaam"]);
										if ($retValPl == 2)
											$updatedPlayers++;
								}
							}
						}
					}
					$out = array();
					$out= ["Bijgewerkt teams: " => $updatedTeams, "Bijgewerkt spelers" => $updatedPlayers, "Nieuwe teams: " => $newTeams, "Nieuwe spelers: " => count($newPlayers)];
				}
			}
		} else {
			$wpdb->insert( 'webservice__test', array('value' => 'nofile') );
		}
		WPSWS_Output::get()->output( array('Succesvol:' => $out, 'Niet bekend bij API:' => $unknown) );
	}
	
	public function old_archiveSeason() {
		//WPSWS_Output::get()->output("Closed for now. On change: add current season!");
		//return;
		// [DB] Haal alle teams op
		global $wpdb;
		$teams = $wpdb->get_results("SELECT * FROM webservice__team", ARRAY_A);
		// Per team:
		foreach ($teams as $team) {
			$knvbId = $team["knvb_id"];
			// [DB] Sla teamgegevens op
			// debug:
			// if ($knvbId != 222824) continue;
			$wpdb->replace('webservice__hist_team', array( 
				'season' => '2016',
				'team_name' => $team["name"],
				'category' => $team["category"],
				'group' => $team["group"],
				'knvb_id' => $knvbId
			));
			$teamHistId = $wpdb->insert_id;
			// [DB] Sla de spelers op
			$players = $wpdb->get_results($wpdb->prepare("SELECT * FROM webservice__user u WHERE u.team = %d", $knvbId), ARRAY_A);
			foreach ($players as $player) {
				$wpdb->replace('webservice__hist_user', array( 
					'hist_team_id' => $teamHistId,
					'knvb_code' => $player["knvb_code"],
					'name' => $player["first_name"] . " " . $player["infix"] . " " . $player["last_name"],
					'function' => $player["function"]
				));
			}
			// [API] Haal alle competities van dit team op
			$competitionData = WSMacro::retreiveKnvbData("/competities/" . $knvbId, null);
			$competitions = $competitionData["List"];
			// Per competitie:
			foreach ($competitions as $competition) {
				$compType = $competition['CompType'];
				$pouleId = $competition["PouleId"];
				$path = "/competities/" . $knvbId . "/" . $competition["District"] . "/" . $competition["CompId"] . "/" . $competition["ClassId"] . "/" . $pouleId;
				$hasRanking = $competition['metStand'] == 'J';
				$compRanking = WSMacro::retreiveKnvbData($path  . "/ranking", null);
				if ($compRanking != null && $compRanking["errorcode"] == 1000)
					$hasRanking = true;
				$params = array('weeknummer' => 'A');
				$ranking = "";
				// [API] Haal de eindranglijst op, indien aanwezig
				if ($hasRanking) {
					// [API] Sla de eindranglijst op
					$teamRankingMap = array();
					foreach ($compRanking["List"] as $r) {
						//1:<naam>:<clubnummer>:<knvb_id>:22.16.3.3.51.68.24.0,
						$naam = str_replace(":", "", $r["naam"]);
						$ranking = $ranking . $r["Positie"] . ":" . $naam . ":" . $r["ClubNummer"] . ":" . $r["TeamID"] . ":" . $r["Gespeeld"] . "." . $r["Gewonnen"] . "." . $r["Gelijk"] . "." . $r["Verloren"] . "." . $r["Punten"] . "." . $r["DoelpuntenVoor"] . "." . $r["DoelpuntenTegen"] . "." . $r["PuntenMindering"] . ",";
						$teamRankingMap[$r["TeamID"]] = $r["Positie"];
					}
				}
				$wpdb->replace('webservice__hist_competition', array(
					'knvb_comp_id' => $competition["PouleId"],
					'hist_team_id' => $teamHistId,
					'type' => $competition["CompType"],
					'has_ranking' => $hasRanking,
					'name' => $competition["CompName"] . " " . $competition["ClassName"] . " " . $competition["PouleName"],
					'ranking' => $ranking
				));
				$compHistId = $wpdb->insert_id;
				// [API] Hall alle resultaten op
				$compResults = WSMacro::retreiveKnvbData($path  . "/results", $params);
				// [API] Sla alle resultaten op
				$matchDays = array();
				foreach ($compResults["List"] as $result) {
					$matchDay = $result["WedstrijdDag"];
					$matchDayArray = $matchDays[$matchDay];
					if ($matchDayArray == null)
						$matchDayArray = array();
					$matchDayArray[] = $result;
					$matchDays[$matchDay] = $matchDayArray;
				}
				foreach ($matchDays as $nr => $matches) {
					// 1-12:0-1,2-11:2-1,3-10:2-2
					$str = "";
					if ($hasRanking) {
						foreach($matches as $match) {
							$ext = "";
							$pen = "";
							$bijz = "";
							if($match["PuntenTeam1Verl"] != null)
								$ext = ":V" . $match["PuntenTeam1Verl"] . "-" . $match["PuntenTeam2Verl"];
							if($match["PuntenTeam1Strafsch"] != null)
								$pen = ":P" . $match["PuntenTeam1Strafsch"] . "-" . $match["PuntenTeam2Strafsch"];
							if($match["Bijzonderheden"] != null)
								$bijz = ":B" . $match["Bijzonderheden"];
							$str = $str . $teamRankingMap[$match["ThuisTeamID"]] . '-' . $teamRankingMap[$match["UitTeamID"]] . ":" . $match["PuntenTeam1"] . "-" . $match["PuntenTeam2"] . $ext . $pen . $bijz . ",";
						}
					} else {
						foreach($matches as $match) {
							$ext = "";
							$pen = "";
							if($match["PuntenTeam1Verl"] != null) {
								$ext = ":V" . $match["PuntenTeam1Verl"] . "-" . $match["PuntenTeam2Verl"];
							}
							if($match["PuntenTeam1Strafsch"] != null) {
								$pen = ":P" . $match["PuntenTeam1Strafsch"] . "-" . $match["PuntenTeam2Strafsch"];
							}
							$str = $str . $match["ThuisClub"] . '-' . $match["UitClub"] . ":" . $match["PuntenTeam1"] . "-" . $match["PuntenTeam2"] . $ext . $pen . ",";
						}
					}
					$wpdb->replace('webservice__hist_match', array( 
						'hist_competition_id' => $compHistId,
						'round' => $nr,
						'results' => $str
					));
					WPSWS_Output::get()->output( $teamRankingMap);
				}
			}	
		}
		// [DB] Export Fabri results & ranking ?
	}
	
	public function imageupload() {
		global $wpdb;
		global $_POST;
		$teamId = $_POST['team_id'];
		$team = $wpdb->get_row($wpdb->prepare("SELECT * FROM webservice__team WHERE knvb_id=%d", $teamId), ARRAY_A);
		$teamName = $team["short_name"];
		$out = "";
		if ($team["group"] == 'ZA' || $team["group"] == 'ZO')
			$teamName = $team["group"] . $team["short_name"];
		if ($_FILES) {
			$allowedExts = array("gif", "jpeg", "jpg", "png", "JPG", "JPEG", "PNG", "GIF");
			$temp = explode(".", $_FILES["file"]["name"]);
			$extension = end($temp);
			if ( in_array($extension, $allowedExts)) {
				if ($_FILES["file"]["error"] > 0) {
					$out = "Error 1";
  				} else {
					$imageDir = ABSPATH . "wp-content/uploads/TeamPictures/";
					$imageName = strtolower($teamName . '.' .  $extension);
					$imageLocation = $imageDir . $imageName;
					move_uploaded_file($_FILES["file"]["tmp_name"], $imageLocation);
					$img = imagecreatefromjpeg($imageLocation);
					$image = wp_get_image_editor( $imageLocation );
					if ( ! is_wp_error( $image ) ) {
						$image->resize( 1750, 1000, false );
						$image->save( $imageLocation );
					}
  					$wpdb->replace( 
						'webservice__team_data', 
						array(
							'key' => 'teampicture',
							'value' => $imageName,
							'team_id' => $teamId
						)
					);
					$out = "Done.";
				}
			}
		} else {
			$out = "Error 2";
		}
		WPSWS_Output::get()->output( $out );
	}
	
	public function deleteimage() {
		global $wpdb;
		global $_POST;
		$teamId = $_POST['team_id'];
		$imageDir = ABSPATH . "wp-content/uploads/TeamPictures/";
		$imageRow = $wpdb->get_row($wpdb->prepare("SELECT * FROM webservice__team_data WHERE `key` = 'teampicture' AND team_id=%d", $teamId), ARRAY_A);
		$imageName = $imageRow["value"];
		$wpdb->query($wpdb->prepare("DELETE FROM webservice__team_data WHERE id=%d", $imageRow["id"]));
		$result = unlink($imageDir . $imageName);
		WPSWS_Output::get()->output( $result );
	}
	
	public function publish() {
		global $wpdb;
		$data = $_POST['data'];
		$values = explode("_", $data);
		$pwd = $values[0];
		if ($pwd != 'Beheer@q20') {
			WPSWS_Output::get()->output(false);
			return;
		}
		$weekNr = $values[1];
		// Clear cache for this week
		$transientString = 'scheduleAll_week_' . $weekNr;
		delete_transient($transientString);
		$wpdb->replace('webservice__published_week', array('week' => $weekNr));
	}
	
	public function updateMatchDetails() {
		global $wpdb;
		$data = $_POST['data'];
		$values = explode("_", $data);
		$pwd = $values[0];
		if ($pwd != 'Beheer@q20') {
			WPSWS_Output::get()->output(false);
			return;
		}
		$matchId = $values[1];
		$referee = $values[2];
		$klkhome = $values[3];
		$klkaway = $values[4];
		$field = $values[5];
		$wpdb->replace('webservice__match_details', array( 
			'match_id' => $matchId,
			'key' => 'referee',
			'value' => $referee
			)
		);
		$wpdb->replace('webservice__match_details', array( 
			'match_id' => $matchId,
			'key' => 'klkhome',
			'value' => $klkhome
			)
		);
		$wpdb->replace('webservice__match_details', array( 
			'match_id' => $matchId,
			'key' => 'klkaway',
			'value' => $klkaway
			)
		);
		$wpdb->replace('webservice__match_details', array( 
			'match_id' => $matchId,
			'key' => 'field',
			'value' => $field
			)
		);
		WPSWS_Output::get()->output(true);
	}
	
	// --- Hulpfuncties --- //
	
	function startsWith($haystack, $needle) {
		$length = strlen($needle);
		return (substr($haystack, 0, $length) === $needle);
	}

	function endsWith($haystack, $needle) {
		$length = strlen($needle);
		if ($length == 0) {
			return true;
		}
		return (substr($haystack, -$length) === $needle);
	}
}