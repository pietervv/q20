<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class CDMacro {
	const COMP_DATA_CACHE_KEY = "COMP_DATA_CACHE_KEY";
	const TRAINING_DATA_TEAM_CACHE_KEY = "TRAINING_DATA_TEAM_CACHE_KEY";
	const TRAINING_DATA_CLUB_CACHE_KEY = "TRAINING_DATA_CLUB_CACHE_KEY";
	const CACHE_EXPIRE = 86400; // 1 day
	const BASE_URL = "https://data.sportlink.com/";
	const CLIENT_ID = "CRqGQ9TjfU";
	
	// List of articles
	const ALL_CANCELED = 'afgelastingen';
	const ALL_RESULTS = 'uitslagen';
	const ALL_SCHEDULE = 'programma';
	const MATCH_DETAILS = 'wedstrijd-informatie';
	const PERIOD_NUMBERS = 'keuzelijst-periodenummers';
	const PERIOD_RANKING = 'periodestand';
	const POULE_LIST = 'teampoulelijst';
	const POULE_RANKING = 'poulestand';
	const POULE_RESULTS = 'pouleuitslagen';
	const POULE_SCHEDULE = 'poule-programma';
	const TEAM_LIST = 'teams';
	const TRAINING_LIST = 'trainingenlijst';
	const TRAINING_DETAILS = 'trainingdetails';
	
	
	public function getArticle($article, $params) {
		$paramString = '';
		if ($params != null) {
			foreach ($params as $key => $value)
				$paramString = $paramString . '&' . $key . '=' . $value;
		}
		$url = self::BASE_URL . "/" . $article . "?client_id=" . self::CLIENT_ID . $paramString;
		$response = wp_remote_get($url);
		$responseBody = wp_remote_retrieve_body($response);
		return json_decode($responseBody, true);
	}
	
	public function getAllTrainingsData($useCache) {
		//$useCache = false;
		// Data per team:
		if ($useCache)
			$trainingDataTeam = get_transient(self::TRAINING_DATA_TEAM_CACHE_KEY);
		else
			$trainingDataTeam = null;
		
		// Data for the entire club:
		if ($useCache)
			$trainingDataClub = get_transient(self::TRAINING_DATA_CLUB_CACHE_KEY);
		else
			$trainingDataClub = null;
		
		if ($trainingDataTeam == null || $trainingDataClub == null) {
			$trainingDataTeam = array();
			$trainingDataClub = array();
			
			$allKeys = array();
			
			$trainingList = self::getArticle(self::TRAINING_LIST, null);
			$tst = null;
			foreach ($trainingList as $training) {
				$teamNaam = $training['trainingnaam'];
				$trainingId = $training['trainingid'];
				$params = array('trainingid' => $trainingId);
				$details = self::getArticle(self::TRAINING_DETAILS, $params);
				foreach($details as $detail) {
					$startDate = new DateTime($detail["begindatum"]); 
					$endDate = new DateTime($detail["einddatum"]);
					$dayOfWeek = $startDate->format('N');
					
					$key = $teamNaam . '|' . $dayOfWeek;
					if (!in_array($key, $allKeys)) {
						$detail['team'] = $teamNaam;
						$detail['start'] = substr($detail["begindatum"], 11, 5);
						$detail['end'] = substr($detail["einddatum"], 11, 5);
						$detail['dayOfWeek'] = $dayOfWeek;
						$dayArray = $trainingDataClub[$dayOfWeek];
						$dayArray[] = $detail;
						$dayArray = self::trainingSort($dayArray);
						$trainingDataClub[$dayOfWeek] = $dayArray;
						$trainingDataTeam[$teamNaam][] = $detail;
						$allKeys[] = $key;
					}
				}
			}
			set_transient(self::TRAINING_DATA_TEAM_CACHE_KEY, $trainingDataTeam, CACHE_EXPIRE);
			set_transient(self::TRAINING_DATA_CLUB_CACHE_KEY, $trainingDataClub, CACHE_EXPIRE);
		}
		return array('trainingDataClub' => $trainingDataClub, 'trainingDataTeam' => $trainingDataTeam);
	}
	
	public function trainingSort($array) {
		$length = count($array);
		if($length <= 1)
			return $array;
		else {
			$pivot = $array[0];
			$left = $right = array();
			for($i = 1; $i < count($array); $i++) {
				if($array[$i]['start'] < $pivot['start'] || 
						($array[$i]['start'] == $pivot['start'] && $array[$i]['team'] < $pivot['team']))
					$left[] = $array[$i];
				else
					$right[] = $array[$i];
			}
			return array_merge(self::trainingSort($left), array($pivot), self::trainingSort($right));
		}
	}
	
	public function getCompetitionData($teamcode) {
	    $compData = wp_cache_get(self::COMP_DATA_CACHE_KEY);
	    if ($compData == null) {
	        $compData = array();
	        $compList = self::getArticle(self::TEAM_LIST, null);
	        if ($compList != null) {
	            foreach($compList as $comp) {
	                $team = $comp["teamcode"];
	                $teamData = $compData[$team];
	                if ($teamData == null)
	                    $teamData = array();
	                    $teamData[] = $comp;
	                    $compData[$team] = $teamData;
	            }
	        }
	        wp_cache_set(self::COMP_DATA_CACHE_KEY, $compData, 0);
	        $compData["test"] = "not from cache";
	    }
	    else {
	        $compData["test"] = "from cache";
	    }
	    if ($teamcode != null)
	        return $compData[$teamcode];
		return $compList;
	}
}