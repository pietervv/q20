<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WPSWS_Webservice_cd_management {
	private static $instance = null;
	private static $fabriPrefix = "FL - ";

	public static function get() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function __construct() {
		$this->hooks();
	}

	private function hooks() {
		add_action( 'wpsws_webservice_csvupload', array( $this, 'csvupload' ) );
		add_action( 'wpsws_webservice_archiveSeason', array( $this, 'archiveSeason' ) );
		add_action( 'wpsws_webservice_fabriupload', array( $this, 'fabriupload' ) );
		add_action( 'wpsws_webservice_reloadTrainingCache', array( $this, 'reloadTrainingCache' ) );
		add_action( 'wpsws_webservice_testCall', array( $this, 'testCall' ) );
	}
	
	public function testCall() {
	    $knvbId = '346877';
	    $competitions = CDMacro::getCompetitionData($knvbId);
	    WPSWS_Output::get()->output($competitions);
	}
	
	public function reloadTrainingCache() {
		$trainings = CDMacro::getAllTrainingsData(false);
		WPSWS_Output::get()->output($trainings['trainingDataClub']);
	}
	
	public function archiveSeason() {
		// $output = array();
		// WPSWS_Output::get()->output("Closed for now. On change: add current season!");
		// return;
		// [DB] Haal alle teams op
		global $wpdb;
		$teams = $wpdb->get_results("SELECT * FROM webservice__team", ARRAY_A);
		// Per team:
		foreach ($teams as $team) {
			$knvbId = $team["knvb_id"];
			// [DB] Sla teamgegevens op
			// debug:
			//if ($knvbId != 161812) 
			//	continue;
			$wpdb->replace('webservice__hist_team', array( 
				'season' => '2019',
				'team_name' => $team["name"],
				'category' => $team["category"],
				'group' => $team["group"],
				'knvb_id' => $knvbId
			));
			$teamHistId = $wpdb->insert_id;
			// [DB] Sla de spelers op
			$players = $wpdb->get_results($wpdb->prepare("SELECT * FROM webservice__user u WHERE u.team = %d", $knvbId), ARRAY_A);
			foreach ($players as $player) {
				$wpdb->replace('webservice__hist_user', array( 
					'hist_team_id' => $teamHistId,
					'knvb_code' => $player["knvb_code"],
					'name' => $player["first_name"] . " " . $player["infix"] . " " . $player["last_name"],
					'function' => $player["function"]
				));
			}
			// [API] Haal alle competities van dit team op
			$competitions = CDMacro::getCompetitionData($knvbId);
			// Per competitie:
			foreach ($competitions as $competition) {
				$compType = '-';
				if ($competition['competitiesoort'] == 'regulier')
					$compType = 'R';
				else if ($competition['competitiesoort'] == 'nacompetitie')
					$compType = 'N';
				else if ($competition['competitiesoort'] == 'beker')
					$compType = 'B';
				$pouleCode = $competition['poulecode'];
				$params = array('poulecode' => $pouleCode);
				$compRanking = CDMacro::getarticle(CDMacro::POULE_RANKING, $params);
				$ranking = '';
				// Get the mapping for teams to team-ids
				// &poulecode=494460&weekoffset=-52&aantaldagen=365&eigenwedstrijden=NEE
				$params = array('poulecode' => $pouleCode, 'eigenwedstrijden' => 'NEE',
					'weekoffset' => -52, 'aantaldagen' => 365);
				$compResults = CDMacro::getarticle(CDMacro::POULE_RESULTS, $params);
				$teamIds = array();
				$nrOfTeams = sizeof($compRanking);
				foreach ($compResults as $res) {
					$teamIds[$res['thuisteam']] = $res['thuisteamid'];
					$teamIds[$res['uitteam']] = $res['uitteamid'];
					if (sizeof($teamIds) >= $nrOfTeams)
						break;
				}
				// [API] Sla de eindranglijst op
				$teamRankingMap = array();
				$ranking = "";
				foreach ($compRanking as $r) {
					//1:<naam>:<clubnummer>:<knvb_id>:22.16.3.3.51.68.24.0,
					$naam = str_replace(":", "", $r["teamnaam"]);
					$teamId = $teamIds[$r['teamnaam']];
					$ranking = $ranking . $r["positie"] . ":" . $naam . ":" . $r["clubrelatiecode"] . ":" . $teamId . ":" . $r["gespeeldewedstrijden"] . "." . $r["gewonnen"] . "." . $r["gelijk"] . "." . $r["verloren"] . "." . $r["punten"] . "." . $r["doelpuntenvoor"] . "." . $r["doelpuntentegen"] . "." . $r["verliespunten"] . ",";
					$teamRankingMap[$teamId] = $r["positie"];
				}
				$output['ranking'] = $ranking;
				$wpdb->replace('webservice__hist_competition', array(
					'knvb_comp_id' => $competition["poulecode"],
					'hist_team_id' => $teamHistId,
					'type' => $compType,
					'has_ranking' => $hasRanking,
					'name' => $competition["competitienaam"],
					'ranking' => $ranking
				));
				$compHistId = $wpdb->insert_id;
				
				// [API] Sla alle resultaten op
				$matchDays = array();
				foreach ($compResults as $result) {
					$date = $result["datumopgemaakt"];
					$matchDayArray = $matchDays[$date];
					if ($matchDayArray == null)
						$matchDayArray = array();
					$matchDayArray[] = $result;
					$matchDays[$date] = $matchDayArray;
				}
				$output['md'] = $matchDays;
				foreach ($matchDays as $date => $matches) {
					// 1-12:0-1,2-11:2-1,3-10:2-2
					$str = "";
					if (true) {//if ($hasRanking) {
						foreach($matches as $match) {
							$ext = "";
							$pen = "";
							$bijz = "";
							//if($match["PuntenTeam1Verl"] != null)
							//	$ext = ":V" . $match["PuntenTeam1Verl"] . "-" . $match["PuntenTeam2Verl"];
							//if($match["PuntenTeam1Strafsch"] != null)
							//	$pen = ":P" . $match["PuntenTeam1Strafsch"] . "-" . $match["PuntenTeam2Strafsch"];
							//if($match["Bijzonderheden"] != null)
							//	$bijz = ":B" . $match["Bijzonderheden"];
							$str = $str . $teamRankingMap[$match["thuisteamid"]] . '-' . $teamRankingMap[$match["uitteamid"]] . ":" . str_replace(' ', '', $match["uitslag"]) . $ext . $pen . $bijz . ",";
						}
					} else {
						foreach($matches as $match) {
							$ext = "";
							$pen = "";
							if($match["PuntenTeam1Verl"] != null) {
								$ext = ":V" . $match["PuntenTeam1Verl"] . "-" . $match["PuntenTeam2Verl"];
							}
							if($match["PuntenTeam1Strafsch"] != null) {
								$pen = ":P" . $match["PuntenTeam1Strafsch"] . "-" . $match["PuntenTeam2Strafsch"];
							}
							$str = $str . $match["ThuisClub"] . '-' . $match["UitClub"] . ":" . $match["PuntenTeam1"] . "-" . $match["PuntenTeam2"] . $ext . $pen . ",";
						}
					}
					$wpdb->replace('webservice__hist_match', array( 
						'hist_competition_id' => $compHistId,
						'date' => $date,
						'results' => $str
					));
				}
			}	
		}
		WPSWS_Output::get()->output( $output);
		// [DB] Export Fabri results & ranking ?
	}
	
	public function csvupload() {
		global $wpdb;
		global $_POST;
		$noYouth = $_POST["nopl_y"] == 'on';
		$noSeniors = $_POST["nopl_s"] == 'on';
		$out = 'De upload is niet gelukt.';
		$outtest = '';
		$unknown = array();
		if ($_FILES) {
			$allowedExts = array("csv");
			$temp = explode(".", $_FILES["file"]["name"]);
			$extension = end($temp);
			if ( in_array($extension, $allowedExts)) {
  				if ($_FILES["file"]["error"] > 0) {
					$wpdb->insert( 'webservice__test', array('value' => 'error') );
  				} else {
					// Retrieve players from csv and add them to teams
					$csv = new File_CSV_DataSource;
					$csvSettings = array('delimiter'=>',', 'eol'=>"", 'length'=>0, 'escape'=>'"');
					$csv->settings($csvSettings);

					$csv->load($_FILES["file"]["tmp_name"]);
					$players = $csv->connect(array('Team', 'Teamcode', 'Relatiecode', 'Roepnaam', 'Tussenvoegsel(s)', 'Achternaam', 'Op wedstrijdformulier?', 'E-mail', 'Mobiel', 'Teamfunctie', 'Teamrol', 'Zichtbaarheid'));
					$outtest = $players;
					$competitions = CDMacro::getCompetitionData();
					$teams = array();
					foreach($players as $player) {
						$teamCode = $player["Teamcode"];
						$team = $teams[$teamCode];
						if ($team == null)
							$team = array();
						$teamDetails = $team['details'];
						if ($teamDetails == null)
							$teamDetails = array();
						$comps = $competitions[$teamCode];
						$regularCompWasFound = false;
						foreach($comps as $comp) {
							if ($comp["competitiesoort"] == "regulier") {
								$regularCompWasFound = true;
								//categorie
								$cat = 'Junioren';
								$teamCat = $comp["leeftijdscategorie"];
								if (strpos($teamCat, 'Onder 13') !== false || strpos($teamCat, 'Onder 12') !== false || strpos($teamCat, 'Onder 11') !== false || strpos($teamCat, 'Onder 10') !== false || strpos($teamCat, 'Onder 9') !== false || strpos($teamCat, 'Onder 8') !== false)
									$cat = 'Pupillen';
								else if (strpos($teamCat, 'Senioren') !== false)
									$cat = 'Senioren';
								$teamDetails['categorie'] = $cat;
								$teamDetails['group'] = $comp["leeftijdscategorie"];
								//speeldag
								$sdLong = $comp["speeldag"];
								$sdShort = $sdLong == 'Zaterdag' ? 'ZA' : 'ZO';
								$teamDetails['speeldag'] = $sdShort;
								//teamname
								$teamname = $comp["teamnaam"];
								if ($cat == 'Senioren')
									$teamname = $teamname . " (" . $sdShort . ")";
								$teamDetails['teamname'] = $teamname;
								// competition
								$teamDetails['competitionname'] = $comp["klassepoule"];
								$team['details'] = $teamDetails;
							}
						}
						if ($this->startsWith($player["Team"], self::$fabriPrefix)) {
							$teamDetails['group'] = 'Fabri league';
							$teamDetails['teamname'] = $player["Team"];
							$teamDetails['categorie'] = 'Pupillen';
							$team['details'] = $teamDetails;
						} else if ($regularCompWasFound == false) {
							// Noodoplossing: aan het begin van het seizoen zijn teams nog niet
							// beschikbaar via de API, maar moeten de spelers al wel getoond
							// worden op de website. We nemen hier alleen de jeugd mee, omdat het
							// voor de jeugd belangrijk is om te weten. Bij de senioren werkt dit
							// niet, omdat via Sportlink geen onderscheid gemaakt kan worden
							// tussen zaterdag- en zondagteams.
							$teamName = $player["Team"];
							if (!($this->startsWith($teamName, 'JO') || $this->startsWith($teamName, 'MO')))
								continue;
							$teamDetails["speeldag"] = 'zaterdag';
							$teamDetails["teamname"] = "Quick '20 " . $player["Team"];
							
							$team['details'] = $teamDetails;
						}
						$playerArray = $team['players'];
						if ($playerArray == null)
							$playerArray = array();
						$playerArray[$player["Relatiecode"]] = $player;
						$team['players'] = $playerArray;
						$teams[$teamCode] = $team;
					}
					foreach ($teams as $key => $value) {
						if ($value['players'] == null)
							unset($teams[$key]);
					}
					$newTeams = array();
					$newPlayers = array();
					$updatedTeams = 0;
					$updatedPlayers = 0;
					// Delete all existing data
					// Teams: not yet, we will use image name
					$wpdb->query( "TRUNCATE TABLE webservice__team");
					$wpdb->query( "TRUNCATE TABLE webservice__user");
					foreach ($teams as $key => $value) {
						$teamDetails = $value['details'];
						$competitionName = $teamDetails['competitionname'];
						$teamCat = $teamDetails['categorie'];
						$speelDag = $teamDetails['speeldag'];
						$teamPlayers = $value['players'];
						$player = array_values($teamPlayers)[0];
						//$teamGroup = $teamCat;
						//foreach ($teamPlayers as $plr => $dat)
						//	if ($dat["Leeftijdscat team"] != '')
						//		$teamGroup = $dat["Leeftijdscat team"];
						$teamGroup = $teamDetails['group'];
						if ($teamGroup == 'Senioren' || $teamGroup == 'Senioren Vrouwen')
							$teamGroup = $speelDag;
						$teamShortname = $player["Team"];
						// Fix teamnames voor Fabri League:
						if ($this->startsWith($teamShortname, self::$fabriPrefix)) {
							$teamShortname = substr($teamShortname, strlen(self::$fabriPrefix));
							$teamGroup = 'Fabri';
						}
						$teamName = $teamDetails['teamname'];
					  	$teamFromDb = $wpdb->get_row($wpdb->prepare("SELECT * FROM webservice__team WHERE knvb_id=%d", $key), ARRAY_A);
					  	if ($teamFromDb == null) {
							$retValTeam = $wpdb->replace('webservice__team', array( 
								'name' =>  $teamName,
								'short_name' => $teamShortname,
								'knvb_id' => $key,
								'category' => $teamCat,
								'group' => $teamGroup,
								'competition_title' => $competitionName,
								'created' => current_time('mysql', 1)
								)
							);
							if ($retValTeam == 1)
								array_push($newTeams, $key);
							if ($retValTeam == 2)
								$updatedTeams++;
						}
						$includePlayers = (($teamCat == 'Junioren' || $teamCat == 'Pupillen') && !$noYouth) || ($teamCat == 'Senioren' && !$noSeniors);
						if ($includePlayers) {
							foreach ($teamPlayers as $playerNr => $playerData) {
								//if ($playerData["Teamrol"] != 'Teamspeler' || $playerData["Op wedstrijdformulier?"] == 'TRUE') {
										$retValPl = $wpdb->replace('webservice__user', array( 
											'team' => $key,
											'knvb_code' => $playerData["Relatiecode"],
											'first_name' => $playerData["Roepnaam"],
											'infix' => $playerData["Tussenvoegsel(s)"],
											'last_name' => $playerData["Achternaam"],
											'email' => $playerData["E-mail"],
											'function' => $playerData["Teamfunctie"],
											'telephone' => $playerData["Mobiel"],
											'created' => current_time('mysql', 1),
											'visibility' => $playerData["Zichtbaarheid"]
											)
										);
										if ($retValPl == 1)
											array_push($newPlayers, $playerData["Roepnaam"] . " " . $playerData["Tussenvoegsel(s)"] . " " . $playerData["Achternaam"]);
										if ($retValPl == 2)
											$updatedPlayers++;
								//}
							}
						}
					}
					$out = array();
					$out= ["Bijgewerkt teams: " => $updatedTeams, "Bijgewerkt spelers" => $updatedPlayers, "Nieuwe teams: " => $newTeams, "Nieuwe spelers: " => count($newPlayers)];
				}
			}
		} else {
			$wpdb->insert( 'webservice__test', array('value' => 'nofile') );
		}
		//WPSWS_Output::get()->output( array('Succesvol:' => $out, 'Niet bekend bij API:' => $unknown) );
		WPSWS_Output::get()->output( $outtest );
	}
	
	public function fabriupload() {
		$nameConv = array();
		$nameConv['FC Porto'] = 'FC Porto';
		$nameConv['Chelsea'] = 'Chelsea';
		$nameConv['Barcelona'] = 'Barcelona';
		$nameConv['Bayern Munchen'] = 'Bayern M.';
		$nameConv['Real Madrid'] = 'Real Madrid';
		$nameConv['Juventus'] = 'Juventus';
		$nameConv['Liverpool'] = 'Liverpool';
		$nameConv['Manchester City'] = 'Manch. City';
		$nameConv['Inter Milaan'] = 'Inter Milaan';
		$nameConv['Borussia Dortmund'] = 'Borussia Dortmund';
		global $wpdb;
		global $_POST;
		$out = 'nothing happened';
		if ($_FILES) {
			$allowedExts = array("xlsx", "xls");
			$temp = explode(".", $_FILES["file"]["name"]);
			$extension = end($temp);
			if ( in_array($extension, $allowedExts)) {
				$out = 'Bestand is ontvangen; ';
  				if ($_FILES["file"]["error"] > 0) {
					$out = 'error';
  				} else {
  					// Read teams from database
  					$teamsFromDb = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` LIKE '%Fabri%'", ARRAY_A);
  					$teamsMap = array();
  					foreach($teamsFromDb as $team)
  						$teamsMap[$team["short_name"]] = $team;
  					// Load Excel
  					$reader;
  					if ($extension == 'xls')
  						$reader = PHPExcel_IOFactory::createReader('Excel5');
  					else
  						$reader = PHPExcel_IOFactory::createReader('Excel2007');
  					$excel = $reader->load($_FILES["file"]["tmp_name"]);
  					// Empty tables;
  					$wpdb->query('TRUNCATE TABLE webservice__fabri_results');
  					$wpdb->query('TRUNCATE TABLE webservice__fabri_standings');
  					// Read results
  					$excel->setActiveSheetIndexByName('Fabriuitslagen');
  					$sheet = $excel->getActiveSheet();
  					$round = 0;
  					for($rownum = 2; $rownum < 1000; $rownum++) {
  						$rowCheck = $sheet->getCell('D' . $rownum)->getValue();
  						if ($rowCheck === null || $rowCheck === '') {
  							// We're done
  							break;
  						}
  						$home = $sheet->getCell('D' . $rownum)->getOldCalculatedValue();
  						if ($home == '0' || $home == null || $home == '') {
  							// No team, so ignore this row
  							continue;
  						}
  						else {
  							$round = $this->cellValue($sheet, 'A', $rownum);
  							$date = $this->cellValue($sheet, 'B', $rownum);
  							$field = $this->cellValue($sheet, 'C', $rownum);
  							$home = $this->cellValue($sheet, 'D', $rownum);
  							$homeTeam = $teamsMap[$nameConv[$home]];
  							$away = $this->cellValue($sheet, 'E', $rownum);
  							$awayTeam = $teamsMap[$nameConv[$away]];
  							$homeScore = $this->cellValue($sheet, 'F', $rownum);
  							$awayScore = $this->cellValue($sheet, 'G', $rownum);
  							$toto = $this->cellValue($sheet, 'H', $rownum);
  							$wpdb->replace('webservice__fabri_results', array( 
									'round' => $round,
									'date' => $date,
									'field' => $field,
									'home_id' => $homeTeam["knvb_id"],
									'away_id' => $awayTeam["knvb_id"],
									'home_score' => $homeScore,
									'away_score' => $awayScore,
									'toto' => $toto
									)
							);
  						}
  					}
  					// Read standings
  					$excel->setActiveSheetIndexByName('fabristand');
  					$sheet = $excel->getActiveSheet();
  					$round = 0;
  					$started = 'false';
  					for($rownum = 2; $rownum < 100; $rownum++) {
  						$rowCheck = $sheet->getCell('A' . $rownum)->getOldCalculatedValue();
  						if ($rowCheck == null || $rowCheck == '') {
  							// We're done
  							break;
  						}
						$place = $sheet->getCell('A' . $rownum)->getOldCalculatedValue();
						$teamName = $sheet->getCell('B' . $rownum)->getOldCalculatedValue();
						$team = $teamsMap[$nameConv[$teamName]];
						$games = $sheet->getCell('C' . $rownum)->getOldCalculatedValue();
						$win = $sheet->getCell('D' . $rownum)->getOldCalculatedValue();
						$draw = $sheet->getCell('E' . $rownum)->getOldCalculatedValue();
						$lose = $sheet->getCell('F' . $rownum)->getOldCalculatedValue();
						$points = $sheet->getCell('G' . $rownum)->getOldCalculatedValue();
						$goalsScored = $sheet->getCell('H' . $rownum)->getOldCalculatedValue();
						$goalsAgainst = $sheet->getCell('I' . $rownum)->getOldCalculatedValue();
						$goalsDiff = $sheet->getCell('J' . $rownum)->getOldCalculatedValue();
						$wpdb->replace('webservice__fabri_standings', array( 
							'place' => $place,
							'team_id' => $team["knvb_id"],
							'games' => $games,
							'win' => $win,
							'draw' => $draw,
							'lose' => $lose,
							'points' => $points,
							'goals_scored' => $goalsScored,
							'goals_against' => $goalsAgainst,
							'goals_diff' => $goalsDiff
							)
						);
  					}
  				}
  			}
		} else {
			$out = 'no file';
		}
		WPSWS_Output::get()->output( array('Succesvol:' => $out) );
	}

	// --- Hulpfuncties --- //
	
	function startsWith($haystack, $needle) {
		$length = strlen($needle);
		return (substr($haystack, 0, $length) === $needle);
	}

	function endsWith($haystack, $needle) {
		$length = strlen($needle);
		if ($length == 0) {
			return true;
		}
		return (substr($haystack, -$length) === $needle);
	}
	
	function cellValue($sheet, $columnnum, $rownum) {
		$val = $sheet->getCell($columnnum . $rownum)->getOldCalculatedValue();
		return $val == null ? $sheet->getCell($columnnum . $rownum)->getValue() : $val;
	}
}