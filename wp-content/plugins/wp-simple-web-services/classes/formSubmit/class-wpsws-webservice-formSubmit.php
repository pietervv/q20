<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WPSWS_Webservice_formSubmit {
	private static $instance = null;
	
	public static function get() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function __construct() {
		$this->hooks();
	}

	private function hooks() {
		add_action( 'wpsws_webservice_formSubmit', array( $this, 'formSubmit' ) );
	}
	
	public function formSubmit() {
	    global $wpdb;
	    $additional = 'secretariaat@quick20.nl';
	    $formId = '';
	    
	    $subject = '[Jubileum] Inschrijving';
	    $message = "Inschrijving jubileum ontvangen via quick20.nl: \n\n";
	    
	    foreach ($_POST as $key => $value) {
	        if(substr($key, 0, 1) !== '_' && $key !== "FNAME" && $key !== "EMAIL") {
	            $formattedKey = str_replace("_", " ", $key);
	            $message = $message . $formattedKey . ': ' . $value . "\n";
	        }
	        if($key === "formId") {
	            $formId = $value;
	            if($value === "loterij") {
	               $additional = 'loterij100jaar@quick20.nl';
	            }
	        }
	    }
	    $to = array(
	        $additional,
	        'pietervv@gmail.com'
	    );
	    
	    $out = $to;
	    
	    wp_mail($to, $subject, $message);
	    
	    $wpdb->replace('jubileum_formsubmit', array(
	        'form_id' => $formId,
	        'form_data' => $message
	    ));
	    
	    WPSWS_Output::get()->output($out);
	}
}