<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WPSWS_Webservice_history {
	private static $instance = null;

	public static function get() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function __construct() {
		$this->hooks();
	}

	private function hooks() {
		add_action( 'wpsws_webservice_allTeams', array( $this, 'allTeams' ) );
		add_action( 'wpsws_webservice_teamsForSeason', array( $this, 'teamsForSeason' ) );
		add_action( 'wpsws_webservice_seasonsForTeam', array( $this, 'seasonsForTeam' ) );
		add_action( 'wpsws_webservice_detailsTeamSeason', array( $this, 'detailsTeamSeason' ) );
	}
	
	public function allTeams() {
		global $wpdb;
		$teams = $wpdb->get_results($wpdb->prepare("SELECT team_name, knvb_id FROM webservice__hist_team GROUP BY knvb_id ORDER BY team_name", $season), ARRAY_A);
		WPSWS_Output::get()->output(array('teams' => $teams));
	}

	public function teamsForSeason() {
		global $wpdb;
		global $_POST;
		$season = (int) $_POST['season'];
		$teams = $wpdb->get_results($wpdb->prepare("SELECT team_name, knvb_id FROM webservice__hist_team WHERE season=%d ORDER BY CHAR_LENGTH(team_name), team_name", $season));
		WPSWS_Output::get()->output(array('teams' => $teams));
	}
	
	public function seasonsForTeam() {
		global $wpdb;
		global $_POST;
		$selectedTeam = (int) $_POST['teamId'];
		$seasons = $wpdb->get_results($wpdb->prepare("SELECT season FROM webservice__hist_team WHERE knvb_id=%d ORDER BY season ASC", $selectedTeam));
		WPSWS_Output::get()->output(array('seasons' => $seasons));
	}

	public function detailsTeamSeason() {
		global $wpdb;
		global $_POST;
		$season = (int) $_POST['season'];
		$teamId= (int) $_POST['teamId'];
		$team = $wpdb->get_row($wpdb->prepare("SELECT * FROM webservice__hist_team WHERE knvb_id=%d AND season=%d", $teamId, $season), ARRAY_A);
		$allCompetitions = array();
		$competitions = $wpdb->get_results($wpdb->prepare("SELECT * FROM webservice__hist_competition WHERE hist_team_id=%d ORDER BY knvb_comp_id ASC", $team["id"]), ARRAY_A);
		foreach ($competitions as $competition) {
			$compArray = array();
			$compArray["overview"] = $competition;
			$compHistId = $competition["id"];
			$matchData = $wpdb->get_results($wpdb->prepare("SELECT * FROM webservice__hist_match WHERE hist_competition_id=%d", $compHistId));
			$compArray["matches"] = $matchData;
			$allCompetitions[$competition["knvb_comp_id"]] = $compArray;
		}
		$players = $wpdb->get_results($wpdb->prepare("SELECT * FROM webservice__hist_user WHERE hist_team_id=%d", $team["id"]), ARRAY_A);
		WPSWS_Output::get()->output(array('team' => $team, 'competitions' => $allCompetitions, 'players' => $players));
	}
}