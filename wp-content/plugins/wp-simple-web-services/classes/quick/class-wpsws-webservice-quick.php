<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class WPSWS_Webservice_quick {
	private static $instance = null;
	private static $staffStr = "'Ass.-trainer/coach', 'Hoofdcoach', 'Teammanager', 'Trainer', 'Trainer/coach', 'Verzorger', 'Verzorger', 'Assistent-scheidsrechter(club)', 'Standaard functie', 'Keeperstrainer'";

	public static function get() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	private function __construct() {
		$this->hooks();
	}

	private function hooks() {
		add_action( 'wpsws_webservice_allTeamsYouth', array( $this, 'allTeamsYouth' ) );
		add_action( 'wpsws_webservice_allTeamsSeniors', array( $this, 'allTeamsSeniors' ) );
		add_action( 'wpsws_webservice_teamDetails', array( $this, 'teamDetails' ) );
		add_action( 'wpsws_webservice_teamNews', array( $this, 'teamNews' ) );
	}

	public function allTeamsYouth() {
		global $wpdb;
		$teamsyouth = $wpdb->get_results( "SELECT short_name, knvb_id FROM webservice__team WHERE category NOT LIKE 'Senioren%' AND short_name NOT LIKE 'FL%' ORDER BY `group`, CHAR_LENGTH(short_name), short_name");
		WPSWS_Output::get()->output($teamsyouth);
	}

	public function allTeamsSeniors() {
		global $wpdb;
		$saturday = $wpdb->get_results("SELECT * FROM webservice__team WHERE `group` = 'ZA' AND short_name != '' ORDER BY CHAR_LENGTH(short_name), short_name");
		$sunday = $wpdb->get_results( "SELECT * FROM webservice__team WHERE `group` = 'ZO' AND short_name != '' ORDER BY CHAR_LENGTH(short_name), short_name");
		WPSWS_Output::get()->output(array('saturday' => $saturday, 'sunday' => $sunday));
	}

	public function teamDetails() {
		global $wpdb;
		$knvbId = (int) $_POST['teamId'];
		$team = $wpdb->get_row($wpdb->prepare("SELECT * FROM webservice__team WHERE knvb_id=%d", $knvbId), ARRAY_A);
		$teamName = $team["short_name"];
		$players = $wpdb->get_results($wpdb->prepare("SELECT first_name, infix, last_name, function FROM webservice__user u WHERE u.team = %d AND function NOT IN (" . self::$staffStr . ") GROUP BY u.knvb_code ORDER BY last_name, infix, first_name", $knvbId ));
		$picture = $wpdb->get_var($wpdb->prepare("SELECT value FROM webservice__team_data WHERE team_id = %d AND `key` = 'teampicture'", $knvbId ));
		$staff = $wpdb->get_results($wpdb->prepare("SELECT first_name, infix, last_name, function, email, telephone FROM webservice__user u WHERE u.team = %d AND function IN (" . self::$staffStr . ") GROUP BY u.knvb_code ORDER BY last_name, infix, first_name", $knvbId ));
		$output = ["players" => $players, "staff" => $staff, "team" => $team, "teamName" => $teamName, "picture" => $picture];
		WPSWS_Output::get()->output( $output );
	}

	public function teamNews() {
		global $wpdb;
		$knvbId = (int) $_POST['teamId'];
		$team = $wpdb->get_row($wpdb->prepare("SELECT * FROM webservice__team WHERE knvb_id=%d", $knvbId), ARRAY_A);
		$teamName = $team["short_name"];
		$matchingStr = '%' . $teamName . '%';
		$teamNews = $wpdb->get_results($wpdb->prepare("SELECT n.news_title, n.news_data, n.news_date, n.summary, n.news_id FROM cmsms_module_news n JOIN cmsms_module_news_categories c ON c.news_category_id = n.news_category_id WHERE c.news_category_name LIKE %s ORDER BY n.news_date DESC LIMIT 0,10", $matchingStr), ARRAY_A);
		WPSWS_Output::get()->output(['teamNews' => $teamNews]);
	}
}